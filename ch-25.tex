% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{How Do You Deal With Motion In A Uniform Electrostatic Field?}
\label{ch-25}

\begin{dialogue}

\tchr Assume that a charged body moves in a uniform electrostatic field, i.e. in a field where each point has the same intensity $E$ both in magnitude and direction. An example is the field between the plates of a parallel-plate capacitor. Can you see any resemblance between the problem on the motion of a charged body in a uniform electrostatic field and any problems considered previously?

\stdb It seems to me that it closely resembles the problem of the motion of a body in a gravitational field. Over relatively short distances, the gravitational field of the earth can be regarded as uniform.

\tchr Exactly. And what is the difference between motion in an electrostatic field and in a gravitational field?

\stdb Different forces act on the bodies. In an electrostatic field, the force acting on the body is $F_{e}=E\,q$ (it imparts an acceleration of $a_{e}=Eq/m$ to the body). The force in a gravitational field is $P=mg$ (imparting the acceleration $g$ to the body). Here $m$ is the mass of the body and $q$ is its electric charge.

\tchr I wish that all examinees could understand the simple truth that the motion of a body in any uniform field is kinematically the same. What differs is only the value of the force acting on the body in different fields. The motion of a charged body in a uniform electrostatic field is of the same nature as the motion of an ordinary stone in the earth's field of gravitation. Let us consider a problem in which the motion of a body takes place simultaneously in two fields: gravitational and electrostatic. \hlred{A body of mass $m$ with a charge $+q$ is thrown upward at an angle of $\alpha$, to the horizontal with an initial velocity $v_{0}$. The body travels simultaneously in the field of gravitation and in a uniform electrostatic field with an intensity $E$. The lines of force of both fields are directed vertically downward \drkgry{(\figr{fig-94}~(a))}. Find the time of light $T_{1}$ range $L_{1}$ and maximum height reached $H_{1}$.}
\begin{figure}%[-5cm]%
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-25/fig-094a}
\caption{Lines of force of two fields in which a body travels.}
\label{fig-94}
\end{figure}

\stdb Two forces act on the body: the weight $P=mg$ and the electric force $F_{e}=Eq$. In the given case, both forces are parallel. As in \chap{ch-05}, I can resolve the initial velocity vector ta) into components in two directions \ldots

\tchr (interrupting): Just a minute! Do you want to repeat the solution demonstrated
in a similar problem in \chap{ch-05}?

\stdb Yes, at least briefly.

\tchr There is no need to do that. You can refer directly to the results in equations \eqref{eq-15}, \eqref{eq-16} and \eqref{eq-17}. Just imagine that the body travels in a ``stronger'' field of gravitation characterized by a total acceleration equal to $g+E (q/m)$. Make the following substitution in equations \eqref{eq-15}, \eqref{eq-16} and \eqref{eq-17})
\begin{equation}%
 g +  \left(\frac{Eq}{m} \right) \,\, \text{for} \,\, g 
\label{139}
\end{equation}
and you will obtain the required results at once:
\begin{align}%
T_{1} & = \frac{2 v_{0} \sin \alpha}{ g + \left(\dfrac{Eq}{m} \right)} \label{eq-140}\\
L_{1} &= \frac{ v_{0}^{2} \sin 2 \alpha}{g + \left(\dfrac{Eq}{m} \right)} \label{eq-141}\\
H_{1} &=\frac{1}{2} \frac{v_{0}^{2} \sin^{2} \alpha}{g + \left(\dfrac{Eq}{m} \right)} \label{eq-142}
\end{align} 

\stda There is one point here that I don't understand. In comparison with the corresponding problem in \chap{ch-05}, an additional force $F_{e}$ acts on the body in the given problem. This force is directed vertically downward and therefore should not influence the horizontal motion of the body. Why then, in the given case, does it influence the range of flight $L_{1}$?

\tchr The range depends upon the time of flight, and this time is determined from a consideration of the vertical motion of the body.

Now let us make a slight change in the conditions of the problem: \hlred{assume that the lines of force of the electrostatic field are directed at an angle $\beta$ to the vertical \drkgry{(\figr{fig-94}~(b))}. As before, find the time of flight $T_{2}$, range $L_{2}$ and maximum height reached $H_{2}$.}

\stda First I shall resolve force $F_{e}$ into two components: vertical ($F_{e} \cos \beta$) and horizontal ($F_{e} \sin \beta$). This problem reminds me of the problem with the tail wind in \chap{ch-05}. Here the component $F_{e} \sin \beta$ plays the part of the ``force of the wind''.

\tchr Quite right. Only remember that in contrast to the problem with the tail wind you mentioned, here we have a different vertical force, namely: $mg + F_{e} \cos \beta$.

\stda I shall make use of equations \eqref{eq-15}, \eqref{eq-16} and \eqref{eq-18}, in which I'll make the following substitutions
\begin{equation}%
\left.
\begin{aligned}
g + 	\frac{E q \cos \beta}{m} \,\, & \text{for} \,\, g \\
\frac{E q \sin \beta}{mg + Eq \cos \beta} \,\, & \text{for} \,\, \frac{F}{P}
\label{eq-143}
\end{aligned}
\right\}
\end{equation}
After this I obtain the required results at once
\begin{align}%
T_{2} & = \frac{2 v_{0} \sin \alpha}{ g +  \left(\dfrac{E q \cos \beta}{m} \right)} \label{eq-144}\\
L_{2} &= \frac{ v_{0}^{2} \sin^{2} \alpha}{g +  \left(\dfrac{E q \cos \beta}{m} \right)} \left(1 + \frac{Eq \sin \beta \tan \alpha}{mg + Eq \cos \beta} \right) \label{eq-145}\\
H_{2} &=\frac{1}{2} \frac{v_{0}^{2} \sin^{2} \alpha}{g +  \left(\dfrac{E q \cos \beta}{m} \right)} \label{eq-146}
\end{align} 
\tchr Absolutely correct. Unfortunately, examinees are often incapable of drawing an analogy between motion in a field of gravitation and motion in a uniform electrostatic field. Consequently, such problems prove to be excessively difficult for them.

\stda We did not study such problems before. The only problem of this kind I have ever encountered concerns the motion of an electron between the plates of a parallel-plate capacitor, but we neglected the influence of the gravitational field on the electron. I remember that such problems seemed to be exceedingly difficult.

\tchr All these problems are special cases of the problem illustrated in \figr{fig-94}~\drkgry{(a)}, since in the motion of an electron inside a capacitor the influence of the gravitational field can be neglected. Let us consider one such problem. 

\hlred{Having an initial velocity $v_{1}$ an electron flies into a parallel-plate capacitor at an angle of $\alpha_{1}$ and leaves the capacitor at an angle of $\alpha_{2}$ to the plates as shown in \figr{fig-95}. The length of  the capacitor plates is $L$. Find the intensity $E$ of the capacitor field and the kinetic energy of the electron as it leaves the capacitor. The mass $m$ and charge $q$ of the electron are known.}

\begin{figure}%[-5cm]%
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-25/fig-095a}
\caption{Electron in a parallel plate capacitor.}
\label{fig-95}
\end{figure}

I denote by $v_{2}$ the velocity of the electron as it flies out of the capacitor. Along the plates the electron flies at uniform velocity. This enables us to determine the time of flight $T$ inside the capacitor
\begin{equation*}%
T= \frac{L}{v_{1} \cos \alpha_{1}}
\end{equation*}
The initial and final components of the electron velocity perpendicular to the plates are related by the familiar kinematic relationship for uniformly decelerated motion
\begin{align*}
v_{2} \sin \alpha_{2} & = v_{1} \sin \alpha_{1} - \frac{Eq}{m} \, T \\
& = v_{1} \sin \alpha_{1} - \frac{Eq}{m} \left( \frac{L}{v_{1} \cos \alpha_{1}} \right)
\end{align*}
from which, taking into account that the velocity component along the plates remains unchanged ($v_{1} \cos \alpha_{1} = v_{2} \cos \alpha_{2}$), we obtain
\begin{equation*}%
v_{1} \cos \alpha_{1} \tan \alpha_{2} = v_{1} \sin \alpha_{1} - \frac{Eq}{m} \left( \frac{L}{v_{1} \cos \alpha_{1}} \right)
\end{equation*}
From this equation we determine the intensity of the capacitor field
\begin{equation}%
E = ( \tan \alpha_{1} - \tan \alpha_{2}) \frac{mv_{1}^{2} \cos^{2} \alpha_{1}}{qL}
\label{eq-147}
\end{equation}
The kinetic energy of the electron as it flies out of the capacitor is
\begin{equation}%
\frac{mv_{2}^{2}}{2} = \frac{mv_{1}^{2}}{2} \left( \frac{\cos^{2} \alpha_{1}}{\cos^{2} \alpha_{2}} \right)
\label{eq-148}
\end{equation}
Is everything quite clear in this solution?

\stda Yes. Now I know how to solve such problems.

\tchr Also of interest are problems concerning the vibration of a pendulum with a charged bob located within a parallel-plate capacitor. We shall consider the following problem. \hlred{A bob of mass $m$ with a charge $q$ is suspended from a thin string of length $l$ inside a parallel-plate capacitor with its plates oriented horizontally. The intensity of the capacitor field is $E$, and the lines of force are directed downward \drkgry{(\figr{fig-96}~(a))}. Find the period of vibration of such a pendulum.}
\begin{figure*}[h]%[-5cm]%
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-25/fig-096a}
\caption{Oscillations of a charged bob in capacitor plates.}
\label{fig-96}
\end{figure*}
\stdb Since in the given case the lines of force of the electrostatic field and of the gravitational field are in the same direction, I can use the result of equation \eqref{eq-75} for an ordinary pendulum after substituting the sum of the accelerations $(g+ Eq/m)$ for the acceleration of gravity $g$. Thus the required period of vibration
\begin{equation}%
T = 2 \pi \sqrt{\dfrac{l}{ g+\left(\dfrac{Eq}{m} \right)}}
\label{eq-149}
\end{equation}
\tchr Quite correct. As you see, the posed problem is very simple if you are capable of using the analogy between motion in a uniform electrostatic field and in a gravitational field.

\stda Equation \eqref{eq-149} resembles equation \eqref{eq-77} in its structure.

\tchr This is quite true. Only in equation \eqref{eq-77} the addend to the acceleration $g$ was due to the acceleration of the frame of reference (in which the vibration of the pendulum was investigated), while in equation \eqref{eq-149} the addend is associated with the presence of a supplementary interaction. 

\hlred{How will equation \eqref{eq-149} change if the sign of the charges on the capacitor plates is reversed?}

\stda In this case the period of vibration will be
\begin{equation}%
T = 2 \pi \sqrt{\dfrac{l}{ g- \left(\dfrac{Eq}{m} \right)}}
\label{eq-150}
\end{equation}

\tchr Good. What will happen to the pendulum if we gradually increase the intensity of the capacitor field?

\stda The period of vibration will increase, approaching infinity at $E=mg/q$. If $E$ continues to increase further, then we will have to fasten the string to the lower and not the upper plate of the capacitor.

\tchr What form will the equation for the period take in this case?

\stda This equation will be of the form
\begin{equation}%
T = 2 \pi \sqrt{\dfrac{l}{ \left(\dfrac{Eq}{m} \right) - g}}
\label{eq-151}
\end{equation}
\\
\tchr Good. Now let us complicate the problem to some extent. \hlred{We will consider the vibration of a pendulum with a charged bob inside a capacitor whose plates are oriented, not horizontally, but vertically \drkgry{(\figr{fig-96}~(b))}. In this case, the accelerations $g$ and $(Eq/m)$ are directed at right angles to each other. As before, find the period of vibration of the pendulum and, in addition, the angle $\alpha$ that the string makes with the vertical when the pendulum is in the equilibrium position.}

\stdb Taking into consideration the line of reasoning given in the present section and in  \chap{ch-12}, I can conclude at once that: 
\begin{enumerate}[label=(\arabic*)]
\item the period of vibration is expressed in terms of the effective acceleration $g_{e\!f\!\!f}$ which is the vector sum of the accelerations of the earth's gravity and of the electrostatic field; and 
\item the equilibrium direction of the string coincides with the vector of the above-mentioned effective acceleration (this direction is shown in \figr{fig-96}~\drkgry{(b)} by a dashed line). 
\end{enumerate}
Thus
\begin{equation}%
T = 2 \pi \sqrt{ \dfrac{l}{\sqrt{ g^{2}+ \left(\dfrac{Eq}{m} \right)^{2}}}}
\label{eq-152}
\end{equation}
and
\begin{equation}%
\tan \alpha = \dfrac{Eq}{gm}
\label{eq-153}
\end{equation}
\tchr Absolutely correct. I think that now it will be easy to investigate the general case in which \hlred{the capacitor plates make an angle of $\beta$ with the horizontal \drkgry{(\figr{fig-96}~(c))}. The same problem is posed: find the period of vibration of the pendulum and the angle ex between the equilibrium direction of the pendulum string and the vertical.}

\stdb As in the preceding case, the effective acceleration is the vector sum of the acceleration of the earth's gravity and that of the electrostatic field. The direction of this effective acceleration is the equilibrium direction of the pendulum string. The effective acceleration $g_{e\!f\!\!f}$ can be found by using the law of cosines from trigonometry. Thus
\begin{equation*}
g_{e\!f\!\!f}^{2} = g^{2} \left( \frac{Eq}{m} \right)^{2} + 2g \frac{Eq}{m} \cos \beta
\end{equation*}
Then
\begin{equation}%
T = 2 \pi \sqrt{ \dfrac{l}{\sqrt{ g^{2}+ \left(\dfrac{Eq}{m} \right)^{2} +2g \left( \dfrac{Eq}{m} \right) \cos \beta}}}
\label{eq-154}
\end{equation}
The value of $\tan \alpha$ can be found as follows
\begin{equation}%
\tan \alpha =  \dfrac{g_{e\!f\!\!f\,x}}{g_{e\!f\!\!f\,y}} = \frac{Eq \sin \beta}{gm + Eq \cos \beta}
\label{eq-155}
\end{equation}
\tchr Your answers are correct. Obviously, at $\beta=0$, they should lead to the results for the case of horizontal plates, and at $\beta =\ang{90}$ to those for vertical plates. Please check
whether this is so.

\stdb If $\beta=0$, then $ \cos \beta = 1$ and $\sin \beta=0$. In this case, equation \eqref{eq-154} reduces to equation \eqref{eq-149} and $\tan \alpha=0$ (the equilibrium position of the string is vertical). If $\beta =\ang{90}$, then $\cos \beta= 0$ and $\sin \beta=1$. In this case, equation \eqref{eq-154} becomes equation \eqref{eq-152}, and equation \eqref{eq-155} reduces to equation \eqref{eq-153}.

\tchr I think that we have completely cleared up the problem of the vibration of a pendulum with a charged bob inside a parallel-plate capacitor. In conclusion, I want you to \hlred{calculate the period of vibration of a pendulum with a charged bob given that at the point where the string of the pendulum is attached there is another charge of exactly the same magnitude and sign \drkgry{(\figr{fig-97})}.}

There are no capacitors whatsoever.
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=.8\textwidth]{figs/ch-25/fig-097a}
\caption{A pendulum with two charges: one at the bob and other at the attachment point.}
\label{fig-97}
\end{marginfigure}

\stda According to Coulomb's law, the bob will be repulsed from the point of suspension of the string with a force of $q^{2}/l^{2}$. This force should impart an acceleration of $q^{2}/l^{2}m$ to the bob. The acceleration must be taken into account in the equation for finding the period of vibration. As a result we obtain the following expression
\begin{equation}%
T = 2 \pi \sqrt{ \dfrac{l}{g+ \left(\dfrac{q^{2}}{l^{2}m} \right)}}
\label{eq-156}
\end{equation}

\tchr (to \smallcaps{Student B}): Do you agree with this result?

\stdb No, I don't. For equation \eqref{eq-156} to be valid, it is necessary for the acceleration $q^{2}/l^{2}m $ to be directed vertically downward at all times. Actually, it is so directed only when the pendulum passes the equilibrium position. Thus it is clear that equation \eqref{eq-156} is wrong in any case. However, I don't think that I can give the right answer.

\tchr That you understand the error in equation \eqref{eq-156} is a good sign in itself. In the given case, the electric force is at all times. directed along the string and is therefore always counterbalanced by the reaction of the string. It follows that the electric force does not lead to the development of a restoring force and, consequently, cannot influence the period of vibration of the pendulum.

\stdb Does that mean that in the given case the period of vibration of the pendulum will be found by equation \eqref{eq-75} for a pendulum with an uncharged bob?

\tchr Exactly. In the case we are considering, the field of electric forces is in no way uniform and no analogy can be drawn with a gravitational field.

\end{dialogue}

\section*{Problems}
\label{prob-25}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume=problems]
\item An electron flies into a parallel-plate capacitor in a direction parallel to the plates and at a distance of \SI{4}{\centi\metre} from the positively charged plate which is \SI{15}{\centi\metre} long. How much time will elapse before the electron falls on this plate if the intensity of the capacitor field equals \SI[per-mode=symbol]{500}{\volt\per\metre})? At what minimum velocity can the electron fly into the capacitor so that it does not fall on the plate? The mass of the electron is \SI{9d-28}{\gram}, its charge is \num{4.8d-10} esu (electrostatic units).
\item An electron flies into a parallel-plate capacitor parallel to its plates at a velocity of \SI[per-mode=symbol]{3d6}{\metre\second}. Find the intensity of the field in the capacitor if the electron flies out of it at an angle of \ang{30} to the plates. The plates are \SI{20}{\centi\metre}? long. The mass and charge of the electron are known (see problem No. 45).
\item Inside a parallel-plate capacitor with a field intensity $E$, a bob with a mass $m$ and charge $q$, suspended from a string of length $l$, rotates with uniform motion in a circle (\figr{fig-98}). The angle of inclination of the string is $\alpha$. Find the tension of the string and the kinetic energy of the bob. 
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-25/fig-098a}
\caption{Pendulum inside a parallel plate capacitor.}
\label{fig-98}
\end{marginfigure}
\item Two balls of masses $m_{1}$ and $m_{2}$ and with charges $+q_{1}$ and $+q_{2}$ are connected by a string which passes over a fixed pulley. Calculate the acceleration of the balls and the tension in the string if the whole system is located in a uniform electrostatic field of intensity $E$ whose lines of force are directed vertically downward. Neglect any interaction between the charged balls.
\item A ball of mass $m$ with a charge of $+q$ can rotate in a vertical plane at the end of a string of length $l$ in a uniform electrostatic field whose lines of force are directed vertically upward. What horizontal velocity must be imparted to the ball in its upper position so that the tension of the string in the lower position of the ball is 10 times the weight of the ball?
\end{enumerate}









\cleardoublepage

%\thispagestyle{empty}
%\vspace*{-1.95cm}
%\begin{figure*}[!ht]
%\centering
%\includegraphics[width=0.7\textwidth]{figs/sec/sec-i.pdf}
%\end{figure*}
%\vspace*{-.4cm}
%%\begin{centering}
%\begin{fullwidth}
%%%\paragraph{}
%{\textsf{\Large \hlred{What is a field? How is a field described? How does motion take place in a field? These fundamental problems of physics can be most conveniently considered using an electrostatic field as an example. We shall discuss the motion of charged bodies in a uniform electrostatic field. A number of problems illustrating Coulomb's law will be solved.
%}}}
%\end{fullwidth}
%
%\cleardoublepage
