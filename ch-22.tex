% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{Let Us Discuss Field Theory}
\label{ch-22}

\begin{dialogue}

\tchr Let us discuss the field, one of the basic physical concepts. For the sake of definiteness, we shall deal with electrostatic fields. What is your idea of a field? What do you think it is?

\stda I must confess that I have a very vague idea of  what a field really is. A field is something elusive, invisible, a kind of spectre. At the same time, it is said to be present throughout space. I do not object to the field being defined as a material entity. But this means nothing to me. When we speak of matter, I understand what we are talking about. But when we speak of a field, I give up.

\stdb To me, the concept of the field is quite tangible. Matter in any substance is in concentrated form, as it were. In a field, on the contrary, matter is ``spread'' throughout space, so to speak. The fact that we cannot see a field with the naked eye doesn't prove anything. A field can be ``seen'' excellently by means of relatively simple instruments. A field acts as a transmitter of interactions between bodies. For instance, an electrostatic field transmits interactions between fixed electric charges. Each charge can be said to set up a field around itself. A field set up by one charge influences another charge and, conversely, the field set up by the second charge influences the first charge. Thus, Coulomb (electrostatic) interaction of charges is accomplished.

\stda But couldn't we get along without any ``go-betweens''? What prevents us from supposing that one charge acts directly on another charge?

\stdb Your supposition may raise serious objections. Assume that at some instant one of the charges is displaced (i.e. ``budges'') for some reason. If we proceed from the supposition of, ``direct interaction'', we have to conclude that the second charge must also ``budge'' at the very same instant. This would mean that a signal from the first charge reaches the second charge instantaneously. This would contradict the basic principles of the theory of relativity. If, however, we have a transmitter of interactions, l.e. a field, the signal is propagated from one charge to the other through the field. However large the velocity of propagation, it is always finite. Therefore, a certain interval of time exists during which the first charge has stopped ``budging'' and the second has not yet started. During this interval, only the field contains the signal for ``budging''.

\stda All the same, I would like to hear a precise definition of the field.

\tchr I listened to your dialogue with great interest. I feel that \smallcaps{Student B} has displayed a keen interest in problems of modern physics and has read various popular books on physics. As a result, he has developed what could be called initiative thinking. To him the concept of the field is quite a real, ``working'' concept. His remarks on the field as a transmitter of interactions are quite correct. \smallcaps{Student A}, evidently, confined himself to a formal reading of the textbook. As a result, his thinking is inefficient to a considerable extent. I say this, of course, without any intention to offend him or anyone else, but only to point out that many examinees feel quite helpless in like situations. Strange as it may be, a comparatively large number of students almost never read any popular-science literature. However, let us return to the essence of the problem. (To \smallcaps{Student A}) You demanded a precise definition of the field. Without such a definition the concept of the field eludes you.However, you said that you understand what matter is. But do you really know the precise definition of matter?

\stda The concept of matter requires no such definition. Matter can be ``touched'' with your hand.

\tchr In that case, the concept of the field also ``requires no such definition''; it can also be ``touched'', though not with your hand. However, the situation with the definition is much more serious. To give a precise, logically faultless definition means to express the concept in terms of some more ``primary'' concepts. But what can be done if the given concept happens to be one of the ``primary'' concepts? Just try to define a straight line in geometry. Approximately the same situation exists with respect to the concepts of matter and the field. These are primary, fundamental concepts for which we can scarcely hope to find a clear-cut blanket definition.

\stda Can we, nevertheless, find some plausible definition?

\tchr Yes, of course. Only we must bear in mind that no such definition can be exhaustive. Matter can exist in various forms. It can be concentrated within a restricted region of space with more or less definite boundaries (or, as they say, it can be ``localized''), but, conversely, it can also be ``delocalized''. The first of these states of matter can be associated with the concept of matter in the sense of a ``substance'', and the second state with the concept of the ``field''. Along with their distinctive characteristics, both states have common physical characteristics. For example, there is the energy of a unit volume of matter (as a substance) and the energy of a unit volume of a field. We can speak of the momentum of a unit volume of a substance and the momentum of a unit volume of a field. Each kind of field transmits a definite kind of interaction. It is precisely from this interaction that we can determine the characteristics of the field at any required point. An electrically charged body, for instance, sets up an electrostatic field around itself in space. To reveal this field and measure its intensity at some point in space, it is necessary to bring another charged body to this point and to measure the force acting on it. It is assumed that the second charged body is sufficiently small so that the distortion it causes in the field can be neglected.

The properties of matter are inexhaustible, the process of seeking knowledge is eternal. Gradually, step by step, we advance along the road of learning and the practical application of the properties of matter that surrounds us. In our progress we have to ``stick on labels'' from time to time which are a sort of landmarks along the road to knowledge. Now we label something as a ``field''. We understand that this ``something'' is actually the primeval abyss. We know much about this abyss we have called a ``field'', and therefore we can employ this newly introduced concept more or less satisfactorily. We know much, but far from all. An attempt to give it a clear-cut definition is the same as an attempt to measure the depth of a bottomless chasm.

\stdb I think that the concept of the field, as well as any other concept emerging in the course of our study of the material world, is inexhaustible. This, exactly, is the reason why it is impossible to give an exhaustive, clear-cut definition of a field.

\tchr I completely agree with you.

\stda I am quite satisfied with your remarks about substance and the field as two states of matter-localized and unlocalized. But why did you begin this discussion about the inexhaustibility of physical concepts and the eternity of learning? As soon as I heard that, clarity vanished again and everything became sort of blurred and vague.

\tchr I understand your state of mind. You are seeking for some placid definition of a field, even if it is not absolutely precise. You are willing to conscientiously memorize this definition and hand it out upon request. You don't wish to recognize that the situation is not at all static but, on the contrary, a dynamic one. You shouldn't believe that everything becomes blurred and vague. I would say that everything becomes dynamic in the sense that it tends toward change. Any precise definition, in itself, is something rigid and final. But physical concepts should be investigated in a state of their development. That which we understood to be the concept of the field yesterday appreciably differs from what we understand by this concept today. Thus, for instance, modern physics, in contrast to the classical version, does not draw a distinct boundary between the field and substance. In modern physics, the field and substance are mutually transformable: a substance may become a field and a field may become a substance. However, to discuss this subject in more detail now would mean getting too far ahead.
\stdb Our discussion on physics has taken an obviously philosophical turn.

\tchr That is quite natural because any discussion of physical conceptions necessarily presupposes that the participants possess a sufficiently developed ability for dialectical thinking. If this ability has not yet been cultivated, we have, even against our will, to resort to digressions of a philosophical nature. This is exactly why I persist in advising you to read more and more books of various kinds. Thereby you will train your thinking apparatus, make it more flexible and dynamic. In this connection, invaluable aid can be rendered to any young person by V. I. Lenin's book \emph{Materialism and Empiriocriticism}. I advise you to read it.

\stda But that is a very difficult book. It is studied by students of institutes and universities.

\tchr I don't insist on your studying this book. It certainly was not intended for light reading. Simply try to read it through carefully. Depending on your background, this book will exert a greater or lesser influence on your mode of thinking. In any case, it will be beneficial.

In conclusion, I wish to mention the following: Student A is obviously afraid of vagueness or indefiniteness; he demands maximum precision. He forgets that there is a reasonable limit to everything, even precision. Try to imagine a completely precise world about which we have exhaustive information. Just conjure up such a world and then tell me: wouldn't you be amazed at its primitiveness and inability to develop any further? Think about all this and don't hurry with your conclusions. And now, for the present, let us attempt to approach the problem from another angle. I will pose the following question: ``How is a field described?'' I know that many people, after getting the answer, will say: ``Now we know what the field is''.


\end{dialogue}




\cleardoublepage

%\thispagestyle{empty}
%\vspace*{-1.95cm}
%\begin{figure*}[!ht]
%\centering
%\includegraphics[width=0.7\textwidth]{figs/sec/sec-i.pdf}
%\end{figure*}
%\vspace*{-.4cm}
%%\begin{centering}
%\begin{fullwidth}
%%%\paragraph{}
%{\textsf{\Large \hlred{What is a field? How is a field described? How does motion take place in a field? These fundamental problems of physics can be most conveniently considered using an electrostatic field as an example. We shall discuss the motion of charged bodies in a uniform electrostatic field. A number of problems illustrating Coulomb's law will be solved.
%}}}
%\end{fullwidth}
%
%\cleardoublepage
