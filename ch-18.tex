% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{What Do You Know About The Molecular-Kinetic Theory Of Matter?}
\label{ch-18}

\begin{dialogue}

\tchr One of the common examination questions is: \hlred{what are the basic principles of the
molecular-kinetic theory of matter?} How would you answer this question?

\stda I would mention the two basic principles. The first is that all bodies consist of molecules, and the second, that the molecules are in a state of chaotic thermal motion.

\tchr Your answer is very typical: laconic and quite incomplete. I have noticed that students usually take a formal attitude with respect to this question. As a rule, they do not know what should be said about the basic principles of the molecular-kinetic theory, and explain it away with just a few general remarks. In this connection, I feel that the molecular-kinetic theory of matter should be discussed in more detail. I shall begin by mentioning the principles of this theory that can be regarded as the basic ones.
\begin{enumerate}[label=\arabic*.,leftmargin=1cm]
\item Matter has a ``granular'' structure: it consists of molecules (or atoms). One gram-molecule of a substance contains $N_{A} = \num{6d23}$ molecules regardless of the physical state of the substance (the number $N_{A}$ is called Avogadro's number).
\item The molecules of a substance are in a state of incessant thermal motion.
\item The nature of the thermal motion of the molecules depends upon the nature of their interaction and changes when the substance goes over from one physical state to another.
\item The intensity of the thermal motion of the molecules depends upon the degree to which the body is heated, this being characterized by the absolute temperature $T$. The theory proves that the mean energy $e$ of a separate molecule is proportional to the temperature $T$. Thus, for instance, for monoatomic (single-atom) moleculese
\begin{equation}%
e = \frac{3}{2} kT 
\label{eq-98}
\end{equation}
where $k= \num{1.38d-16}$ erg/deg is a physical constant called Boltzmann's constant.

\item From the standpoint of the molecular-kinetic theory, the total energy $E$ of a body is the sum of the following terms: 
\begin{equation}%
E = E_{k} + E_{p} + U
\label{eq-99}
\end{equation}
 where $E_{k}$ is the kinetic energy of the body as a whole, $E_{p}$ is the potential energy of the body as a whole in a certain external field, and $U$ is the energy associated with the thermal motion of the molecules of the body. Energy $U$ is called the internal energy of the body. Inclusion of the internal energy in dealing with various energy balances is a characteristic feature of the molecular-kinetic theory.
\end{enumerate}
\stdb We are used to thinking that the gram-molecule and Avogadro's number refer to chemistry.
\\
\tchr Evidently, that is why students taking a physics examination do not frequently know what a gram-molecule is, and, as a rule, are always sure that Avogadro's number refers only to gases. Remember: \hlblue{a gram-molecule is the number of grams of a substance which is numerically equal to its molecular weight (and by no means the weight of the molecule expressed in grams, as some students say); the gram-atom is the number of grams of a substance numerically equal to its atomic weight; and Avogadro's number is the number of molecules in a gram-molecule (or atoms in a gram-atom) of any substance, regardless of its physical state.}

I want to point out that Avogadro's number is a kind of a bridge between the macro and micro characteristics of a substance. Thus, for example, using Avogadro's number, you can express such a micro characteristic of a substance as the mean distance between its molecules (or atoms) in terms of the density and molecular (or atomic) weight. For instance, let us consider solid iron. Its density is 
$\rho=\SI{7.8}{\gram\per\cubic\centi\metre}$ and atomic weight $A =56$. We are to find the mean distance between the atoms in iron. We shall proceed as follows: in $A\,\, \si{\gram}$ of iron there are $N_A$ atoms, then in \SI{1}{\gram} of iron there must be $N_{A}/A$ atoms. It follows that in \SI{1}{\cubic\centi\meter} there are $\rho N_{A}/A$ atoms. Thus each atom of iron is associated with a volume of  $A/\rho N_{A} \si{\cubic\centi\meter}$. The required mean distance between the atoms is approximately equal to the cube root of this volume
\begin{equation*}%
x \approx \sqrt[3]{\frac{A}{\rho N_{A}}} =\sqrt[3]{\frac{56}{7.8\times \num{6d23}}} \,\, \si{\centi\meter} \approx \num{2d-8}\,\, \si{\centi\meter}
\end{equation*}
\stdb Just before this you said that the nature of the thermal motion of the molecules depends upon the intermolecular interaction and is changed in passing over from one physical state to another. Explain this in more detail, please.
%\begin{marginfigure}%
%\centering
%\includegraphics[width=\linewidth]{fig-069a}
%\caption{What additional weight must be put on the pan with the stand to restore equilibrium?}
%\label{fig-69}
%\end{marginfigure}

\begin{marginfigure}
\centering
%\fbox{
\begin{tikzpicture}[>=latex,scale=.75]
\clip (-1,-1.6) rectangle (5, 4.8);
\draw[thick, ->,gray] (0,0) -- (5,0);
\draw[thick, ->,gray] (0,-1.8) -- (0,4.5);
\draw[thick, dashed,gray] (0,-1) -- (4,-1);
\node [left] at (0,4.5) {$E_{p}$};
\node [left] at (0,-1) {$e_{1}$};
\node [below left] at (4.8,0) {$r$};
\node [left] at (0,0) {0};
\draw[ thick,Maroon] (0.1,3.4) .. controls (.38,-4.8) and (2.,0.).. (4.4, -0.1);
\draw[<-] (.7,0) -- (.7,-1);
\node [above] at (0.7,0) {$A$};
\draw[<-] (2.3,0) -- (2.3,-1);
\node [above] at (2.3,0) {$B$};
%\draw[gray,step=0.25] (-0.6,-3) grid (5, 5.3);
\end{tikzpicture}%}
\caption{Dependence of the potential energy $E_{p}$ of interaction of the molecules on the distance $r$ between their centres.}
\label{fig-69}
\end{marginfigure}

\tchr Qualitatively, the interaction of two molecules can be described by means of the curve illustrated in \figr{fig-69}. This curve shows the dependence of the potential energy $E_{p}$ of interaction of the molecules on the distance $r$ between their centres. At a sufficiently large distance between the molecules the curve $E_{p}(r)$ asymptotically approaches zero, i.e. the molecules practically cease to interact. As the molecules come closer together, the curve $E_{p}(r)$ turns downward. Then, when they are sufficiently close to one another, the molecules begin to repulse one another and curve $E_{p}(r)$ turns upward and $E$ continues to rise (this repulsion means that the molecules cannot freely penetrate into each other). As can be seen, the $E_{p}(r)$ curve has a characteristic minimum.

\stdb What is negative energy?

\tchr As we know, energy can be measured from any value. For instance, we can measure the potential energy of a stone from ground level of the given locality, or we can measure it from sea level, it makes no difference. In the given case, the zero point corresponds to the energy of interaction between molecules separated from each other at an infinitely large distance. Therefore, the negative energy of the molecule means that it is in a bound state (bound with another molecule). To ``free'' this molecule, it is necessary to add some energy to it to increase the energy of the molecule to the zero level. Assume that the molecule has a negative energy $e_{1}$ (see \figr{fig-69}). It is evident from the curve that in this case the molecule cannot get farther away from its neighbour than point $B$ or get closer than point $A$. In other words, the molecule will vibrate between points $A$ and $B$ in the field of the neighbouring molecule (more precisely, there will be relative vibration of two molecules forming a bound system).

In a gas molecules are at such great distances from one another on an average that they can be regarded as practically noninteracting. Each molecule travels freely, with relatively rare collisions. Each molecule participates in three types of motion: translatory, rotary (the molecule rotates about its own axis) and vibratory (the atoms in the molecule vibrate with respect to one another). If a molecule is monoatomic it will have only translatory motion.

In a crystal the molecules are so close together that they form a single bound system. In this case, each molecule vibrates in some kind of general force field set up by the interaction of the whole collective of molecules. Typical of a crystal as a common bound system of molecules is the existence of an ordered three-dimensional structure - the crystal lattice. The lattice points are the equilibrium positions of the separate molecules. The molecules accomplish their complex vibratory motions about these positions. It should be noted that in some cases when molecules form a crystal, they continue to retain their individuality to some extent. In these cases, distinction is to be made between the vibration of the molecule in the field of the crystal and the vibration of the atoms in the separate molecules. This phenomenon occurs when the binding energy of the atoms in the molecules is substantially higher than the binding energy of the molecules themselves in the crystal lattice. In most cases, however, the molecules do not retain their individuality upon forming a crystal so that the crystal turns out to be made up, not of separate molecules, but of separate atoms. Here, evidently, there is no intramolecular vibration, but only the vibration of the atoms in the field of the crystal. This, then, is the minimum amount of information that examinees should possess about atomic and molecular thermal motions in matter. Usually, when speaking about the nature of thermal motions in matter, examinees get no farther than saying it is a ``chaotic motion'', thus trying to cover up the lack of more detailed knowledge of thermal motion.

\stdb But you haven't said anything about the nature of the thermal motions of molecules in a liquid.

\tchr Thermal motions in a liquid are more involved than in other substances. A liquid occupying an intermediate position between gases and crystals exhibits, along with strong particle interaction, a considerable degree of disorder in its structure. The difficulty of dealing with crystals, owing to the strong interaction of the particles, is largely compensated for by the existence of an ordered structure-the crystal lattice. The difficulty of dealing with gases owing to the disordered position of the separate particles is compensated for by a practically complete absence of particle interaction. In the case of liquids, however, there are both kinds of difficulties mentioned above with no corresponding compensating factors. It can be said that in a liquid the molecules, as a rule, completely retain their individuality. A great diversity of motions exists in liquids: displacement of the molecules, their rotation, vibration of the atoms in the molecules and vibration of the molecules in the fields of neighbouring molecules. The worst thing is that all of these types of motion cannot, strictly speaking, be treated separately (or, as they say, in the pure form) because there is a strong mutual influence of the motions.

\stdb I can't understand how translational motion of the molecule can be combined with its vibration in the fields of neighbouring molecules.

\tchr Various models have been devised in which attempts were made to combine these motions. In one model, for instance, it was assumed that the molecule behaves as follows: it vibrates for a certain length of time in the field set up by its neighbours, then it takes a jump, passing over into new surroundings, vibrates in these surroundings, takes another jump, etc. Such a model is called the ``jump-diffusion model''.

\stdb It seems that is precisely the way in which atoms diffuse in crystals.

\tchr You are right. Only remember that in crystals this process is slower: jumps into a new environment occur considerably more rarely. There exists another model according to which a molecule in a liquid behaves as follows: it vibrates surrounded by its neighbours and the whole environment smoothly travels (``floats'') in space and is gradually deformed. This is called the ``continuous-diffusion model''.

\stdb You said that a liquid occupies an intermediate position between crystals and gases. Which of them is it closer to?

\tchr What do you think?

\stdb It seems to me that a liquid is closer to a gas.

\tchr In actuality, however, a liquid is most likely closer to a crystal. This is indicated by the similarity of their densities, specific heats and coefficients of volume expansion. It is also known that the heat of fusion is considerably less than the heat of vaporization. All these facts are evidence of the appreciable similarity between the forces of inter-particle bonding in crystals and in liquids. Another consequence of this similarity is the existence of elements of ordered arrangement in the atoms of a liquid. This phenomenon, known as ``short-range order'', was established in $X$-ray scattering experiments.

\stdb What do you mean by short-range order?

\tchr Short-range order is the ordered arrangement of a certain number of the nearest neighbours about any arbitrarily chosen atom (or molecule). In contrast to a crystal, this ordered arrangement with respect to the chosen atom is disturbed as we move away' from it, and does not lead to the formation of a crystal lattice. At short distances, however, it is quite similar to the arrangement of the atoms of the given substance in the solid phase. Shown in \figr{fig-70}~\drkgry{(a)} is the long-range order for a chain of atoms. It can be compared with the short-range order shown in \figr{fig-70}~\drkgry{(b)}.

\begin{figure}
\centering
%\fbox{
\begin{tikzpicture}
\draw[thick, gray] (0,0) -- (8,0);	
\foreach \x in {0,1,2,...,8}{
\draw (\x,0) circle (7pt);
\fill (\x,0) circle (2pt);}
\draw[fill] (4,0) circle (7pt);
\draw[thick, gray] (0,-1.5) -- (8,-1.5);	
\foreach \x in {0,1,2,...,8}{
\fill (\x,-1.5) circle (2pt);}
\draw (0,-1.68) circle (7pt);
\draw (1,-1.58) circle (7pt);
\draw (2,-1.45) circle (7pt);
\draw (3,-1.5) circle (7pt);
\draw[fill] (4,-1.5) circle (7pt);
\draw (5,-1.5) circle (7pt);
\draw (6,-1.43) circle (7pt);
\draw (7,-1.38) circle (7pt);
\draw (8,-1.33) circle (7pt);
\node [above] at (0,.6) {(a)};
\node [above] at (0,-1.2) {(b)};
\end{tikzpicture}%}
\caption{Long range order of crystals and short range order of liquids.}
\label{fig-70}
\end{figure}
The similarity between liquids and crystals has led to the term ``quasi-crystallinity'' of liquids.

\stdb But in such a case, liquids can evidently be dealt with by analogy with crystals.

\tchr I should warn you against misuse of the concept of quasi-crystallinity of liquids and attributing too much importance to it. Firstly, you must keep in mind that the liquid state corresponds to a wide range of temperatures, and the structural-dynamic properties of liquids cannot be expected to be the same (or even approximately the same) throughout this range. Near the critical state, a liquid should evidently lose all similarity to a solid and gradually transform to the gaseous phase. Thus, the concept of quasi-crystallinity of liquids may only be justified somewhere near the melting point, if at all. Secondly, the nature of the intermolecular interaction differs from one liquid to another. Consequently, the concept of quasi-crystallinity is not equally applicable to all liquids. For example, water is found to be a more quasi-crystalline liquid than molten metals, and this explains many of its special properties (see \chap{ch-19}).

\stdb I see now that there is no simple picture of the thermal motions of molecules in a liquid.

\tchr You are absolutely right. Only the extreme cases are comparatively simple. Intermediate cases are always complex.

\stda The physics entrance examination requirements include the question about the basis for the molecular-kinetic theory of matter. Evidently, one should talk about Brownian motion.

\tchr Yes, Brownian motion is striking experimental evidence substantiating the basic principles of the molecular-kinetic theory. But, do you know what Brownian motion actually is?

\stda It is thermal motion of molecules.

\tchr You are mistaken; Brownian motion can be observed with ordinary microscopes! It is motion of separate particles of matter bombarded by molecules of the medium in their thermal motion. From the molecular point of view these particles are macroscopic bodies. Nevertheless, by ordinary standards they are extremely small. As a result of their random uncompensated collisions with molecules, the Brownian particles move continuously in a haphazard fashion and thus move about in the medium, which is usually some kind of liquid.

\stdb But why must the Brownian particles be so small? Why don't we observe Brownian motion with appreciable particles of matter such as tea leaves in a glass of tea?

\begin{figure}
\centering
%\fbox{
\begin{tikzpicture}[>=latex,domain=0:1.5,scale=2.5]
	%\draw[gray,step=0.2] (-5,-5) grid (5, 5);
%	\clip (-1,-1) rectangle (3, 2.1);
	\clip (-0.2,-0.2) rectangle (2.1, 2.1);
	
	\draw[thick,Maroon] plot (\x,\x*\x)	;
	\draw[thick,SteelBlue] plot (\x,\x*\x*\x);
	\node[below right,Maroon] at (1.35,1.9 ) {$R^{2}$};
	\node[below,SteelBlue] at (1,1.9) {$R^{3}$};
		
	\draw[thick, ->, gray] (0,0) -- (2,0);
	\draw[thick, ->, gray] (0,0) -- (0,2);
	\node [left,gray] at (0,2) {$y$};
	\node [below left,gray] at (2.1,0) {$R$};
	\node [left,gray] at (0,0) {0};

\end{tikzpicture}%}
\caption{Effect of surface and volume relationships.}
\label{fig-71}
\end{figure}


\tchr There are two reasons for this. In the first place, the number of collisions of molecules with the surface of a particle is proportional to the area of the surface; the mass of the particle is proportional to its volume. Thus, with an increase in the size $R$ of a particle, the number of colIisions of molecules with its surface increases proportionally to $\hlred{R^{2}}$, while the mass of the particle which is to be displaced by the collision increases in proportion to $\hlblue{R^{3}}$. Therefore, as the particles increase in size it becomes more and more difficult for the molecules to push them about. To make this clear, I plotted two curves in \figr{fig-71}: $y=\hlred{R^{2}}$ and $y=\hlblue{R^{3}}$. You can readily see that the quadratic relationship predominates at small values of $R$ and the cubic relationship at large values. This means that surface effects predominate at small values of $R$ and volume effects at large values.

In the second place, the Brownian particle must be very small since its collisions with molecules are uncompensated, i.e. the number of collisions from the left and from the right in unit time should differ substantially. But the ratio of this difference in the number of collisions to the whole number of collisions will be the greater, the less the surface of the particle.

\stda What other facts substantiating the molecular-kinetic theory are we expected to know?

\tchr The very best substantiation of the molecular- kinetic theory is its successful application in explaining a great number of physical phenomena. For example, we can give the explanation of the pressure of a gas on the walls of a vessel containing it. The pressure $p$ is the normal component of the force $F$ acting on unit area of the walls. Since
\begin{equation}
F=m \left( \frac{\Delta v}{\Delta t} \right) =  \frac{\Delta (mv)}{\Delta t} 
\label{eq-100}
\end{equation}
to find the pressure we must determine the momentum transmitted to a unit area of the wall surface per unit time due to the blows with which the molecules of the gas strike the walls. 

Assume that a molecule of mass $m$ is travelling perpendicular to a wall with a velocity $v$. As a result of an elastic collision with the wall, the molecule reverses its direction of travel and flies away from the wall with a velocity of $v$. The change in the momentum of the molecule equals $\Delta (mv)=m \Delta v=2mv$. This momentum is transmitted to the wall. For the sake of simplicity we shall assume that all the molecules of the gas have the same velocity $v$ and six directions of motion in both directions along three coordinate axes (assume that the wall is perpendicular to one of these axes). Next, we shall take into account that in unit time only those molecules will reach the wall which are at a distance within $v$ from it and whose velocity is directed toward the wall. Since a unit volume of the gas contains $N/V$ molecules. in unit time $\dfrac{1}{6}\left(\dfrac{N}{V} \right)v$ molecules strike a unit area of the wall surface. Since each of these molecules transmits a momentum of $2mv$, as a result of these blows a unit area of the wall surface receives a momentum equal to $2mv \dfrac{1}{6}\left(\dfrac{N}{V} \right)v$. According to equation \eqref{eq-100}. this is the required pressure $p$. Thus
\begin{equation}%
p= \frac{2}{3}\frac{N}{V} \frac{mv^{2}}{2}
\label{eq-101}
\end{equation}
According to equation \eqref{eq-98}, we can replace the energy of the molecule $\dfrac{mv^{2}}{2}$ by the quantity $\dfrac{3}{2} kT$ [in reference to the translational motion of molecules, equation \eqref{eq-98} is valid for molecules with any number of atoms]. After this, equation \eqref{eq-101} can be rewritten as
\begin{equation}%
pV=NkT
\label{eq-102}
\end{equation}
Note that this result was obtained by appreciable simplification of the problem (it was assumed, for instance, that the molecules of the gas travel with the same velocity). However, theory shows that this result completely coincides with that obtained in a rigorous treatment. 

Equation \eqref{eq-102} is beautifully confirmed by direct measurements. It is good proof of the correctness of the concepts of the molecular-kinetic theory which were used for deriving equation \eqref{eq-102}.

Now let us discuss the phenomena of the evaporation and boiling of liquids on the basis of molecular-kinetic conceptions. \hlred{How do you explain the phenomenon of evaporation?}

\stda The fastest molecules of liquid overcome the attraction of the other molecules and fly out of the liquid.

\tchr What will intensify evaporation?

\stda Firstly, an increase in the free surface of the liquid, and secondly, heating of the liquid.

\tchr It should be remembered that evaporation is a two-way process: while part of the molecules leave the liquid, another part returns to it. Evaporation will be the more effective the greater the ratio of the outgoing molecules to the incoming ones. The heating of the liquid and an increase of its free surface intensify the escape of molecules from the liquid. At the same time, measures can be taken to reduce the return of molecules to the liquid. For example, if a wind blows across the surface of the liquid, the newly escaped molecules are carried away, thereby reducing the probability of their return. That is why wet clothes dry more rapidly in the wind. 

If the escape of molecules from a liquid and their return compensate each other, a state of dynamic equilibrium sets in, and the vapour above the liquid becomes saturated. In some cases it is useful to retard the evaporation process. For instance, rapid evaporation of the moisture in bread is undesirable. To prevent fast drying of bread it is kept in a closed container (bread box, plastic bag). This impedes the escape of the evaporated molecules, and a layer of saturated vapour is formed above the surface of the bread, preventing further evaporation of water from the bread.

Now, please explain the boiling process.

\stda The boiling process is the same as evaporation, but proceeds more intensively.

\tchr I don't like your definition of the boiling process at all. I should mention that many examinees do not understand the essence of this process. When a liquid is heated, the solubility of the gases it contains reduces. As a result, bubbles of gas are formed in the liquid (on the bottom and walls of the vessel). Evaporation occurs in these bubbles, they become filled with saturated vapour, whose pressure increases with the temperature of the liquid. At a certain temperature, the pressure of the saturated vapour inside the bubbles becomes equal to the pressure exerted on the bubbles from the outside (this pressure is equal to the atmospheric pressure plus the pressure of the layer of water above the bubble). Beginning with this instant, the bubbles rise rapidly to the surface and the liquid boils. As you can see, the boiling of a liquid differs essentially from evaporation. Note that \hlblue{evaporation takes place at any temperature, while boiling occurs at a definite temperature called the boiling point.} Let me remind you that \hlblue{if the boiling process has begun, the temperature of the liquid cannot be raised, no matter how long we continue to heat it. The temperature remains at the boiling point until all of the liquid has boiled away.}

It is evident from the above discussion that the boiling point of a liquid is depressed when the outside pressure reduces. In this. connection, let us consider the following problem. \hlred{A flask contains a small amount of water at room temperature. We begin to pump out the air above the water from the flask with a vacuum pump. What will happen to the water?}

\stda As the air is depleted, the pressure in the flask will reduce and the boiling point will be depressed. When it comes down to room temperature, the water will begin to boil.

\tchr Could the water freeze instead of boiling?

\stda I don't know. I think it couldn't.

\tchr It all depends upon the rate at which the air is pumped out of the flask. If this process is sufficiently slow, the water should begin to boil sooner or later. But if the air is exhausted very rapidly, the water should, on the contrary, freeze. As a result of the depletion of the air (and, with it, of the water vapour), the evaporation process is' intensified. Since in evaporation the molecules with the higher energies escape from the water, the remaining water will be cooled. If the air is exhausted slowly, the cooling effect is compensated for by the transfer of heat from the outside. As a result the temperature of the water remains constant. If the air is exhausted very rapidly, the cooling of the water cannot be compensated by an influx of heat from the outside, and the temperature of the water begins to drop. As soon as this happens, the possibility of boiling is also reduced. Continued rapid exhaustion of the air from the flask will lower the temperature of the water to the freezing point, and the unevaporated remainder of the water will be transformed into ice.

\end{dialogue}



\cleardoublepage
%%
%%\thispagestyle{empty}
%%\vspace*{-1.95cm}
%%\begin{figure*}[!ht]
%%\centering
%%\includegraphics[width=0.7\textwidth]{figs/sec/sec-h.pdf}
%%\end{figure*}
%%\vspace*{-.4cm}
%%%\begin{centering}
%%\begin{fullwidth}
%%%%\paragraph{}
%%{\textsf{\Large \hlred{Basically, modern physics is molecular physics. Hence it is especially important to obtain some knowledge of the fundamentals of the molecular-kinetic theory of matter, if only by using the simplest example of the ideal gas. The question of the peculiarity in the thermal expansion of water is discussed separately. The gas laws will be analysed in detail and will be applied in the solution of specific engineering problems.
%%}}}
%%\end{fullwidth}
%%
%\cleardoublepage
