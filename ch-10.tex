% !TEX root = tarasov-qasp-2.tex

\chapter{Can You Apply The Laws Of Conservation Of Energy And Linear Momentum?}
\label{ch-10}

\begin{dialogue}

\tchr To begin with I wish to propose several simple problems. 
\begin{description}[leftmargin=1cm]
\item[The first problem:] \hlred{Bodies slide without friction down two inclined planes of equal height $H$
but with two different angles of inclination $\alpha_{1}$ and $\alpha_{2}$. The initial velocity of the bodies equals zero. Find the velocities of the bodies at the end of their paths.}

\item[The second problem:] \hlred{We know that the formula expressing the final velocity of a body in terms of the acceleration and distance travelled $v = \sqrt{2as}$ refers to the case when there is no initial velocity. What will this formula be if the body has an initial velocity $v_{0}$? }

\item[The third problem:] \hlred{A body is thrown from a height $H$ with a horizontal velocity of $v_{0}$. Find its velocity when it reaches the ground.}

\item[The fourth problem:] \hlred{A body is thrown upward at an angle a to the horizontal with an initial velocity $v_{0}$. Find the maximum height reached in its flight.}
\end{description}

\stda I shall solve the first problem in the following way. We first consider one of the inclined planes, for instance the one with the angle of inclination $\alpha_{1}$. Two forces are applied to the body: the force of gravity $P$ and the bearing reaction $N_{1}$. We resolve the force $P$ into two components, one along the plane ($P \sin \alpha_{1}$) and the other perpendicular to it
($P \cos \alpha_{1}$). We then write the equations for the forces perpendicular to the inclined plane
\begin{equation*}%
P \cos \alpha_{1} - N_{1} = 0
\end{equation*}
and for the forces along the plane 
\begin{equation*}%
P \sin \alpha_{1}  = \frac{P a_{1}}{g}
\end{equation*}
where $a_{1}$ is the acceleration of the body. From the second equation we find that $a_{1} =g \sin \alpha_{1}$. The distance travelled by the body is $H/\sin \alpha_{1}$. Next, using the formula mentioned in the second problem, we find that the velocity at the end of
the path is
\begin{equation*}%
v_{1}  = \sqrt{a_{1}s_{1}} = \sqrt{\frac{2gH \sin \alpha_{1}}{\sin \alpha_{1}}} = \sqrt{2gH}
\end{equation*}
Since the final result does not depend upon the angle of inclination, it is also applicable to the second plane inclined at the angle $\alpha_{2}$.

To solve the second problem I shall make use of the well known kinematic relationships
\begin{equation*}%
\begin{split}
v & = v_{0} + at\\
s & = v_{0}t+ \frac{at^{2}}{2}
\end{split}
\end{equation*}
From the first equation we find that $t= (v - v_{0})/a$. Substituting this for $t$ in the second equation we obtain
\begin{equation*}%
s = \frac{ v_{0}(v - v_{0})}{a} + \frac{a}{2} \frac{(v - v_{0})^{2}}{a^{2}}
\end{equation*}
or
\begin{equation*}%
2sa = 2 v_{0}v - 2v_{0}^{2}+ v^{2} - 2 v_{0}v + v_{0}^{2}
\end{equation*}

from which $2sa=v^{2}-v_{0}^{2}$. The final result is
\begin{equation}%
v= \sqrt{2as+v_{0}^{2}}
\label{eq-48}
\end{equation}
To solve the third problem, I shall first find the horizontal $v_{1}$ and vertical $v_{2}$ components of the final velocity. Since the body travels at uniform velocity in the horizontal direction, $v_{1} = v_{0}$. In the vertical direction the body travels with acceleration $g$ but has no initial velocity. Therefore, we can use the formula $v_{2}=\sqrt{2gH}$. Since the sum of the squares of the sides of a right triangle equals, the square of the hypotenuse, the final answer is 
\begin{equation}
v=\sqrt{v_{1}^{2}+v_{2}^{2}} =\sqrt{v_{0}^{2}+2gH}
\label{eq-49}
\end{equation}


The fourth problem has already been discussed in \chap{ch-05}. It is necessary to resolve the initial velocity into the horizontal ($v_{0} \cos \alpha$) and vertical ($v_{0} \sin \alpha$) components. Then we consider the vertical motion of the body and, first of all, we find the time $t_{1}$ of ascent from the formula for the dependence of the velocity on time in uniformly decelerated motion $(v_{v}=v_{0} \sin \alpha -gt)$, taking into account that at $t=t_{1}$ the vertical velocity of the body vanishes. Thus $v_{0} \sin \alpha -gt_{1} =0$, from which $t_{1} = (v_{0}/g) \sin \alpha$. The time $t_{1}$ being known, we find the height $H$ reached from the formula of the dependence of the distance travelled on time in uniformly decelerated motion. Thus
\begin{equation*}%
H = v_{0} t_{1} \sin \alpha - \frac{gt_{1}^{2}}{2} =  \frac{v_{0}^{2}}{2g}\sin \alpha
\end{equation*}
\tchr In all four cases you obtained the correct answers. I am not, however, pleased with the way you solved these problems. They could all have been solved much simpler if you had used the law of conservation of energy. You can see for yourself. 
\begin{description}[leftmargin=1cm, style=nextline]

\item[First problem.] The law of conservation of energy is of the form $mgH = mv^{2}/2$ (the potential energy of the body at the top of the inclined plane is equal to its kinetic energy at the bottom). From this equation we readily find the velocity of the body at the bottom
\begin{equation*}
v = \sqrt{2gH}
\end{equation*}


\item[Second problem.] In this case, the law of conservation of energy is of the form 
$mv_{0}^{2}/2+mas= mv^{2}/2$, where $mas$ is the work done by the forces in imparting the acceleration $a$ to the body. This leads immediately to $v_{0}^{2} + 2as = v_{2}$ or, finally, to 
\begin{equation*}
v=\sqrt{2as+v_{0}^{2}}
\end{equation*}

\item[Third problem.] We write the law of conservation of energy as $mgH+mv_{0}^{2}2= mv^{2}/2$. Then the result is
\begin{equation*}
v=\sqrt{2gH+v_{0}^{2}}
\end{equation*}

\item[Fourth problem.] At the point the body is thrown its energy equals $mv_{0}^{2}/2$. At the top point of its trajectory, the energy of the body is $mgH+mv_{1}^{2}/2$. Since the velocity $v_{1}$ at the top point equals $v_{0} \cos \alpha$, then, using the law of conservation of energy
\begin{equation*}
\frac{mv_{0}^{2}}{2} = mgH + \frac{mv_{0}^{2}}{2} \cos^{2} \alpha
\end{equation*}
we find that
\begin{equation*}
H=\left(\frac{v_{0}^{2}}{2g} \right) \left( 1 - \cos^{2} \alpha \right)
\end{equation*}
or, finally
\begin{equation*}
H=\left(\frac{v_{0}^{2}}{2g} \right) \sin^{2} \alpha 
\end{equation*}
\end{description}

\stda Yes, it's quite clear to me that these problems can be solved in a much simpler way. It didn't occur to me to use the law of conservation of energy.

\tchr Unfortunately, examinees frequently forget about this law. As a result they begin to solve such problems by more cumbersome methods, thus increasing the probability of errors. My advice is: \hlblue{make more resourceful and extensive use of the law of conservation of energy.} This poses the question: how skillfully can you employ this law?

\stda It seems to me that no special skill is required; the law of conservation of energy as such is quite simple.

\tchr The ability to apply a physical law correctly is not determined by its complexity or simplicity. Consider an example. \hlred{Assume that a body travels at uniform velocity in a circle in a horizontal plane. No friction forces operate. The body is subject to a centripetal force. What is the work done by this force in one revolution of the body?}

\stda Work is equal to the product of the force by the distance through which it acts. Thus, in our case, it equals $(mv^{2}/R) 2 \pi R = 2 \pi mv^{2}$, where $R$ is the radius of the circle and $m$ and $v$ are the mass and velocity of the body.

\tchr According to the law of conservation of energy; work cannot completely disappear. What has become of the work you calculated?

\stda It has been used to rotate the body.

\tchr I don't understand. State it more precisely.

\stda It keeps the body on the circle.

\tchr Your reasoning is wrong. No work at all is required to keep the body on the circle.

\stda Then I don't know how to answer your question.

\tchr Energy imparted to a body can be distributed, as physicists say, among the following ``channels'':
\begin{enumerate*}[label=(\arabic*)] 
\item increasing the kinetic energy of the body; 
\item increasing its potential energy; 
\item work performed by the given body on other bodies; and 
\item heat evolved due to friction. 
\end{enumerate*}
Such is the general principle which not all examinees understand with sufficient clarity. Now consider the work of the centripetal force. The body travels at a constant velocity and therefore its kinetic energy is not increased. Thus the first channel is closed. The body travels in a horizontal plane and, consequently, its potential energy is not changed. The second channel is also closed. The given body does not perform any work on other bodies, so that the third channel is closed. Finally, all kinds of friction have been excluded. This closes the fourth and last channel.

\stda But then there is simply no room for the work of the centripetal force, or is there?

\tchr As you see, none. It remains now for you to declare your position on the matter. Either you admit that the law your this your of conservation of energy is not valid, and then all troubles are gone, or you proceed from the validity of law and then \ldots{}. However, try to find the way out of
difficulties.

\stda I think that it remains to conclude that \hlblue{the centripetal force performs no work whatsoever.}

\tchr That is quite a logical conclusion. I want to point out that it is the direct consequence of the law of conservation of energy.

\stdb All this is very well, but what do we do about the formula for the work done by a body?

\tchr In addition to the force and the distance through which it acts, this formula should also contain the cosine of the angle between the direction of the force and the velocity
$ A = F s \cos \alpha$. In the given case, $\cos \alpha = 0$.

\stda Oh yes, I entirely forgot about this cosine.

\tchr I want to propose another example. Consider communicating vessels connected by a narrow tube with a stopcock. Assume that at first all the liquid is in the left vessel and its height is $H$ (\figr{fig-43}\drkgry{(a)}). Then we open the stopcock and the liquid flows from the left into the right vessel. The flow ceases when there is an equal level of $H/2$ in each vessel (\figr{fig-43}\drkgry{(b)}).

Let us calculate the potential energy of the liquid in the initial and final positions. For this we multiply the weight of the liquid in each vessel by one half of the column of liquid. In the initial position the potential energy equalled $PH/2$, and in the final one it is 
\begin{equation*}
 \left( \frac{P}{2} \right)  \left( \frac{H}{4} \right) +  \left( \frac{P}{2} \right)   \left( \frac{H}{4} \right) =   \left( \frac{PH}{4} \right).
\end{equation*}
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-10/fig-043a.pdf}
\caption{Problem with potential energy of a liquid column in a communicating vessel.}
\label{fig-43}
\end{marginfigure}

Thus in the final state, the potential energy of the liquid turns out to be only one half of that in the
initial state. Where has one half of the energy disappeared to?

\stda I shall attempt to reason as you advised. The potential energy $\dfrac{PH}{4} $ could be used up in performing work on other bodies, on heat evolved in friction and on the kinetic energy of the liquid itself. Is that right?

\tchr Quite correct. Continue.

\stda In our case, the liquid flowing from one vessel to the other does not perform any work on other bodies. The liquid has no kinetic energy in the final state because it is in a state of rest. Then, it remains to conclude that one half of the potential energy has been converted into heat
evolved in friction. True, I don't have a very clear idea of what kind of friction it is.

\tchr You reasoned correctly and came to the right conclusion. I want to add a few words on the nature of friction. One can imagine that the liquid is divided into layers, each characterizing a definite rate of flow. \hlblue{The closer the layer to the walls of the tube, the lower its velocity.} There is an exchange of molecules between the layers, as a result of which molecules with a higher velocity of ordered motion find themselves among molecules with a lower velocity of ordered
motion, and vice versa. As a result, the ``faster'' layer has an accelerating effect on the ``slower'' layer and, conversely, the ``slower'' layer has a decelerating effect on the ``faster'' layer.

This picture allows us to speak of the existence of a peculiar internal friction between the layers. Its effect is stronger with a greater difference in the velocities of the layers in the middle part of the tube and near the walls. Note that the velocity of the layers near the walls is influenced by the kind of interaction between the molecules of the liquid and those of the walls. If the liquid wets the tube then the layer directly adjacent to the wall is actually stationary.

\stda Does this mean that in the final state the temperature of the liquid should be somewhat higher than in the initial state?

\tchr Yes, exactly so. Now we shall change the conditions of the problem to some extent. Assume that there is no interaction between the liquid and the tube walls. Hence, all the layers will flow at the same velocity and there will be no internal friction. How then will the liquid flow from one
vessel to the other?

\stda Here the potential energy will be reduced owing to the kinetic energy acquired by the liquid. In other words, the state illustrated in \figr{fig-43}\drkgry{(b)} is not one of rest. The liquid will continue to flow from the left vessel to the right one until it reaches the state shown in \figr{fig-43}\drkgry{(c)}. In this state the potential energy is again the same as in the initial state \figr{fig-43}\drkgry{(a)}.

\tchr What will happen to the liquid after this?

\stda The liquid will begin to flow back in the reverse direction, from the right vessel to the left one. As a result, the levels of the liquid will fluctuate in the communicating vessels.

\tchr Such fluctuations can be observed, for instance, in communicating glass vessels containing mercury. We know that mercury does not wet glass. Of course these fluctuations will be damped in the course of time, since it is impossible to completely exclude the interaction between the
molecules of the liquid and those of the tube walls.

\stda I see that the law of conservation of energy can be applied quite actively.

\tchr Here is another problem for you. \hlred{A bullet of mass $m$, travelling horizontally with a velocity $v_{0}$, hits a wooden block of mass $M$, suspended on a string, and sticks in the block. To what height $H$ will the block rise, after the bullet hits it, due to deviation of the string from the equilibrium position \drkgry{(\figr{fig-44})}?}
\begin{figure}[!ht]
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-10/fig-044a.pdf}
\caption{A bullet hits a block of wood and sticks in it. The problem is to find the height to which the block will rise after the impact.}
\label{fig-44}
\end{figure}

\stda We shall denote by $v_{1}$ the velocity of the block with the bullet immediately after the bullet hits the block. To find this velocity we make use of the law of conservation of energy. Thus
\begin{equation}%
\frac{m v_{o}^{2}}{2} = \left( m + M \right) \frac{v_{1}^{2}}{2}
\label{eq-50}
\end{equation}
from which
\begin{equation}%
v_{1} = v_{o} \sqrt{\frac{m}{ m + M}} 
\label{eq-51}
\end{equation}
This velocity being known, we find the sought-for height $H$ by again resorting to the law of conservation of energy
\begin{equation}%
(m+ M) gH = (m + M) 	\frac{v_{1}^{2}}{2}
\label{eq-52}
\end{equation}
Equations \eqref{eq-50} and \eqref{eq-52} can be combined
\begin{equation*}%
(m+ M) gH = \frac{mv_{0}^{2}}{2}
\end{equation*}
from which
\begin{equation}%
H = \frac{m}{(m + M)} 	\frac{v_{0}^{2}}{2g}
\label{eq-53}
\end{equation}
\tchr (to \textsc{Student~B}) What do you think of this solution?

\stdb I don't agree with it. We were told previously that in such cases the law of conservation of momentum is to be used. Therefore, instead of equation \eqref{eq-50} I would have used
a different relationship 
\begin{equation}%
mv_{0} = (m+M) v_{1}
\label{eq-54}
\end{equation}
(the momentum of the bullet before it hits the block is equal to the momentum of the bullet and block afterward). From this it follows that
\begin{equation}%
v_{1} = v_{0} \frac{m}{(m + M)}
\label{eq-55}
\end{equation}
If we now use the law of conservation of energy \eqref{eq-52} and substitute the result of equation \eqref{eq-55} into \eqref{eq-52} we obtain
\begin{equation}%
H = \left(\frac{m}{m + M} \right)^{2}	\frac{v_{0}^{2}}{2g}
\label{eq-56}
\end{equation}
\tchr We have two different opinions and two results. The point is that in one case the law of conservation of kinetic energy is applied when the bullet strikes the block, and in the other case, the law of conservation of momentum. Which is correct? (to \textsc{Student~A}): What can you say to justify your
position?

\stda It didn't occur to me to use the law of conservation of momentum.

\tchr (to \textsc{Student~B}): And what do you say?

\stdb I don't know how to substantiate my position. I remember that \hlblue{in dealing with collisions, the law of conservation of momentum is always valid, while the law of conservation of energy does not always hold good.} Since in the given case these laws lead to different results, my solution is evidently correct.

\tchr As a matter of fact, it is indeed quite correct. It is, however, necessary to get a better insight into the matter. A collision after which the colliding bodies travel stuck together (or one inside the other) is said to be a ``completely inelastic collision''. Typical of such impacts is the presence of permanent set in the colliding bodies, as a result of which a certain amount of heat is evolved. Therefore, equation \eqref{eq-50}, referring only to the kinetic energy of bodies, is inapplicable. In our case, it is necessary to employ the law of conservation of momentum \eqref{eq-54} to find the velocity of the box with the bullet after the impact.

\stda Do you mean to say that the law of conservation of energy is not valid for a completely inelastic collision? But this law is universal.

\tchr There is no question but that the law of conservation of energy is valid for a completely inelastic collision as well. The kinetic energy is not conserved after such a collision. I specifically mean the kinetic energy and not the whole energy. Denoting the heat evolved in collision by $Q$, we can write the following system of laws of conservation referring to the completely inelastic collision discussed above 
\begin{equation}%
\left.
\begin{aligned}
mv_{0}  & = (m + M) v_{1} \\
\frac{mv_{0}^{2}}{2} & = \frac{(m+M) v_{1}^{2}}{2} + Q
\label{eq-57}
\end{aligned}
\right\}
\end{equation}
Here the first equation is the law of conservation of momentum, and the second is the law of conservation of energy (including not only mechanical energy, but heat as well). The system of equations \eqref{eq-57} contains two unknowns: $v_{1}$ and $Q$. After determining $v_{1}$ from the first equation, we can use the second equation to find the evolved heat $Q$
\begin{equation}%
%\left.
%\begin{split}
Q = \frac{mv_{0}^{2}}{2} - \frac{(m+M)m^{2}v_{0}^{2}}{2(m+M)^{2}} =  \frac{mv_{0}^{2}}{2} \left( 1 - \frac{m}{m+M}\right)
\label{eq-58}
%\end{split}
%\right\}
\end{equation}
It is evident from this equation that the larger the mass $M$, the more energy is converted into heat. In the limit, for an infinitely large mass. $M$, we obtain $Q=m v_{0}^{2}/2$, i.e. all the kinetic energy is converted into heat. This is quite natural: assume that the bullet sticks in a wall.

\stda Can there be an impact in which no heat is evolved?

\tchr Yes, such collisions are possible. They are said to be ``perfectly elastic''. For instance, the impact of two steel balls can be regarded as perfectly elastic with a fair degree of approximation. Purely elastic deformation of the balls occurs and no heat is evolved. After the collision, the balls return to their original shape.

\stda You mean that in a perfectly elastic collision, the law of conservation of energy becomes the law of conservation of kinetic energy?

\tchr Yes, of course.

\stda But in this case I cannot understand how you can reconcile the laws of conservation of momentum and of energy. We obtain two entirely different equations for the velocity after impact. Or, maybe, the law of conservation of momentum is not valid for a perfectly elastic collision.

\tchr Both conservation laws are valid for a perfectly elastic impact: for momentum and for kinetic energy. You have no reason to worry about the reconciliation of these laws because after a perfectly elastic impact, the bodies fly apart at different velocities. \hlblue{Whereas after a completely inelastic impact the colliding bodies travel at the same velocity (since they stick together), after an elastic impact each body travels at its own definite velocity.} Two unknowns require two equations. 

Let us consider an example. Assume that a body of mass $m$ travelling at a velocity $v_{0}$ elastically collides with a body of mass $M$ at rest. Further assume that as a result of the impact the incident body bounces back. We shall denote the velocity of body m after the collision by $v_{1}$ and that of body $M$ by $v_{2}$. Then the laws of conservation of momentum and energy can be written in the form
\begin{equation}%
\left.
\begin{aligned}
mv_{0} & =Mv_{2}-  mv_{1} \\
\frac{mv_{0}^{2}}{2} & = \frac{mv_{2}^{2}}{2} + \frac{mv_{1}^{2}}{2}
\label{eq-59}
\end{aligned}
\right\}
\end{equation}
Note the minus sign in the first equation. It is due to our assumption that the incident body bounces back.

\stdb But you cannot always know beforehand in which direction a body will travel after the impact. Is it impossible for the body $m$ to continue travelling in the same direction but at a lower velocity after the collision?

\tchr That is quite possible. In such a case, we shall obtain a negative velocity $v_{1}$ when solving the system of equations \eqref{eq-59}.

\stdb I think that the direction of travel of body $m$ after the collision is determined by the ratio of the masses $m$ and $M$.

\tchr Exactly. If $m<M$, body $m$ will bounce back; at $m=M$, it will be at rest after the collision; and at $m>M$, it will continue its travel in the same direction but at a lower velocity. In the general case, however, you need not worry about the direction of travel. It will be sufficient to assume some direction and begin the calculations. \hlblue{The sign of the answer will indicate your mistake, if any.}

\stdb We know that upon collision the balls may fly apart at an angle to each other. We assumed that motion takes place along a single straight line. Evidently, this must have been a special case.

\tchr You' are right. We considered what is called a central collision in which the balls travel before and after the impact along a line passing through their centres. The more general case of the off-centre collision will be dealt with later. For the time being, I'd like to know if everything is quite clear.

\stda I think I understand now. As I see it, in any collision (elastic or inelastic), two laws of conservation are applicable: of momentum and of energy. Simply the different nature of the impacts leads to different equations for describing the conservation laws. \hlblue{In dealing with inelastic collisions, it is necessary to take into account the heat evolved on impact.}

\tchr Your remarks are true and to the point.

\stdb So far as I understand it, completely elastic and perfectly inelastic collisions are the two extreme cases. Are they always suitable for describing real cases?

\tchr You are right in bringing up this matter. The cases of collision we have considered are extreme ones. In real collisions some amount of heat is always generated (no ideally elastic deformation exists) and the colliding bodies may fly apart with different velocities. In many cases, however, real
collisions are described quite well by means of simplified models: completely elastic and perfectly inelastic collisions.

Now let us consider an example of an off-centre elastic collision. \hlred{A body in the form of an inclined plane with a \ang{45} angle of inclination lies on a horizontal plane. A ball of mass $m$, flying horizontally with a velocity $v_{0}$, collides with the body (inclined plane), which has a mass of $M$. As a result of the impact, the ball bounces vertically upward and the body $M$ begins to slide without friction along the horizontal plane. Find the velocity with which the ball begins its vertical travel after the collision.} \drkgry{(\figr{fig-45}).}

Which of you wishes to try your hand at this problem?

\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-10/fig-045a.pdf}
\caption{A ball collides with another body at rest in the form of an inclined plane. The problem is to find the velocity of the ball after the impact.}
\label{fig-45}
\end{figure}

\stdb Allow me to. Let us denote the sought-for velocity of the ball by $v_{1}$ and that of body $M$ by $v_{2}$. Since the collision is elastic, I have the right to assume that the kinetic energy is conserved. Thus
\begin{equation}% 
%\left.
%\begin{split}
%mv_{0} & =Mv_{2}-  mv_{1} \\
\frac{mv_{0}^{2}}{2} = \frac{mv_{1}^{2}}{2} + \frac{mv_{2}^{2}}{2} 
\label{eq-60}
%\end{split}
%\right\}
\end{equation}
I need one more equation, for which I should evidently use the law of conservation of momentum. I shall write it in the form 
\begin{equation}%
%\left.
%\begin{split}
mv_{0} =Mv_{2} +  mv_{1} 
\label{eq-61}
%\end{split}
%\right\}
\end{equation}
True, I'm not so sure about this last equation because velocity $v_{1} $ is at right angles to velocity $v_{2}$.

\tchr Equation \eqref{eq-60} is correct. Equation \eqref{eq-61} is incorrect, just as you thought. You should remember that the law of conservation of momentum is a vector equation, since the momentum is a vector quantity having the same direction as the velocity. True enough, when all the velocities are directed along a single straight line, the vector equation can be replaced by a scalar one. That is precisely what happened when we discussed central collisions. \hlblue{In the general case, however, it is necessary to resolve all velocities in mutually perpendicular directions and to write the law of conservation of momentum for each of these directions separately} (if the problem is considered in a plane, the vector equation can be replaced by two scalar equations for the projections of the momentum in the two mutually perpendicular directions).

For the given problem we can choose the horizontal and vertical directions. For the horizontal direction, the law of conservation of momentum is of the form
\begin{equation}%
mv_{0}= Mv_{2}
\label{eq-62}
\end{equation}
From equations \eqref{eq-60} and \eqref{eq-62} we find the velocity
\begin{equation*}%
v_{1} = v_{0}\sqrt{\frac{M-m}{m}}
%\label{eq-62}
\end{equation*}
\stdb What do we do about the vertical direction?

\tchr At first sight, it would seem that the law of conservation of momentum is not valid for the vertical direction. Actually it is. Before the impact there were no vertical velocities; after the impact, there is a momentum $mv_{1}$, directed vertically upwards. We can readily see that still another body participates in the problem: the earth. If it was not for the earth, body $M$ would not travel horizontally
after the collision. Let us denote the mass of the earth by $M_{\Earth}$ and the velocity it acquires as a result of the impact by $v_{\Earth}$.

The absence of friction enables us to treat the interaction between the body $M$ and the earth as taking place only in the vertical direction. In other words, the velocity $v_{\Earth}$ of the earth is directed vertically downwards. Thus, the participation of the earth in our problem doesn't change the form of equation \eqref{eq-62}, but leads to an equation which describes the law of conservation of momentum for the vertical direction
\begin{equation}%
%\left.
%\begin{split}
mv_{1}  - M_{\Earth}v_{\Earth} = 0   
\label{eq-63}
%\end{split}
%\right\}
\end{equation}
\stdb Since the earth also participates in this problem it will evidently be necessary to correct the energy relation \eqref{eq-60}.

\tchr Just what do you propose to do to equation \eqref{eq-60}?

\stdb I wish to add a term concerning the motion of the earth after the impact
\begin{equation}%
%\left.
%\begin{split}
%mv_{0} & =Mv_{2}-  mv_{1} \\
\frac{mv_{0}^{2}}{2} = \frac{mv_{1}^{2}}{2} + \frac{Mv_{2}^{2}}{2} +  \frac{M_{\Earth}v_{\Earth}^{2}}{2} 
\label{eq-64}
%\end{split}
%\right\}
\end{equation}

\tchr Your intention is quite logical. There is, however, no need to change equation (\ref{eq-60}). As a matter of fact, it follows from equation (\ref{eq-63}) that the velocity of the earth is
\begin{equation*}
v_{\Earth} = \frac{mv_{1}}{M_{\Earth}}
\end{equation*}
Since the mass $M_{\Earth}$ is practically infinitely large, the velocity $v_{\Earth}$ of the earth after the impact is practically equal to zero. Now, let us rewrite the term $M_{\Earth}v_{\Earth}^{2}/2 $ in equation \eqref{eq-64} to obtain the form $(M_{\Earth}v_{\Earth})v_{\Earth}/2$. The quantity $M_{\Earth}v_{\Earth}$ in this product has, according to equation (\ref{eq-63}), a finite value. If this value is multiplied by zero (in the given case by $v_{\Earth}$) the product is also zero. 

From this we can conclude that the earth participates very peculiarly in this problem. \hlblue{It acquires a certain momentum, but at the same time, receives practically no energy.} In other words, it participates in the law of conservation of momentum, but does not participate in the law of conservation of energy. This circumstance is especially striking evidence of the fact that \hlblue{the laws of conservation of energy and of momentum are essentially different, mutually independent laws.}
\end{dialogue}
\section*{Problems}
\label{prob-10}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume*=problems]
\item A body with a mass of \SI{3}{\kilogram} falls from a certain height with an initial velocity of \SI{2}{\meter\per\second}, directed vertically downward. Find the work done to overcome the forces of resistance during \SI{10}{\second} if it is known that the body acquired a velocity of \SI{50}{\meter\per\second} at the end of the \SI{10}{\second} interval. Assume that the force of resistance is constant.

\item A body slides first down an inclined plane at an angle of \ang{30} and then along a horizontal surface. Determine the coefficient of friction if it is known that the body slides along the horizontal surface the same distance as along the inclined plane.

\item Calculate the efficiency of an inclined plane for the case when a body slides off it at uniform velocity.

\item A ball of mass $m$ and volume $V$ drops into water from a height $H$, plunges to a depth $h$ and then jumps out of the water (the density of the ball is less than that of water). Find the resistance of the water (assuming it to be constant) and the height $h_{1}$ to which the ball ascends after jumping
out of the water. Neglect air resistance. The density of water is denoted by $\rho_{w}$.

\item A railway car with a mass of 50 tons, travelling with a velocity of \SI{12}{\kilo\meter\per\hour}, runs into a flatcar with a mass of 30 tons standing on the same track. Find the velocity of joint travel of the railway car and flatcar directly after the automatic coupling device operates. Calculate the distance travelled by the two cars after being coupled if the force of resistance is 5 per cent of the weight.

\item A cannon of mass $M$, located at the base of an inclined plane, shoots a shell of mass $m$ in a horizontal direction with an initial velocity $v_{0}$. To what height does the cannon ascend the inclined plane as a result of recoil if the angle of inclination of the plane is $\alpha$ and the coefficient of friction between the cannon and the plane is $k$?

\item Two balls of masses $M$ and $2M$ are hanging on threads of length $l$ fixed at the same point. The ball of mass $M$ is pulled to one side through an angle of $\alpha$ and is released after imparting to it a tangential velocity of $v_{0}$ in the direction of the equilibrium position. To what height will the balls rise after collision if: 
\begin{enumerate*}
\item the impact is perfectly elastic, and
\item if it is completely inelastic (the balls stick together as a result of the impact)?
\end{enumerate*}
\item A ball of mass $M$ hangs on a string of length $l$. A bullet of mass $m$, flying horizontally, hits the ball and sticks in it. At what minimum velocity must the bullet travel so that the ball will make one complete revolution in a vertical plane?

\item Two wedges with angles of inclination equal to \ang{45} and each of mass $M$ lie on a horizontal plane (\figr{fig-46}). A ball of mass $m$ ($m \ll M$) drops freely from the height $H$. It first strikes one wedge and then the other and bounces vertically upward. Find the height to which the ball bounces. Assume that both impacts are elastic and that there is no friction between the wedges and the plane.
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-10/fig-046a.pdf}
\caption{A ball strikes one of the two edges after being dropped from a height. First it strikes the second wedge and then goes vertically up. The problem is to find the height to which the ball rises.}
\label{fig-46}
\end{marginfigure}

\item A wedge with an angle of \ang{30} and a mass $M$ lies on a horizontal plane. A ball of mass $m$ drops freely from the height $H$, strikes the wedge elastically and bounces away at an angle of \ang{30} to the horizontal. To what height does the ball ascend? Neglect friction between the wedge and the horizontal plane.
\end{enumerate}

%\cleardoublepage
%\thispagestyle{empty}
%\vspace*{2cm}
%
%\begin{figure*}
%\centering
%\includegraphics[width=0.65\linewidth]{sec-e.pdf}
%%\caption{Velocity \emph{vs} time graph for the function shown in Figure \ref{fig-05}.}
%%\label{fig-06}
%\end{figure*}
%%\addtocounter{figure}{1}
%% For incrementing the figure numbers for figures without caption or numbering



\cleardoublepage

\thispagestyle{empty}
\vspace*{-1.5cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.73\textwidth]{figs/sec/tarasov-qasp-sec-06.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{The world about us is full of vibrations and waves. Remember this when you study the branch of physical science devoted to these phenomena. Let us discuss harmonic vibrations and, as a special case, the vibrations of a mathematical pendulum. We shall analyse the behaviour of the pendulum in non-inertial frames of reference.
}}}
\end{fullwidth}
