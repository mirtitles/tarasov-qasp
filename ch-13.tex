% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{Can You Use The Force Resolution Method Efficiently?}
\label{ch-13}

\begin{dialogue}
%
\tchr In solving mechanical problems it is frequently necessary to resolve forces. Therefore, I think it would be useful to discuss this question in somewhat more detail. First let us recall the main rule: to resolve a force into any two directions it is necessary to pass two straight lines through the head and two more through the tail of the force vector, each pair of lines being parallel to the respective directions of resolution. As a result we obtain a parallelogram whose sides are the components of the given force. This rule is illustrated in \figr{fig-54} in which force $F$ is resolved in two directions: $AA_{1}$ and $BB_{1}$. 
\begin{marginfigure}[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-13/fig-054a.pdf}
\caption{Illustration for resolution of forces resulting in a parallelogram.}
\label{fig-54}
\end{marginfigure}
Let us consider several problems in which force resolution is the common approach. The first problem is illustrated in \figr{fig-55}: \hlred{we have two identical loads $P$ suspended each from the middle of a string. The strings sag due to the loads and make angles of $\alpha_{1}$ and $\alpha_{2}$ with the horizontal. Which of the strings is subject to greater tension?}

\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-13/fig-055a.pdf}
\caption{Anaysing the motion of a pendulum.}
\label{fig-55}
\end{marginfigure}
\stda I can resolve the weight of each load on the same drawing in directions parallel to the branches of the strings. From this resolution it follows that the tension in the string is \\
\begin{equation*}
T=\frac{P}{2 \sin \alpha}
\end{equation*}

Thus, the string which sags less is subject to greater tension.

\tchr Quite correct. Tell me, \hlred{can we draw up the string so tightly that it doesn't sag at all when the load is applied?}

\stda And why not?

\tchr Don't hurry to answer. Make use of the result you just obtained.

\stda Oh yes, I see. The string cannot be made so taut that there is no sag. The tension in the string increases with a decrease in angle $\alpha$. However strong the string, it will be broken by the tension when angle $\alpha$ becomes sufficiently small.

\tchr Note that the sagging of a string due to the action of a suspended load results from the elastic properties of the string causing its elongation. \hlblue{If the string could not deform (elongate) no load could be hung from it.} This shows that in construction engineering, the strength analysis of various structures is closely associated with their capability to undergo elastic deformations (designers are wont to say that the structure must ``breathe''). \hlblue{Exceedingly rigid structures are unsuitable since the stresses developed in them at small deformations may prove to be excessively large and lead to failure.} Such structures may even fail under their own weight.

If we neglect the weight of the string in the preceding problem, we can readily find the relationship between the angle $\alpha$ of sag of the string and the weight $P$ of the load. To do this we make use of Hooke's law for elastic stretching of a string or wire (see Problem No. 35). 

Consider another example. There' is a Russian proverb, ``a wedge is driven out by a wedge'' (the English equivalent being ``like cures like''). This can be demonstrated by applying the method of force resolution \drkgry{(\figr{fig-56}~(a))}. 
\hlred{Wedge 1 is driven out of a slot by driving wedge 2 into the same slot, applying the force $F$. Angles $\alpha$ and $\beta$ are given. Find the force that acts on wedge 1 and enables it to be driven out of the slot.}

\stda I find it difficult to solve this problem.

\tchr Let us begin by resolving force $F$ into components in the horizontal direction and in a direction perpendicular to side $AB$ of wedge 2. The components obtained are denoted by $F_{1}$ and $F_{2}$ \drkgry{(\figr{fig-56}~(b))}. Component $F_{2}$ is counterbalanced by the reaction of the left wall of the slot; component $F_{1}$  equal to $F/\tan \alpha$, will act on wedge 1. Next we resolve this force into components in the vertical direction and in a direction perpendicular to the side $CD$ of wedge 1. The respective components are $F_{3}$ and $F_{4}$ \drkgry{(\figr{fig-56}~(c))}. Component $F_{4}$ is counterbalanced by the reaction of the right wall of the slot, while component $F_{3}$ enables wedge 1 to be driven out of the slot. This is the force we are seeking. It can readily be seen that it equals
\begin{equation*}%
F_{1} \tan \beta = F \left( \frac{\tan \alpha}{\tan \beta} \right)
\end{equation*}
\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-13/fig-056a.pdf}
\caption{Anaysing the motion of a pendulum.}
\label{fig-56}
\end{marginfigure}


Let us now consider a third example, illustrated in \drkgry{(\figr{fig-57}~(a))}. 
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-13/fig-057a.pdf}
\caption{Anaysing the motion of a pendulum.}
\label{fig-57}
\end{marginfigure}
\emph{Two weights, $P_{1}$ and $P_{2}$, are suspended from a string so that the portion of the string between them is horizontal. Find angle $\beta$ (angle $\alpha$ being known) and the tension in each portion of the string ($T_{AB}$, $T_{BC}$ and $T_{CD}$).} 

This example resembles the preceding one with the wedges.

\stda First I shall resolve the weight $P_{1}$ into force components in the directions $AB$ and $BC$ \drkgry{(\figr{fig-57}~(b))}. From this resolution we find that 
\begin{equation*}%
T_{AB} = \frac{P_{1}}{\sin \alpha} \, \, \text{and} \, \, T_{BC} = \frac{P_{1}}{\tan \alpha}
\end{equation*}
Thus we have already found the tension in two portions of the string. Next I shall resolve the weight $P_{2}$ into components in the directions $BC$ and $CD$ \drkgry{(\figr{fig-57}~(c))}. From this resolution we can write the equations: 
\begin{equation*}%
T_{BC} = \frac{P_{2}}{\tan \beta} \, \, \text{and} \, \, T_{CD} = \frac{P_{2}}{\sin \beta}
\end{equation*}
Equating the values for the tension in portion $BC$ of the string obtained in the two force resolutions, we can write
\begin{equation*}%
\frac{P_{1}}{\tan \alpha} =  \frac{P_{2}}{\tan \beta}
\end{equation*}
 from which 
\begin{equation*}%
\beta = \arctan \left( \frac{P_{2} \tan \alpha}{P_{1}} \right)
\end{equation*}
Substituting this value into the equation for $T_{CD}$ we can find the tension in portion $CD$ of the string.

\tchr Is it really so difficult to complete the problem, i.e. to find the force $T_{CD}$?

\stda The answer will contain the sine of the $\arctan \beta$, i.e.
\begin{equation*}%
T_{CD} = \frac{P_{2}}{\sin \left( \arctan \left( \dfrac {P_{2} \tan \alpha}{P_{1}} \right) \right)}
\end{equation*}

\tchr Your answer is correct but it can be written in a
simpler form if $\sin \beta$ is expressed in terms of $\tan \beta$. As a matter of fact
\begin{equation*}%
\sin \beta = \frac{\tan \beta}{ \sqrt{1 + \tan^{2} \beta}}
\end{equation*}
Since 
\begin{equation*}%
\tan \beta = \tan \alpha \left( \frac{P_{2}}{P_{1}}\right)
\end{equation*}
 we obtain 
\begin{equation*}%
T_{CD} = \frac{P_{1}}{\tan \alpha} \sqrt{1 + \left( \frac{P_{2}}{P_{1}} \right)^{2} \tan^{2} \alpha }
\end{equation*}
\textsc{Student B:} I see that before taking an examination in physics, you must review your mathematics very thoroughly.

\tchr Your remark is quite true.
\end{dialogue}

\section*{Problems}
\label{prob-13}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume*=problems]
\item An elastic string, stretched from wall to wall in a lift, sags due to the action of a weight suspended from its middle point as shown in \figr{fig-55}. The angle of sag $\alpha$ equals \ang{30} when the lift is at rest and \ang{45} when the lift travels with acceleration. Find the magnitude and direction of acceleration of the lift. The weight of the string is to be neglected.

\item A bob of mass $m = \SI{100}{\gram}$ is suspended from a string of length $l =\SI{1}{\meter}$ tied to a bracket as shown in \figr{fig-58} ($\alpha = \ang{30}$). A horizontal velocity of \SI[per-mode=symbol]{2}{\meter\per\second} is imparted to the bob and it begins to vibrate as a pendulum. Find the forces acting in members $AB$ and $BC$ when the bob is at the points of maximum deviation from the equilibrium position.
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-13/fig-058a.pdf}
\caption{Anaysing the motion of a pendulum, Problem 36.}
\label{fig-58}
\end{marginfigure}

\end{enumerate}


\cleardoublepage
