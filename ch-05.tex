% !TEX root = tarasov-qasp-2.tex

\chapter{How Do You Go About Solving Problems In Kinematics?}
\label{ch-05}


\begin{dialogue}

\tchr \hlred{Assume that two bodies are falling from a certain height. One has no initial velocity and the other has a certain initial velocity in a horizontal direction. Here and further on
we shall disregard the resistance of the air. Compare the time it takes for the two bodies to fall
to the ground.}
\std The motion of a body thrown horizontally can be regarded as a combination of two motions: vertical and horizontal. The time of flight is determined by the vertical component of the
motion. Since the vertical motions of the bodies are determined in both cases by the same data (same height and the absence of a vertical component of the initial velocity), the time of fall is the same for the two bodies. 
It equals $\sqrt{2H/g}$, where $H$ is the initial height.
\tchr Absolutely right. Now let us consider a more complex case. \hlred{Assume that both bodies are falling from the height $H$ with no initial velocity, but in its path one of them meets a fixed plane, inclined at an angle of \ang{45} to the horizontal. As a result of this impact on the plane the direction of the velocity of the body becomes horizontal \drkgry{(\figr{fig-23})}. The point of impact is at the height $h$. Compare the times of fall of the two bodies.}

\begin{figure}[!ht]
\centering
\includegraphics[angle=-1.2,width=0.4\textwidth]{figs/ch-05/fig-023a.pdf}
\caption{Two bodies falling from height $H$. One of them meets a fixed plane at height $h$. Problem is to compare the time of fall of the two bodies.}
\label{fig-23}
\end{figure}
\std Both bodies take the same time to fall to the level of the inclined
plane. As a result of the impact on the, plane one of the bodies acquires a horizontal component of velocity. This horizontal component cannot, however, influence the vertical component of the body's motion. Therefore, it follows that in this case as well the time of fall should be the same for the bodies.

\tchr Your answer is wrong. You were right in saying that the horizontal component of the velocity doesn't influence the vertical motion of the body and, consequently, its time of
fall. \hlblue{When the body strikes the inclined plane it not only acquires a horizontal velocity component, but also loses the vertical component of its velocity, and this of course must affect the time of fall.} After striking the inclined plane, the body falls from the height $h$ with no initial vertical velocity. The impact against the plane delays the vertical motion of the body and thereby increases its time of fall. The time of fall for the body which dropped straight to the ground is $\sqrt{2H/g}$; that for the body striking the plane is $\sqrt{2(H-h)/g}+\sqrt{2h/g}$.

This leads us to the following question: \hlred{at what $h$ to $H$ ratio will the time of fall reach its maximum value? In other words, at what height should the inclined plane be located so that it delays the fall most effectively?}

\std I am at a loss to give you an exact answer. It seems to me that the ratio $h/H$ should not be near to 1 or to 0, because a ratio of 1 or 0 is equivalent to the absence of any plane whatsoever. The inclined plane should be located somewhere in the middle between the ground and the initial point.

\tchr Your qualitative remarks are quite true. But you should find no difficulty in obtaining the exact answer. We can write the time of fall of the body as
\begin{equation*}%
t = \sqrt{\frac{2 H}{g}} \left( \sqrt{1-x} + \sqrt{x} \right) \, \, \text{where} \, \, x = \frac{h}{H}
\end{equation*}
Now we find the value of $x$ at which the function $t(x)$ is a maximum. First we square the time of fall. Thus
\begin{equation*}%
t^{2} = \frac{2 H}{g} \left(  1 +2\sqrt{(1-x) \,x} \,\, \, \right)
\end{equation*}
If the time is maximal, its square is also maximal. It is evident from the last equation that $t^{2}$ is a maximum when the function $y= (1- x) \,x$ is a maximum. Thus, the problem is reduced to finding the maximum of the quadratic trinomial 
\begin{equation*}%
y = -x^{2} + x = - \left( x- \frac{1}{2}\right)^2 + \frac{1}{4}
\end{equation*}
This trinomial is maximal at $x= \dfrac{1}{2}$. Thus, height $h$ should be one half of height $H$.

Our further discussion on typical procedure for solving problems in kinematics will centre around the example of a body thrown upward at an angle to the horizontal (usually called the elevation angle).

\std I'm not very good at such problems.

\tchr We shall begin with the usual formulation of the problem: \hlred{a body is thrown upward at an angle of $\alpha$, to the horizon with an initial velocity of $v_{0}$. Find the time of flight $T$, maximum height reached $H$ and the range $L$.} 

As usual, we first find the forces acting on the body. The only force is gravity. Consequently, the body travels at uniform velocity in the horizontal direction and with uniform acceleration $g$ in the vertical direction. We are going to deal with the vertical and horizontal components of motion separately, for which purpose we resolve the initial velocity vector into the vertical ($v_{0} \sin \alpha$,) and horizontal ($v_{0} \cos \alpha$,) components. The horizontal velocity component remains constant throughout the flight while the vertical component varies as shown in \figr{fig-24}. 
\begin{figure}[!ht]%
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-05/fig-024a.pdf}
\caption{A body thrown with an angle $\alpha$ to the horizon. The problem is to find the time of flight, maximum height reached and the range of the body.}
\label{fig-24}
\end{figure}
Let us examine the vertical component of the motion. The time of flight $T= T_{1} +T_{2}$, where $T_{1}$ is the time of ascent (the body travels vertically with uniformly decelerated motion) and $T_{2}$ is the time of descent (the body travels vertically downward with uniformly accelerated motion). The vertical
velocity of the body at the highest point of its trajectory (at the instant $t=T_{1}$) is obviously equal to zero. On the other hand, this velocity can be expressed by the formula showing the dependence of the velocity of uniformly decelerated motion on time. Thus we obtain
\begin{equation*}%
0 = v_{0} \sin \alpha - gT_{1}
\end{equation*}
or
\begin{equation}%
T_{1} = \frac{v_{0} \sin \alpha}{g}
\label{eq-14}
\end{equation}

When $T_{1}$ is known we can obtain
\begin{equation}%
H = v_{0} T_{1} \sin \alpha - \frac{g T_{1}^{2}}{2} = \frac{v_{0}^{2} \sin^{2} \alpha}{2g}
\label{eq-15}
\end{equation}

The time of descent $T_{2}$ can be calculated as the time a body falls from the known height $H$ without any initial vertical velocity:
\begin{equation*}%
T_{2} = \sqrt{\frac{2H}{g}} = \frac{v_{0} \sin \alpha}{g}
\end{equation*}
Comparing this with equation \eqref{eq-14} we see that the time of
descent is equal to the time of ascent. The total time of flight is
\begin{equation}%
T= T_{1} + T_{2} = \frac{2 \,v_{0} \sin \alpha}{g}
\label{eq-16}
\end{equation}
To find the range $L$, or horizontal distance travelled, we make use of the horizontal component of motion. As mentioned before, the body travels horizontally at uniform velocity. Thus
\begin{equation}%
L= (v_{0} \cos \alpha)\, T = \frac{v_{0}^{2} \sin 2 \alpha}{g}
\label{eq-17}
\end{equation}
It can be seen from equation \eqref{eq-17} that \hlblue{if the sum of the angles at which two bodies are thrown is \ang{90} and if the initial velocities are equal, the bodies will fall at the same point.} Is everything clear to you so far?
%\sidenote{$\sin\, (90 - \alpha) = ?$}
\std Why yes, everything seems to be clear.

\tchr Fine. Then we shall add some complications. \hlred{Assume that a horizontal tail wind of constant force $F$ acts on the body. The weight of the body is $P$. Find, as in the
preceding case, the time of flight $T$, maximum height reached $H$, and range $L$.}

\std In contrast to the preceding problem, the horizontal motion of the body is not uniform; now it travels with a horizontal acceleration of $a= \left( F/P \right)g$.
\tchr \hlred{Have there been any changes in the vertical component of motion?}

\std \hlblue{Since the force of the wind acts horizontally the wind cannot affect the vertical motion of the body}.

\tchr Good. Now tell me which of the sought for quantities should have the same values as in the preceding problem.

\std These will evidently be the time of flight $T$ and the height $H$. They are the ones determined on the basis of the vertical motion of the body. They will therefore be the same as in the preceding problem.

\tchr Excellent. How about the range?

\std The horizontal acceleration and time of flight being known, the range can be readily found. Thus
\begin{equation*}%
L  = \left( v_{0} \cos \alpha \right)T + \frac{aT^{2}}{2} = \frac{v_{0}^{2} \sin 2 \alpha}{g} + \frac{2F}{P}\frac{v_{0}^{2} \sin^{2} \alpha}{g}
\end{equation*}
\tchr Quite correct. Only the answer would best be written in another form:
\begin{equation}
L= \frac{v_{0}^{2} \sin 2 \alpha}{g} \left( 1 + \dfrac{F}{P} \tan \alpha \right)
\label{eq-18}
\end{equation}
% $\sin 2 \alpha = 2 sin^{2} \alpha$
Next we shall consider a new problem: \hlred{a body is thrown at an angle $\alpha$ to an inclined plane which makes the angle $\beta$ with the horizontal \drkgry{(\figr{fig-25})}. The body's initial velocity is $v_{0}$. Find the distance $L$ from the point where the body is thrown to the point where it falls on the plane.}

\begin{figure}[!ht]%
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-05/fig-025a.pdf}
\caption{A body thrown with an angle $\alpha$ to an inclined plane. The problem is to find the distance at which the body will land on the inclined plane.}
\label{fig-25}
\end{figure}
\std I once made an attempt to solve such a problem but failed.

\tchr Can't you see any similarity between this problem and the preceding one?

\std No, I can't.

\tchr Let us imagine that the figure for this problem
is turned through the angle $\beta$ so that the inclined plane becomes horizontal (\figr{fig-26}~\drkgry{(a)}).

\begin{figure*}
\centering
\includegraphics[width=0.8\linewidth]{figs/ch-05/fig-026a.pdf}
\caption{Rotating the \figr{fig-25} through angle $\beta$ gives us a problem similar to the one we have solved in the previous section. }
\label{fig-26}
\end{figure*}

Then the force of gravity is no longer vertical. Now we resolve it into a vertical ($P \cos \beta$) and a horizontal ($P \sin \beta$) component. You can readily see now that we have the preceding problem again, in which the force $P \sin \beta$ plays the role of the force of the wind, and $P \cos \beta$ the role of the force of gravity. Therefore we can find the answer by making use of equation \eqref{eq-18} provided that we make the following substitutions:
\begin{equation*}%
P \sin \beta \, \, \text{for} \, \, F, P \cos \beta \, \, \text{for} \, \, P, \, \, \text{and} \, \, g \cos \beta \, \, \text{for} \, \, g
\end{equation*}
Then we obtain
\begin{equation}%
L= \frac{v_{0}^{2} \sin 2 \alpha}{g \cos \beta } \left( 1 + \tan \beta \tan \alpha \right)
\label{eq-19}
\end{equation}
At $\beta =0$, this coincides with equation \eqref{eq-17}. 

Of interest is another method of solving the same problem. We introduce the coordinate axes $Ox$ and $Oy$ with the origin at the point the body is thrown from (\figr{fig-26}~\drkgry{(b)}). The inclined plane is represented in these coordinates by the linear function
\begin{equation*}%
y_{1}=-x \tan \beta
\end{equation*}
and the trajectory of the body is described by the parabola
\begin{equation*}%
y_{2}= ax^{2}+bx
\end{equation*}
in which the factors $a$ and $b$ can be expressed in terms of $v_{0}$, $\alpha$ and $\beta$. Next we find the coordinate $x_{A}$ of the point $A$ of intersection of functions $y_{1}$ and $y_{2}$ by equating the expressions for these functions
\begin{equation*}%
- x \tan \beta = ax^{2} + bx
\end{equation*}
From this it follows that 
\begin{equation*}%
x_{A} = \left( \frac{\tan \beta+b}{-a} \right)
\end{equation*}
Then we can easily find the required distance 
\begin{equation}
L= \frac{x_{A}}{\cos \beta } = - \frac{ \tan \beta + b} {a \cos \beta}
\label{eq-20}
\end{equation}
It remains to express factors $a$ and $b$ in terms of $v_{0}$, $\alpha$ and $\beta$. For this purpose, we examine two points of the parabola - $B$ and $C$ (see \figr{fig-26}~\drkgry{(b)}). We write the equation of the parabola for each of these points:
\begin{equation*}%
\left.
\begin{aligned}
 y_{2C} &= ax^{2}_{C} + bx_{C} \\
y_{2B} &= ax^{2}_{B} + bx_{B}
\end{aligned}
\right\}
\end{equation*}

%\begin{equation*} |x| =
%\begin{cases} 
%x & \text{if $x\ge 0$} 
%-x & \text{if $x\le 0$}
%\end{cases} 
%\end{equation*}
The coordinates of points $C$ and $B$ are known to us. Consequently, the preceding system of equations enables us to determine factors $a$ and $b$. I suggest that in your spare time you complete
the solution of this problem and obtain the answer in the form of equation (\ref{eq-19}).

\std I like the first solution better.

\tchr That is a matter of taste. The two methods of solution differ essentially in their nature. The first could be called the ``physical'' method. It employs simulation which is so typical of the physical approach (we slightly altered the point of view and reduced our problem to the previously
discussed problem with the tail wind). The second method could be called ``mathematical''. Here we employed two functions and found the coordinates of their points of intersection.

In my opinion, the first method is the more elegant, but less general. The field of application of the second method is substantially wider. It can, for instance, be applied in principle when the profile of the hill from which the body is thrown is not a straight line. Here, instead of the linear function $y_{1}$, some other function will be used which conforms to the profile of the hill. The first method is inapplicable in principle in such cases. We may note that the more extensive field of application of mathematical methods is due to their more abstract nature.

\end{dialogue}

\section*{Problems}
\label{prob-01}
\addcontentsline{toc}{section}{Problems}

%%\begin{fullwidth}
\begin{enumerate}[series=problems,first={\small}]
\item Body $A$ is thrown vertically upward with a velocity of \SI{20}{\meter \per \second}. At what height was body $B$ which, when thrown at a horizontal velocity of  \SI{4}{\meter \per \second} at the same time body $A$ was thrown, collided with it in its
flight? The horizontal distance between the initial points of the flight equals \SI{4}{m}. Find also the time of flight of each body before the collision and the velocity of each at the instant of collision.
\item From points $A$ and $B$, at the respective heights of \SIlist{2;6}{\metre}, two bodies are thrown simultaneously towards each other: one is thrown horizontally with a velocity of \SI{8}{\meter \per \second} and the other, downward at an angle of \ang{45} to the horizontal and at an initial velocity such that the bodies collide in flight. The horizontal distance between points $A$ and $B$ equals \SI{8}{m}. Calculate the initial velocity $v_{0}$ of the body thrown at an angle of \ang{45}, the coordinates $x$ and $y$ of the point of collision, the time of flight t of the bodies before colliding and the velocities $v_{A}$ and $v_{B}$ of the two bodies at the instant of collision. The trajectories of the bodies lie in a single plane.
\item Two bodies are thrown from a single point at the angles $\alpha_{1}$ and $\alpha_{2}$ to the horizontal and at the initial velocities $v_{1}$ and $v_{2}$, respectively. At what distance from each other will the bodies be after the time $t$? Consider two cases: 
\begin{enumerate}[label=(\arabic*),leftmargin=1cm]
\item the trajectories of the two bodies lie in a single plane and the bodies are thrown in opposite directions, and 
\item the trajectories lie in mutually perpendicular planes.
\end{enumerate}
\item A body falls from the height $H$ with no initial velocity. At the height $h$ it elastically bounces off a plane inclined at an angle of \ang{30} to the horizontal. Find the time it takes the body to reach the ground.
\item At what angle to the horizontal (elevation angle) should a body of weight $P$ be thrown so that the maximum height reached is equal to the range? Assume that a horizontal tail wind of constant force $F$ acts on the body in its flight.
\item A stone is thrown upward, perpendicular to an inclined plane with an angle of inclination $\alpha$. If the initial velocity is $v_{0}$, at what distance from the point from which it is thrown will the stone fall?

\item A boy \SI{1.5}{m} tall, standing at a distance of \SI{15}{m} from a fence \SI{5}{m}
high, throws a stone at an angle of \ang{45} to the horizontal. With what minimum velocity should the stone be thrown to fly over the fence?
\end{enumerate}


