% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{How Do You Go About Solving Problems On Gas Laws?}
\label{ch-21}

\begin{dialogue}


\stda I would like to look into the application of gas laws in solving various types of problems.

\tchr In my opinion, almost all the problems involving gas laws that are assigned to examinees are quite simple. Most of them belong to one of the following two groups. 
\begin{description}[font=\bfseries, leftmargin=1cm]
\item[First group:] Problems devised on the basis of a change in the state in a certain mass of gas;
the value of the mass is not used. As a result of expansion, heating and other processes, the gas goes
over from a certain state with parameters $p_{1}$, $V_{1}$ and $T_{1}$ to a state with parameters $p_{2}$, $V_{2}$ and $T_{2}$. The parameters of the initial and final states are related to one another by the equation of the combined gas law
\begin{equation}%
\frac{p_{1} V_{1}}{T_{1}}  = \frac{p_{2} V_{2}}{T_{2}}
\label{119}
\end{equation}
The problem consists in finding one of these six parameters.

\item[Second group:] Problems in which the state of the gas does not change but the value of the mass of the gas appears in the problem. It is required to find either this mass when all the parameters are known, or one of the parameters when the mass and the other parameters are known. In such problems the molecular weight of the gas must be known.
\end{description}

\stdb I think the most convenient way of solving problems of the second group is to use equation \eqref{eq-104} of the combined gas law.

\tchr Of course you can use this equation. To do this, however, you must know the numerical value of the universal gas constant $R$. As a rule, nobody remembers it. For this reason, in practice it is more convenient to resort to the following method: we assume that the gas is brought to standard conditions, denoting the gas parameters at these conditions by $p_{S}$, $V_{S}$ and $T_{S}$; Then we can write the equation
\begin{equation}%
\frac{p V}{T} = \frac{p_{S} V_{S}}{T_{S}}
\label{eq-120}
\end{equation}
where 
\begin{equation*}%
V_{S} =  \frac{m}{\mu} \times \SI{22.4}{\litre}
\end{equation*}

\stdb In my opinion, this method of solution is by no means simpler than the use of equation (\ref{eq-104}). Here we have to remember three values: $P_{S}=\SI{76}{\centi\meter}$ Hg, $T_{S}=\SI{273}{\kelvin}$ and $V_{S}/( m/\mu) =\SI{22.4}{\litre}$. It is obviously simpler to memorize one value, the universal gas constant.

\tchr Nevertheless, my method is simpler because nobody has any difficulty in remembering the three values you indicated (pressure, temperature and the volume of the gram-molecule of a gas under standard conditions). Assume that \hlred{we are to find the volume of \SI{58}{\gram} of air at a pressure of 8 atm and a temperature of \SI{91}{\degreeCelsius}}. Let us solve this problem by the method I proposed. Since the mass of the gram-molecule of air equals \SI{29}{\gram}, we have 2 gram-molecules. At standard conditions they occupy a volume of \SI{44.8}{\litre}. From equation \eqref{eq-120} we obtain
\begin{equation*}%
V = V_{S} \frac{p_{S}T}{p T_{S}} = \SI{44.8}{\litre} \times \frac{1 \times 364}{ 8 \times 273} = \SI{7.5}{\litre}
\end{equation*}

\stdb I see you have assumed that $p_{S} = 1 \,\,\text{atm}$. The conditions of the problem, however, most likely referred to technical atmospheres. Then it should be  $p_{S} = 1.034 \,\,\text{atm}$. 

\tchr You are right. There is a difference between the physical atmosphere (corresponding to standard pressure) and the technical atmosphere. I simply neglected this difference.

\stda Could you point out typical difficulties in solving problems of the first and second groups?

\tchr I have already mentioned that in my opinion these problems are quite simple.

\stda But what mistakes do examinees usually make?
\begin{marginfigure}%
\centering
\includegraphics[width=\textwidth]{figs/ch-21/fig-080a}
\caption{Work done by a gas.}
\label{fig-80}
\end{marginfigure}

\tchr Apart from carelessness, the main cause of errors is the inability to compute the pressure of the gas in some state or other. Consider a problem involving a glass tube sealed at one end. \hlred{The tube contains a column of mercury isolating a certain volume of air from the medium. The tube can be turned in a vertical plane. In the first position \drkgry{(\figr{fig-80}(a))}, the column of air in the tube has the length $l_{1}$ and in the second position \drkgry{(\figr{fig-80}(b))}, $l_{2}$. Find the length $l_{3}$ of the column of air in the third position when the tube is inclined at an angle of a to the vertical \drkgry{(\figr{fig-80}(c))}.} We shall denote the atmospheric pressure by $p_{A}$ in terms of length of mercury column, and the length of the mercury column in the tube by $\Delta l$. In the first position, the pressure of the air in the tube is evidently equal to the atmospheric pressure. In the second position, it is equal to the difference $(p_{A}-\Delta l)$, because the atmospheric pressure is counterbalanced by the combined pressures of the mercury column and the air inside the tube. Applying the law of Boyle and Mariotte we write
\begin{equation*}%
l_{1} p_{A} = l_{2} (p_{A} -\Delta l)
\end{equation*}
from which we find that the atmospheric pressure is
\begin{equation}%
p_{A} = \Delta l \frac{l_{2}}{l_{2} - l_{1}}
\label{eq-121}
\end{equation}
In the third position, a part of the weight of the mercury column will be counterbalanced by the
reaction of the tube walls. As a result, the pressure of the air inside the tube turns out to be equal to $(p_{A}-\Delta l \cos \alpha)$. Using the law of Boyle and Mariotte for the first and third states of the gas, we can write 
\begin{equation*}%
l_{1} p_{A} = l_{3} (p_{A} -\Delta l \cos \alpha)
\end{equation*}
from which the atmospheric pressure equals
\begin{equation}%
p_{A} = \Delta l \frac{l_{3} \cos \alpha}{l_{3} - l_{1}}
\label{eq-122}
\end{equation}
Equating the right-hand sides of equations \eqref{eq-121} and \eqref{eq-122} we obtain
\begin{equation*}%
\frac{l_{2}}{l_{2} - l_{1}} = \frac{l_{3} \cos \alpha}{l_{3} - l_{1}}
\end{equation*}
from which we find the required length
\begin{equation}%
l_{3}  = \frac{l_{1} l_{2}}{l_{2} -(l_{2} -  l_{1})\cos \alpha}
\label{eq-123}
\end{equation}
You can readily see that if $\cos \alpha =1$, then $l_{3}  = l_{2}$ i. e. we have the second position of the tube, and if $\cos \alpha =0$, then $l_{3}  = l_{1}$ which corresponds to the first position of the tube.

\stda The first and second groups of problems in your classification are clear to me. But, is it likely that the examination will include combinations of problems from the first and second groups?

\tchr Why yes, such a possibility cannot be ruled out. Let us consider the following problem. \hlred{At a pressure of 2 atm, \SI{16}{\gram} of oxygen occupies a volume of 5 litres. How will the temperature of the gas change if it is known that upon increase in pressure to 5 atm the volume reduces by 1 litre?}

\stda The mass, pressure and volume of the oxygen being known, we can readily find its temperature. Thus, \SI{16}{\gram} of oxygen is 0.5 gram-molecule,which has a volume of 11.2 litres at standard conditions. Next, we find that
\begin{equation}%
T_{1}=T_{S}\frac{p_{1}V_{1}}{p_{S}V_{S}} =273 \frac{2 \times 5}{ 1 \times 11.2} =\SI{244}{\kelvin}
\label{eq-124}
\end{equation}
\tchr Quite right. At the given stage you've handled the problem as a typical one from the second group.

\stda Then, since we know the temperature $T_{1}$ of the gas in the initial state, we can find the temperature $T_{2}$ in the final state. Thus
\begin{equation*}%
T_{2}=T_{1}\frac{p_{2}V_{2}}{p_{1}V_{1}} =244 \frac{5 \times 4}{ 2 \times 5} =\SI{488}{\kelvin}
\end{equation*}
Comparing this result with equation \eqref{eq-124}, we find that the temperature has been raised by \SI{244}{\kelvin}.

\tchr Your solution is absolutely correct. As you could see, the second half of the problem was dealt with as a typical one from the first group.

\stdb At the very beginning of our discussion, in speaking of the possible, groups of problems, you said most problems belong to these groups. Are there problems that differ in principle from those of the first and second groups?

\tchr Yes, there are. In the problems of these groups, it was assumed that the mass of the gas remained unchanged. Problems can be devised, however, in which the mass of the gas is changed (gas is pumped out of or into the container). We will arbitrarily classify such problems in the third group. There are no ready-made rules for solving such problems; they require an individual approach in each case. However, in each specific case, problems of the third group can be reduced to problems of the first two groups or to their combination. This can be illustrated by two examples. 

Here is the first one. \hlred{The gas in a vessel is subject to a pressure of 20 atm at a temperature of \SI{27}{\degree}C. Find the pressure of the gas in the vessel after one half of the mass of the gas is released from the vessel and the temperature of the remainder is raised by \SI{50}{\degree}.}

This problem resembles those of the first group, since it involves a change in the state of the gas. With the change in state, however, the mass of the gas also changes. In order to make use of the combined gas law, we must study the change in state of the same mass of the gas. We shall choose the mass of the gas that is finally left in the vessel. We denote its final parameters by $p_{2},\, V_{2}$ and $T_{2}$. Then $T_{2}=(273+27+50)= \SI{350}{\kelvin}$; $V_{2}=V$, where $V$ is the volume of the vessel; and $P_{2}$ is the required pressure. How can we express the initial parameters of this mass of gas?

\stda It will have the same temperature as the whole mass of gas: $T_{1}= (273+27)=\SI{300}{\kelvin}$; its volume will be one half of the volume of the vessel, i.e. $V/2$; and its pressure is the same as that of the whole mass of gas: $p_{1} =20$ atm.

\stdb I would deal with the initial parameters of the above-mentioned mass of gas somewhat differently: $T_{1}=\SI{300}{\kelvin}$: the volume is the same as for the whole mass of gas $(V_{1}=V)$ but the pressure is equal to one half of the pressure of the whole mass of gas, i.e. $p_{1} =10$ atm.
\begin{marginfigure}%
\centering
\includegraphics[width=\textwidth]{figs/ch-21/fig-081a}
\caption{Work done by a gas.}
\label{fig-81}
\end{marginfigure}


\tchr Since the pressure and volume appear in the equation in the form of their product, both of your proposals, though they differ, lead to the same result. For this reason, we could have. refrained from discussing these differences if they didn't happen to be of interest from the physical
point of view. We shall arbitrarily call the molecules of the portion of the gas that finally remains in the vessel ``white'' molecules, and those of the portion to be released from the vessel, ``black'' molecules. Thus, we have agreed that the white molecules remain in the vessel and the black molecules are released from it. The initial state of the gas can be treated in two ways: 
\begin{enumerate}[label=(\arabic*),leftmargin=1cm]
\item the black and white molecules are separated so that macroscopic volumes can be separated out in the vessel containing only white or only black molecules \drkgry{(\figr{fig-81}(a))}; 
\item the white and black molecules are thoroughly mixed together so that any macroscopic' volume contains a practically equal number of each kind of molecules \drkgry{(\figr{fig-81}(b))}. 
\end{enumerate}

In the first case, molecules of each kind form their own gaseous ``body'' with a volume of $V/2$ which exerts a pressure of 20 atm on the walls and on the imaginary boundary with the other body. In the second case, molecules of both kinds are distributed uniformly throughout the whole volume $V$ of the vessel, and the molecules of each kind exert only one half of the pressure on the walls (at any place on the walls, one half of the blows come from white molecules and the other half from black ones). In this case, $V_{1}=V$ and $p_{1}=10$ atm. In connection with this last remark, let us recall the law of partial pressures: the pressure of a mixture of gases is equal to the sum of the pressures of the component gases. I wish to emphasize that here we are dealing with a mixture of gases, where molecules of all kinds are intimately mixed together.

\stdb I think the second approach is more correct because the molecules of both kinds are really mixed together.

\tchr In the problem being considered, both approaches are equally justified. Don't forget that our \emph{a priori} division of the molecules into two kinds was entirely arbitrary. 

But let us return to the solution of the problem. We write the equation of the combined gas law for the mass of the gas remaining in the vessel:
\begin{equation*}%
\frac{10 V}{300} = \frac{p_{2}V}{350}
\end{equation*}
from which we find that $p_{2}=11.7$ atm. 

Now, consider the following problem. \hlred{A gas is in a vessel of volume $V$ at a pressure of $p_{0}$. It is being pumped out of the vessel by means of a piston pump with a stroke volume of $v$ (\figr{fig-82}). Find the number, $n$, of strokes required to lower the pressure of the gas in the vessel to $p_{n}$.}
\begin{marginfigure}%
\centering
\includegraphics[width=\textwidth]{figs/ch-21/fig-082a}
\caption{Work done by a gas.}
\label{fig-82}
\end{marginfigure}

\stda This problem seems to be quite simple: $n$ strokes of the piston lead to an n-fold increase in the volume of the gas by the volume $v$. Therefore, we can write the law of Boyle and Mariotte in the form
\begin{equation*}%
p_{0}V = p_{n} (V + nv)
\end{equation*}
from which we can find the number of strokes $n$.

\tchr To what mass of gas does your equation refer?

\stda To the mass that was initially in the vessel.

\tchr But even after the first stroke a part of this mass leaves the system entirely: when the piston moves to the left it closes valve $A$ and opens valve $B$ through which the gas leaves the system (see \figr{fig-82}). In other words, the $n$-fold increase of the volume of the gas by the amount $v$ does not refer to the same mass of gas. Consequently, your equation is incorrect.

Let us consider each stroke of the piston separately. We shall begin with the first stroke. For the mass of gas that was initially in the vessel we can write
\begin{equation*}%
p_{0}V = p_{1} (V + v)
\end{equation*}
where $p_{1}$ is the pressure of the gas after the piston has completed the first working stroke and is in the extreme right-hand position. Then the piston returns to its initial left-hand position. At this, as I previously mentioned, valve $A$ is closed, and the mass of the gas in the vessel is less than the initial mass. Its pressure is $p_{1}$. For this mass of gas we can write the equation
\begin{equation*}%
p_{1}V = p_{2} (V + v)
\end{equation*}
where $p_{2}$ is the pressure of the gas at the end of the second stroke. Dealing consecutively with the third, fourth and subsequent strokes of the piston, we obtain a system of equations of the law of Boyle and Mariotte:
\begin{equation}%
\left.
\begin{aligned}
p_{0}V & = p_{1} (V + v)\\
p_{1}V &= p_{2} (V + v)\\
p_{2}V & = p_{3} (V + v)\\
\dots{}\ldots{} &\dots{}\ldots{}\dots{}\ldots{}  \\
p_{n-1}V & =p_{n} (V+v)
\label{eq-125}
\end{aligned}
\right\}
\end{equation}
Each of these equations refers to a definite mass of gas. Solving the system of equations \eqref{eq-125} we obtain
\begin{equation*}%
p_{n}= p_{0} \left( \dfrac{V}{V+v} \right)^{n}
\end{equation*}
Taking the logarithm of this result, we finally obtain
\begin{equation}%
n = \frac{\log \left(\frac{p_{n}}{p_{0}}\right)}{\log \left( \frac{V}{V+v} \right)}
\label{eq-126}
\end{equation}

\end{dialogue}

\section*{Problems}
\label{prob-21}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume=problems]
\begin{marginfigure}[2cm]%
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-21/fig-083a}
\caption{A glass tube with sealed end submerged in a vessel with mercury.}
\label{fig-83}
\end{marginfigure}
\item A glass tube with a sealed end is completely submerged in a vessel with mercury (\figr{fig-83}). The column of air inside the tube has a length of \SI{10}{\centi\meter}. To what height must the upper end of the tube be raised above the level of the mercury in the vessel so that the level of the mercury inside the tube is at the level of the mercury in the vessel? Assume standard atmospheric pressure. Calculate the mass of the air inside the tube if its cross-sectional area equals  \SI{1}{\centi\meter^{2}}. The temperature is \SI{27}{\degreeCelsius}.

\item A glass tube, one end of which is sealed, is submerged with the open end downward into a vessel with mercury (\figr{fig-84}). How will the level of the mercury in the tube change if the temperature is raised from \SIrange{27}{77}{\degreeCelsius}? Neglect the thermal expansion of the tube. Assume standard atmospheric pressure. Find the mass of the air inside the tube if its cross-
sectional area is \SI{0.5}{\centi\meter^{2}}.
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-21/fig-084a}
\caption{A glass tube with sealed end submerged in a vessel with mercury.}
\label{fig-84}
\end{marginfigure}

\item The air in a vessel with a volume of \SI{5}{\liter} has a temperature of \SI{27}{\degreeCelsius} and is subject to a pressure of 20 atm. What mass of air must be released from the vessel so that its pressure drops to 10 atm?
\item Compute the amount of work done by a gas which is being isobarically heated from \SIrange{20}{100}{\degreeCelsius} if it is in a vessel closed by a movable piston with a cross-sectional area of \SI{20}{\centi\meter^{2}} and weighing 5 kgf. Consider two cases: 
\begin{enumerate}
\item the vessel is arranged horizontally (\figr{fig-85}~\drkgry{(a)}), and 
\item the vessel is arranged vertically (\figr{fig-85}~\drkgry{(b)}). 
\end{enumerate}
The initial volume of the gas is 5 litres. Assume standard atmospheric pressure.
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-21/fig-085a}
\caption{Work done by a gas.}
\label{fig-85}
\end{marginfigure}
\item A column of air \SI{40}{\centi\meter} long in a glass tube with a cross-sectional area of \SI{0.5}{\centi\meter^{2}} and arranged vertically with the sealed end upward, is isolated by a column of mercury \SI{8}{\centi\meter} long. The temperature is \SI{27}{\degreeCelsius}. How will the length of the air column change if the tube is inclined \ang{60} from the vertical and the temperature is simultaneously raised by \ang{30}? Assume standard atmospheric pressure. Find the mass of the air enclosed in the tube.
\item What is the mass of the water vapour in a room of a size $\SI{6}{\metre} \times  \SI{5}{\metre} \times \SI{3.5}{\metre}$ if, at a temperature of \SI{15}{\degree}C \SI{15}{\degreeCelsius}, the relative humidity is 55\%? Will dew be formed if the air temperature drops to \SI{10}{\degreeCelsius}? What part of the total mass of the air in the room is the mass of the water vapour if the air pressure equals \SI{75}{\centi\meter} Hg?
\end{enumerate}




\cleardoublepage

\thispagestyle{empty}
\vspace*{-1.95cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.75\textwidth]{figs/sec/tarasov-qasp-sec-10.pdf}
\end{figure*}
\vspace*{-.4cm}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{What is a field? How is a field described? How does motion take place in a field? These fundamental problems of physics can be most conveniently considered using an electrostatic field as an example. We shall discuss the motion of charged bodies in a uniform electrostatic field. A number of problems illustrating Coulomb's law will be solved.
}}}
\end{fullwidth}

%\cleardoublepage
