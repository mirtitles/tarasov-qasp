% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{How Well Do You Know The Gas Laws?}
\label{ch-20}

\begin{dialogue}

\tchr Please write the equation for the combined gas law.

\stda This equation is of the form
\begin{equation}%
\frac{pV}{T} = \frac{p_{0}V_{0}}{T_{0}}
\label{eq-103}
\end{equation}
where $p$, $V$ and $T$ are the pressure, volume and temperature of a certain mass of gas in a certain state, and $p_{0}$, $V_{0}$ and $T_{0}$ are the same for the initial state. The temperature is expressed in the absolute scale.

\stdb I prefer to use an equation of a different form
\begin{equation}%
pV = \frac{m}{\mu} RT
\label{eq-104}
\end{equation}
where $m$ is the mass of the gas, $\mu$ is the mass of one gram-molecule and $R$ is the universal gas constant.

\tchr Both versions of the combined gas law are correct. (To \smallcaps{Student~B}) You have used the universal gas constant. Tell me, how would you compute its value? I don't think one can memorize it.

\stdb To compute $R$, I can use equation \eqref{eq-103}, in which the parameters $p_{0}$, $V_{0}$ and $T_{0}$ refer to a given mass of gas but taken at standard conditions. This means that $p_{0}= \SI{76}{\centi\meter}$ Hg (\si{\centi\meter} of mercury column), $T_{0}=\SI{273}{\kelvin}$ and $V_{0} =\left( \dfrac{m}{\mu} \right) \times \SI{22.4}{\litre}$, since a gram-molecule of any gas at standard conditions occupies a definite volume equal to \SI{22.4}{\litre}. The ratio $m/\mu$ is evidently the number of gram-molecules contained in the given mass of the gas. Substituting these values in equation \eqref{eq-103} we obtain
\begin{equation*}%
pV = \left( \frac{m}{\mu}\right) \, T \,\, \left( \frac{\SI{76}{\centi\meter} \,\,\text{Hg} \times \SI{22.4}{\litre}}{\SI{273}{\kelvin}} \right)
\end{equation*}
Comparing this with expression \eqref{eq-104} we find that $R=6.2 \,(\si{\centi\meter}~\text{Hg}) \dfrac{\text{litres}}{\text{deg}}$.

\tchr I purposely asked you to do these calculations in order to demonstrate the equivalence of expressions \eqref{eq-103} and \eqref{eq-104}. Unfortunately, examinees usually know only equation \eqref{eq-103} and are unfamiliar with \eqref{eq-104}, which coincides with equation \eqref{eq-102} obtained previously on the basis of molecular-kinetic considerations. From a comparison of equations \eqref{eq-102} and \eqref{eq-104} it follows that $( m/\mu)R=Nk$. Then
\begin{equation}%
R=\frac{N}{\left( \dfrac{m}{\mu} \right)} k =N_{A}k
\label{eq-105}
\end{equation}
Thus the universal gas constant turns out to be the product of Avogadro's number by Boltzmann's constant. Next, we shall see whether you can use the equation of the combined gas law. \hlred{Please draw a curve showing an isobaric process, i.e. a process in which the gas pressure remains constant, using coordinate axes $V$ and $T$.}

\stda I seem to recall that this process is described by a straight line.

\tchr Why recall? Make use of equation \eqref{eq-104}. On its basis, express the volume of the gas as a function of its temperature.

\stda From equation \eqref{eq-104} we get
\begin{equation}%
V = \left( \dfrac{m}{\mu} \right) \left( \dfrac{R}{p} \right) T
\label{eq-106}
\end{equation}
\tchr Does the pressure here depend upon the temperature?

\stda In the given case it doesn't because we are dealing with an isobaric process.

\tchr Good. Then the product $\left( \dfrac{m}{\mu} \right) \left( \dfrac{R}{p} \right)$ in equation \eqref{eq-106} is a constant factor. We thus obtain a linear dependence of the volume of the gas on its temperature. Examinees can usually depict \hlred{isobaric} ($p$=const), \textcolor{violet}{isothermal} ($T$=const) and \hlblue{isochoric} ($V$=const) processes in diagrams with coordinate axes $p$ and $V$. At the same time they usually find it difficult to depict these processes with other sets of coordinate axes, for instance $V$ and $T$ or $T$ and $p$. These three processes are shown in \figr{fig-73} in different sets of coordinate axes.
\begin{figure*}
\centering
%\fbox{
\begin{tikzpicture}
[>=latex,scale=2]
	
	\clip (-2.7,-0.21) rectangle (4.55, 2.1);
	%\draw[gray,step=0.2] (-3,-1) grid (5, 3);
	
	\node[below right,Maroon] at (-2,1.8 ) 
	{\emph{Isobar}};
	\node[below right,SteelBlue] at (.5,1.8 ) 
	{\emph{Isochore}};
	\node[below right,violet] at (2.9,1.8 ) 
	{\emph{Isotherm}};
	
	\node[below right,SteelBlue,rotate=90] at (-1,.4 ) 
	{\emph{Isochore}};
	\node[below right,violet,rotate=90] at (1.5,.3 ) 
	{\emph{Isotherm}};
	\node[below right,Maroon,rotate=90] at (4,.4 ) 
	{\emph{Isobar}};

	\node[below right,violet,] at (-2,.5 ) 
	{\emph{Isotherm}};
	\node[below right,Maroon,rotate=35] at (0.4,.55 ) 
	{\emph{Isobar}};
	\node[below right,SteelBlue,rotate=35] at (3,.6 ) 
	{\emph{Isochore}};
		
	\draw[thick, ->, gray] (0,0) -- (2,0);
	\draw[thick, ->, gray] (0,0) -- (0,2);

	\node [left,gray] at (-2.5,2) {$P$};
	\node [left,gray] at (0,2) {$V$};
	\node [left,gray] at (2.5,2) {$T$};
	
	\node [below left, gray] at (2.1,0) {$T$};
	\node [below left, gray] at (4.6,0) {$P$};
	\node [below left, gray] at (-.4,0) {$V$};
	
	\node [below left,gray] at (0,0) {0};
	\node [below left,gray] at (2.5,0) {0};
	\node [below left,gray] at (-2.5,0) {0};
	
	\draw[thick, violet] (-2.45,1.8) .. controls (-2.45,0) and (-1.6,.05) .. (-.7,.05);
	\draw[thick, SteelBlue] (2.5,0) -- (4.3,1.2);
	\draw[thick, Maroon] (0,0) -- (1.8,1.2);

	
	\draw[thick, ->, gray] (2.5,0) -- (4.5,0);
	\draw[thick, ->, gray] (2.5,0) -- (2.5,2);
	
	\draw[thick, ->, gray] (-2.5,0) -- (-.5,0);
	\draw[thick, ->, gray] (-2.5,0) -- (-2.5,2);
	
	\draw[thick, Maroon] (-2.5,1.5) -- (-.7,1.5);
	\draw[thick, SteelBlue] (0,1.5) -- (1.8,1.5);
	\draw[thick, violet] (2.5,1.5) -- (4.3,1.5);
	
	\draw[thick, SteelBlue] (-1,0) -- (-1,1.8);
	\draw[thick, violet] (1.5,0) -- (1.5,1.8);
	\draw[thick, Maroon] (4,0) -- (4,1.8);
\end{tikzpicture}%}
\caption{The Gas Laws.}
\label{fig-73}
\end{figure*}

\stdb I have a question concerning isobars in a diagram with coordinate axes $V$ and $T$. From equation \eqref{eq-106} and from the corresponding curve in \figr{fig-73} we see that as the temperature approaches zero, the volume of the gas also approaches zero. However, in no case can the volume of a gas become less than the total volume of all its molecules. Where is the error in my reasoning?

\tchr Equations \eqref{eq-102}, \eqref{eq-103}, \eqref{eq-104} and \eqref{eq-106} refer to the so-called ideal gas. The ideal gas is a simplified model of a real gas in which neither the size of the molecules nor their mutual attraction is taken into consideration. All the curves in \figr{fig-73} apply to such a simplified model, i.e. the ideal gas.


\stdb But the gas laws agree well with experimental data, and in experiments we deal with real gases whose molecules have sizes of their own.

\tchr Note that such experiments are never conducted at extremely low temperatures. If a real gas has not been excessively cooled or compressed, it can be described quite accurately by the ideal gas model. Note also that for the gases contained in the air (for instance, nitrogen and oxygen), these conditions are met at room temperatures and ordinary pressures.

\stdb Do you mean that if we plot the dependence of the volume on the temperature in an isobaric process for a real gas, the curve will coincide with the corresponding straight line in \figr{fig-73} at sufficiently high temperatures but will not coincide in the low temperature zone?

\tchr Exactly. Moreover, remember that on a sufficiently large drop in temperature a gas will be condensed into a liquid.

\stdb I see. The fact that the curve of equation \eqref{eq-106} in \figr{fig-73}  passes through the origin, or zero point, has no physical meaning. But then maybe we should terminate the curve before it reaches this point?

\tchr That is not necessary. You are just drawing the curves for the model of a gas. Where this model can be applied is another question. Now I want to propose the following. Two isobars are shown in \figr{fig-74} in coordinate axes $V$ and $T$: one corresponds to the pressure $p_{1}$ and the other to the pressure $p_{2}$. Which of these pressures is higher?

\stda Most likely, $p_{2}$ is higher than $p_{1}$.

\begin{marginfigure}[-3cm]
\centering
%\fbox{
\begin{tikzpicture}	[>=latex ,scale=2]
	
	\clip (-.2,-0.21) rectangle (2.1, 2.1);
	%\draw[gray,step=0.2] (-3,-1) grid (5, 3);
		
	%\draw[thick,red] plot (\x,\x*\x)	;
	%\draw[thick,cyan] plot (\x,\x*\x*\x);
	
	%\node[below right,cyan] at (.5,1.8 ) 
	%{\emph{Isochore}};
	
	%\node[below right,violet,rotate=90] at (1.5,.3 ) 
	%{\emph{Isotherm}};
		
	\draw[thick, ->, gray] (0,0) -- (2,0);
	\draw[thick, ->, gray] (0,0) -- (0,2);
	\draw[thick, violet, dashed] (1.5,0) -- (1.5,1.8);

	\node [left,gray] at (0,2) {$V$};
	\node [below left, gray] at (2.1,0) {$T$};
	\node [below left,gray] at (0,0) {0};
	\node [below left,gray] at (0,0) {0};
	\node [below right,gray] at (1.5,.75) {$p_{1}$};
	\node [below right,gray] at (1.5,1.59) {$p_{2}$};
	
	\draw[thick, red] (0,0) -- (2,1);
	\draw[thick, red] (0,0) -- (1.7,1.8);
	\draw[<-,semithick,gray] (0,1.59) -- (1.5,1.59);
	\draw [fill,gray](1.5,1.59) circle (0.8pt);
	\draw[<-,semithick,gray] (0,.75) -- (1.5,.75);
	\draw [fill,gray](1.5,.75) circle (0.8pt);
\end{tikzpicture}%}
\caption{Which pressure is higher?}
\label{fig-74}
\end{marginfigure}

\tchr You answer without thinking. Evidently, you decided that since that isobar is steeper, the corresponding pressure is higher. This, however, is entirely wrong. The tangent of the angle of inclination of an isobar equals  $\left( \dfrac{m}{\mu} \right) \left( \dfrac{R}{p} \right)$ according to equation \eqref{eq-106}. It follows that the higher the pressure, the less the angle of inclination of the isobar. Thus, in our case, $p_{2} < p_{1}$ We can reach the same conclusion by different reasoning. Let us draw an isotherm in \figr{fig-74} (see the dashed line). It intersects isobar $p_{2}$ at a higher value of the gas volume than isobar $p_{1}$. We know that at the same temperature, the pressure of the gas will be the higher, the smaller its volume. This follows directly from the combined gas law [see equation \eqref{eq-103} or \eqref{eq-104}]. Consequently,  $p_{2} < p_{1}$.

\stda Now, I'm sure I understand.

\tchr Then look at \figr{fig-75} which shows two isotherms (the coordinate axes are $p$ and $V$) plotted for the same mass of gas at different temperatures, $T_{1}$ and $T_{2}$ Which is the higher temperature?

\begin{marginfigure}[-3cm]
\centering
%\fbox{
\begin{tikzpicture}[>=latex,scale=2]
	
	\clip (-.2,-0.21) rectangle (2.1, 2.1);		
	\draw[thick, <->, gray] (2,0) -- (0,0) -- (0,2);
	\draw[thick, Maroon, dashed] (0,0.7) -- (.8,0.7);
	\node [left,gray] at (0,2) {$p$};
	\node [below left, gray] at (2.1,0) {$V$};
	\node [below left,gray] at (0,0) {0};
	\node [below right,gray] at (.02,.9) {$T_{1}$};
	\node [below right,gray] at (.5,.9) {$T_{2}$};
	\draw[ thick, violet] (.08,1.8) .. controls (.05,.01) and (.8,.05) .. (1.85,.05);
	\draw[ thick, violet] (.15,1.8) .. controls (.14,.45) and (.8,.18) .. (1.85,.15);
	\draw[->,semithick,gray] (0.22,.7) -- (0.22,0);
	\draw [fill,gray](0.22,.7) circle (0.8pt);
	\draw[->,semithick,gray] (0.41,.7) -- (0.41,0);
	\draw [fill,gray](0.41,.7) circle (0.8pt);

\end{tikzpicture}%}
\caption{Which pressure is higher?}
\label{fig-75}
\end{marginfigure}

\stda First I shall draw an isobar (see the dashed line (\begin{tikzpicture}\draw[thick, Maroon, dashed] (0,0) -- (.4,0);\end{tikzpicture}) in \figr{fig-75}). At a constant pressure, the higher the temperature of a gas, the larger its volume. Therefore, the outermost isotherm $T_{2}$ corresponds to the higher temperature.

\tchr Correct. Remember: the closer an isotherm is to the origin of the coordinates $p$ and $V$, the lower the temperature is.

\stdb In secondary school our study of the gas laws was of much narrower scope than our present discussion. The combined gas law was just barely mentioned. Our study was restricted to Boyle and Mariette's, Gay-Lussac's and Charles' laws.

\tchr In this connection, I wish to make some remarks that will enable the laws of Boyle and Mariotte, Gay-Lussac and Charles to be included in the general scheme. Boyle and Mariotte's law (more commonly known as Boyle's law) describes the dependence of $p$ on $V$ in an isothermal process. The equation for this law is of the form
\begin{equation}%
p=\frac{\text{constant}}{V}
\label{eq-107}
\end{equation}
where the $\text{constant}= \left( \dfrac{m}{\mu} \right) RT$.

Gay-Lussac s law describes the dependence of $p$ on $T$ in an isochoric process. The equation of this law is
\begin{equation}%
p=\text{constant} \,\,T
\label{eq-108}
\end{equation}
where the $\text{constant}= \left( \dfrac{m}{\mu} \right) \dfrac{R}{V}$.

The law of Charles describes the dependence of $V$ on $T$ in an isobaric process. Its equation is
\begin{equation}%
V=\text{constant} \,\,T
\label{eq-109}
\end{equation}
where the $\text{constant}= \left( \dfrac{m}{\mu} \right) \dfrac{R}{p}$. [Equation \eqref{eq-109} evidently repeats equation \eqref{eq-106}.] I will make the following remarks concerning the above-mentioned gas laws:
\begin{enumerate}[label=(\arabic*),leftmargin=1cm]
\item All these laws refer to the ideal gas and are applicable to a real gas only to the extent that the latter is described by the model of the ideal gas.
\item Each of these laws establishes a relationship between some pair of parameters of a gas under the assumption that the third parameter is constant.
\item As can readily be seen, each of these laws is a corollary of the combined gas law [see equation (\ref{eq-104})] which establishes a relationship between all three parameters regardless of any special conditions.
\item The constants in each of these laws can be expressed, not in terms of the mass of the gas and the constant third parameter, but in terms of the same pair of parameters taken for a different state of the same mass of the gas. In other words, the gas laws can be rewritten in the following form
\begin{align}%
p & = \frac{p_{0}V_{0}}{V} \tag{107a} \label{107a} \\
p & = \frac{p_{0}}{T_{0}} T \tag{108a}\label{108a} \\
V & = \frac{V_{0}}{T_{0}} T \tag{109a} \label{109a}
\end{align}
\end{enumerate}
\stda It seems I have finally understood the essence of the gas laws.

\tchr In that case, let us go on. Consider the following example. \hlred{A gas expands in such a manner that its pressure and volume comply with the condition}
\begin{equation}%
\hlred{pV^{2} = \text{constant}}
\label{eq-110}
\end{equation}
\hlred{We are to find out whether the gas is heated or, on the contrary, cooled in such an expansion.}

\stda Why must the temperature of the gas change?

\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-20/fig-076a}
\caption{Comparing the isochores for $p \propto \dfrac{1}{V^{2}}$ and $p \propto \dfrac{1}{V}$.}
\label{fig-76}
\end{marginfigure}
\tchr If the temperature remained constant, that would mean that the gas expands according to the law of Boyle and Mariotte [equation \eqref{eq-107}]. For an isothermal process $p \propto \dfrac{1}{V}$, while in our case the dependence of $p$ on $V$ is of a different nature: $p \propto \dfrac{1}{V^{2}}$).

\stda Maybe I can try to plot these relationships? The curves will be of the shape shown in \figr{fig-76}.

\tchr That's a good idea. What do the curves suggest?

\stda I seem to understand now. We can see that in tracing the curve $p \propto \dfrac{1}{V^{2}}$ toward greater volumes, the gas will gradually pass over to isotherms that are closer and closer to the origin, i.e, isotherms corresponding to ever-decreasing temperatures. This means that in this expansion process the gas is cooled.

\tchr Quite correct. Only I would reword your answer. It is better to say that such a gas expansion process is possible only provided the gas is cooled.

\stdb Can we reach the same conclusion analytically?

\tchr Of course. Let us consider two states of the gas: $p_{1}, \,V_{1}, \,T_{1}$ and $p_{2},\, V_{2},\, T_{2}$. Next we shall write the combined gas law [see equation \eqref{eq-104}] for each of these states
\begin{align*}%
p_{1} V_{1} & = \frac{m}{\mu} RT_{1}\\
p_{2}V_{2} & = \frac{m}{\mu} RT_{2}
\end{align*}
We can write the given gas expansion process, according to the condition, in the form
\begin{equation*}%
p_{1} V_{1}^{2} = p_{2}V_{2}^{2}
\end{equation*}
Substituting the two preceding equations of the gas law in the last equation, we obtain
\begin{equation*}%
\frac{m}{\mu} R T_{1} V_{1} = \frac{m}{\mu} R T_{2}V_{2}
\end{equation*}
After cancelling the common factors we find that
\begin{equation}
T_{1} V_{1} = T_{2}V_{2}
\label{eq-111}
\end{equation}
From this equation it is evident that if the gas volume is, for example, doubled, its temperature (in the absolute scale) should be reduced by one half.

\stda Does this mean that whatever the process, the gas parameters ($p$, $V$ and $T$) will be related to one another in each instant by the combined gas law?
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-20/fig-077a}
\caption{Work done by a gas.}
\label{fig-77}
\end{marginfigure}
\tchr Exactly. The combined gas law establishes a relationship between the gas parameters regardless of any conditions whatsoever. Now let us consider the nature of the energy exchange between a gas and its environment in various processes. Assume that the gas is expanding. It will move back all bodies restricting its volume (for instance, a piston in a cylinder). Consequently, the gas performs work on these bodies. This work is not difficult to calculate for isobaric expansion of the gas. Assume that the gas expands isobarically and pushes back a piston of cross-sectional area $S$ over a distance $\Delta l$ (\figr{fig-77}). The pressure exerted by the gas on the piston is $p$. Find the amount of work done by the gas in moving the piston: 
\begin{equation}%
A = F \Delta l = (pS) \Delta l = p (S \Delta l) = p(V_{2} - V_{1})
\label{eq-112}
\end{equation}
where $V_{1}$ and $V_{2}$ are the initial and final volumes of the gas. 

The amount of work done by the gas in nonisobaric expansion is more difficult to calculate because the pressure varies in the course of gas expansion. In the general case, the work done by the gas when its volume increases from $V_{1}$ to $V_{2}$ is equal to the area under the $p (V)$ curve between the ordinates $V_{1}$ and $V_{2}$. The amounts of work done by a gas in isobaric and in isothermal expansion from volume $V_{1}$ to volume $V_{2}$ are shown in \figr{fig-78} by the whole hatched area and the crosshatched area, respectively. The initial state of the gas is the same in both cases. Thus, in expanding, a gas does work on the surrounding bodies at the expense of part of its internal energy. The work done by the gas depends upon the nature of the expansion process. Note also that if a gas is compressed, then work is done on the gas and, consequently, its internal energy increases.
\begin{marginfigure}%
\centering
\includegraphics[width=\textwidth]{figs/ch-20/fig-078a}
\caption{Work done by a gas.}
\label{fig-78}
\end{marginfigure}
The performance of work, however, is not the only method of energy exchange between a gas and the medium. For example, in isothermal expansion a gas does a certain amount of work $A$ and, therefore, loses an amount of energy equal to $A$. On the other hand, however, as follows from the principles enumerated in \chap{ch-18} [see equation \eqref{eq-98}], a constant temperature of the gas in an isothermal process should mean that its internal energy $U$ remains unchanged (let me remind you that $U$ is determined by the thermal motion of the molecules and that the mean energy of the molecules is proportional to the temperature $T$). The question is: what kind of energy is used to perform the work in the given case?

\stdb Evidently, the heat transmitted to the gas from the outside.

\tchr Correct. In this manner, we reach the conclusion that a gas exchanges energy with the medium through at least two channels: by doing work associated with a change in the volume of the gas, and by heat transfer. The energy balance can be expressed in the following form
\begin{equation}%
\Delta U = Q - A
\label{eq-113}
\end{equation}
where $\Delta U$ is the increment of internal energy of the gas characterized by an increase in its temperature, $Q$ is the heat transferred to the gas from the surrounding medium, and $A$ is the work done by the gas on the surrounding bodies. Equation \eqref{eq-113} is called the first law of thermodynamics. Note that it is universal and is applicable, not only to gases, but to any other bodies as well.

\stdb To sum up, we may conclude that in isothermal expansion, all the heat transferred to the gas is immediately converted into work done by the gas. If so, then isothermal processes cannot take place in a thermally insulated system.

\tchr Quite true. Now consider isobaric expansion of gas from the energy point of view.

\stdb The gas expands. That means that it performs work. Here, as can be seen from equation \eqref{eq-106}, the temperature of the gas is raised, i.e. its internal energy is increased. Consequently, in this case, a relatively large amount of heat must be transferred to the gas: a part of this heat is used to increase the internal energy of the gas and the rest is converted into the work done by the gas.

\tchr Very good. Consider one more example. A gas is heated so that its temperature is increased by $\Delta T$. This is done twice: once at constant volume of the gas and then at constant pressure. Do we have to expend the same amount of heat to heat the gas in both cases?

\stda I think so.

\stdb I would say that different amounts are required. At constant volume, no work is done, and all the heat is expended to increase the internal energy of the gas, i.e. to raise its temperature. In this case
\begin{equation}%
Q_{1} = C_{1} \Delta T
\label{114}
\end{equation}
At constant pressure, the heating of the gas is inevitably associated with its expansion, so that the amount of work done is $A=p(V-V_{1})$. The supplied heat $Q_{2}$ is used partly to increase the internal energy of the gas (to raise its temperature) and partly to do this work. Thus
\begin{equation}%
Q_{2} = C_{1} \Delta T + p(V-V_{1})
\label{eq-115}
\end{equation}
Obviously, $Q_{1} < Q_{2}$.

\tchr I agree with Student B. What do you call the quantity of heat required to raise the temperature of a body by one degree?

\stdb The heat capacity of the body.

\tchr What conclusion can be drawn from the last example as regards the heat capacity of a gas?

\stdb A gas has two different heat capacities: at constant volume and at constant pressure. The heat capacity at constant volume (which is factor $C_{1}$ in the last two equations) is less than the heat capacity at constant pressure.

\tchr Can you express the heat capacity at constant pressure in terms of $C_{1}$, that is, the heat capacity at constant volume?

\stdb I'll try. Let us denote the heat capacity at constant pressure by $C_{2}$ In accordance with the definition of heat capacity, we can write $C_{2}= Q_{2}/ \Delta T$. Substituting the value of $Q_{2}$ from equation \eqref{eq-115} we obtain
\begin{equation}%
C_{2} = C_{1} + \frac{p(V-V_{1})}{\Delta T} 
\label{eq-116}
\end{equation}

\tchr You stopped too soon. If we apply the equation of the combined gas law, we can write
\begin{equation*}%
p(V-V_{1}) = \frac{m}{\mu} R (T - T_{1}) =  \frac{m}{\mu} R \Delta T
\end{equation*}
after substituting into equation (\ref{eq-116}), we obtain
\begin{equation}%
C_{2} = C_{1} + \frac{m}{\mu} R
\label{eq-117}
\end{equation}
In reference to one gram-molecule of the gas ($m=\mu$), this relationship is even more simple:
\begin{equation}%
C_{2} = C_{1} + R
\label{eq-118}
\end{equation}
In conclusion, let us consider a certain cycle consisting of an isotherm, isochore and isobar (see \figr{fig-79}~\drkgry{(a)} in which axes $p$ and $V$ are used as the coordinate axes). Please draw this same cycle (qualitatively) in a diagram with coordinate axes $V$ and $T$, and analyse the nature of energy exchange between the gas and the medium in each element of the cycle.
\begin{marginfigure}%
\centering
\includegraphics[width=\textwidth]{figs/ch-20/fig-079a}
\caption{Work done by a gas.}
\label{fig-79}
\end{marginfigure}
\stdb In a diagram with coordinate axes $V$ and $T$, the cycle will be of the form illustrated in \figr{fig-79}~\drkgry{(b)}.

\tchr Quite correct. Now please analyse the nature of energy exchange between the gas and the medium in the separate elements of the cycle.

\stdb In element 1-2, the gas undergoes isothermal expansion. It receives a certain amount of heat from the outside and spends all this heat in doing work. The internal energy of the gas remains unchanged.

In element 2-3 of the cycle, the gas is heated isochorically (at constant volume). Since its volume does not change, no work is done. The internal energy of the gas is increased only due to the heat transferred to the gas from the outside. 

In element 3-1 the gas is compressed isobarically (at constant pressure) and its temperature drops as can be seen in \figr{fig-79}~\drkgry{(b)}. Work is done on the gas, but its internal energy is reduced. This means that the gas intensively gives up heat to the medium.

\tchr Your reasoning is absolutely correct.

\stda Our discussion shows me that I knew very little about the gas laws. Do we have, to know all this for the entrance examinations? In my opinion, some of the questions we discussed are beyond the scope of the physics syllabus for students taking entrance examinations.

\tchr If you carefully think over our discussion, you will see that it only covered questions directly concerned with the combined gas law in its general form or as applied in certain special cases. \hlblue{Your confusion should be attributed not to the imaginary stretching of the syllabus, but simply to the fact that you have not thought over and understood the gas laws thoroughly enough.} Unfortunately, examinees frequently don't care to go beyond a very superficial idea of the gas laws.

\end{dialogue}



\cleardoublepage
%%
%%\thispagestyle{empty}
%%\vspace*{-1.95cm}
%%\begin{figure*}[!ht]
%%\centering
%%\includegraphics[width=0.7\textwidth]{figs/sec/sec-h.pdf}
%%\end{figure*}
%%\vspace*{-.4cm}
%%%\begin{centering}
%%\begin{fullwidth}
%%%%\paragraph{}
%%{\textsf{\Large \hlred{Basically, modern physics is molecular physics. Hence it is especially important to obtain some knowledge of the fundamentals of the molecular-kinetic theory of matter, if only by using the simplest example of the ideal gas. The question of the peculiarity in the thermal expansion of water is discussed separately. The gas laws will be analysed in detail and will be applied in the solution of specific engineering problems.
%%}}}
%%\end{fullwidth}
%%
%\cleardoublepage
