% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{How Do You Locate The Centre Of Gravity? }
\label{ch-15}

\begin{dialogue}

\tchr In many cases, examinees find it difficult to locate the centre of gravity of a body of system of bodies. Is everything quite clear to you on this matter?

\stda No, I can't say it is. I don't quite understand how you find the centre of gravity in the two cases shown in \figr{fig-63}~\drkgry{(a)} and \figr{fig-64}~\drkgry{(a)}.

\begin{figure}[!htb]%[3cm]
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-15/fig-063a.pdf}
\caption{Problem is to find the centre of gravity of the given body.}
\label{fig-63}
\end{figure}

\tchr All right. In the first case it is convenient to divide the plate into two rectangles as shown by the dashed line in \figr{fig-63}~\drkgry{(b)}. The centre of gravity of rectangle 1 is at point $A$; the weight of this rectangle is proportional to its area and is equal, as is evident from the figure, to 6 units (here the weight is conditionally measured in square centimetres). The centre of gravity of rectangle 2 is at point the weight of this rectangle is equal to 10 units. Next we project the points $A$ and $B$ on the coordinate axes $O_{x}$ and $O_{y}$; these projections are denoted by $A_{1}$ and $B_{1}$ on the $X$-axis and by $A_{2}$ and $B_{2}$ on the $Y$-axis. Then we consider the ``bars'' $A_{1}B_{1}$ and $A_{2}B_{2}$, assuming that the masses are concentrated at the ends of the ``bars'', the mass of each end being equal to that of the corresponding rectangle (see \figr{fig-63}~\drkgry{(b)}. As a result, the problem of locating the centre of gravity of our plate is reduced to finding the centres of gravity of ``bars'' $A_{1}B_{1}$ and $A_{2}B_{2}$ The positions of these centres of gravity will be the coordinates of the centre of gravity of the plate.

But let us complete the problem. First we determine the location of the centre of gravity of ``bar'' $A_{1}B_{1}$ using the well-known rule of force moments  (see \figr{fig-63}~\drkgry{(b)}): 
\begin{equation*}%
6x= 10(2-x) \; \text{then,} \;\; x=\dfrac{5}{4} \, \si{\centi\meter}
\end{equation*}
Thus, the $X$-coordinate of the centre of gravity of the plate in the chosen system of coordinates is 
\begin{equation*}%
X = (1 +x) \, \si{\centi\meter}= \frac{9}{4} \, \si{\centi\meter}
\end{equation*}
In a similar way we find the centre of gravity of ``bar'' $A_{2}B_{2}$:\\
 \begin{equation*}
 6y=10(1-y)
\end{equation*}
 from which it follows that $y=\dfrac{5}{8} \; \si{\centi\meter}$. Thus the $Y$-coordinate of the centre of gravity of the plate is 
\begin{equation*}%
Y = (1.5+y) \si{\centi\meter}= \frac{17}{8} \si{\centi\meter}
\end{equation*}

\stda Now I understand. That is precisely how I would go about finding coordinate X of the centre of gravity of the plate. I was not sure that coordinate Y could be found in the same way.

\tchr Let us consider the second case, shown in \figr{fig-64}~\drkgry{(a)}. 
\begin{figure}
\centering
\includegraphics[width=0.6\textwidth,angle=2]{figs/ch-15/fig-064a.pdf}
\caption{Problem is to find the centre of gravity of the given body.}
\label{fig-64}
\end{figure}
Two approaches are available. For instance, instead of the given circle with one circular hole, we can deal with a system of two bodies: a circle with two symmetrical circular holes and a circle inserted into one of the holes (\figr{fig-64}~\drkgry{(b)}). The centres of gravity of these bodies are located at their geometric centres. Knowing that the weight of the circle with two holes is proportional to its area, i.e. 
\begin{equation*}%
\left ( \pi R^{2} -  \frac{2 \pi R^{2}}{4} \right) = \frac{\pi R^{2}}{2}
\end{equation*}
and that of the small circle is proportional to its area $ \pi R^{2}/4 $, we reduce the problem to finding the point of application of the resultant of the two parallel forces shown below in \figr{fig-64}~\drkgry{(b)}. We denote by $x$ the distance from the sought-for centre of gravity to the geometric centre of the large circle. Then, according to \figr{fig-64}~\drkgry{(b)}, we can write 
\begin{equation*}%
\left( \frac{\pi R^{2}}{4} \right) \left( \frac{R}{2} -x \right) = \frac{\pi R^{2}}{2} x \;\; \text{from which} \;\; x=\frac{R}{6}
\end{equation*}
There is another possible approach. The given circle with the hole can be replaced by a solid circle (with no hole) plus a circle located at the same place where the hole was and having a negative weight (i.e. one acting upward) \drkgry{(\figr{fig-64}~(c))} which will compensate for the positive weight of the corresponding portion of the solid circle. As a whole, this arrangement corresponds to the initial circle with the circular hole.

In this case, the problem is again reduced to finding the point of application of the resultant of the two forces shown at the bottom of \figr{fig-64}~\drkgry{(c)}. According to the diagram we can write:
\begin{equation*}%
\pi R^{2} x = \left( \frac{\pi R^{2}}{4} \right) \left( \frac{R}{2}+x \right)
\end{equation*}
from which, as in the preceding case, $x=\dfrac{R}{6}$.

\stda I like the first approach better because it does not require the introduction of a negative weight.

\tchr In addition, I want to propose \hlred{a problem involving locating the centre of gravity of the system of loads shown in  \figr{fig-65}~\drkgry{(a)}. We are given six loads of different weights ($P_{1},P_{2}, \ldots{}, P_{6}$) arranged along a bar at equal distances a from one another. The weight of the bar is neglected. How would you go about solving this problem?}
\begin{figure}[!htb]%[-6cm]%
\centering
\includegraphics[width=0.6\textwidth,angle=-2.5]{figs/ch-15/fig-065a.pdf}
\caption{Problem is to find the centre of gravity of the bodies in the given configuration.}
\label{fig-65}
\end{figure}

\stda First I would consider two loads, for instance,  $P_{1}$ and $P_{2}$, and find the point of application of their resultant. Then I would indicate this resultant (equal to the sum $P_{1} + P_{2}$) on the drawing and would cross out forces $P_{1}$ and $P_{2}$, from further consideration. Now, instead of the six forces, only five would remain. Next, I would find the point of application of the resultant of another pair of forces, etc. Thus, by consecutive operations I would ultimately find the required resultant whose point of application is the centre of gravity of the whole system.

\tchr Though your method of solution is absolutely correct, it is by far too cumbersome. I can show you a much more elegant solution. We begin by assuming that we are sup porting the system at its centre of gravity (at point $B$ in \figr{fig-65}~\drkgry{(b)}.

\stdb (interrupting): But you don't yet know the location of the centre of gravity. How do you know that it is between the points of application of forces $P_{3}$ and $P_{4}$?

\tchr It makes no difference to me where exactly the centre of gravity is. I shall not take advantage of the fact that in \figr{fig-65}~\drkgry{(b)} the centre of gravity turned out to be between the points of application of forces $P_{3}$ and $P_{4}$. So we assume we are supporting the system at its centre of gravity. As a result, the bar is in a state of equilibrium. In addition to the six forces, one more force - the bearing reaction $N$ - will act on the bar. Since the bar is in a state of equilibrium, we can apply the conditions of equilibrium (see \chap{ch-14}). We begin with the first condition of equilibrium for the projection of all the forces in the vertical direction
\begin{equation}%
N=P_{1}+P_{2}+P_{3}+P_{4}+P_{5}+P_{6}
\label{eq-86}
\end{equation}
Then we apply the second condition (moment condition), considering the force moments with respect to point $A$ in \figr{fig-65}~\drkgry{(b)} (i.e. the left end of the bar). Here, all the forces tend to turn the bar clockwise, and the bearing reaction tends to turn it counterclockwise. We can write
\begin{equation}%
N (\overline{AB}) = aP_{2} + 2aP_{3} + 3aP_{4} + 4aP_{5} + 5aP_{6}
\label{eq-87}
\end{equation}
Combining conditions \eqref{eq-86} and \eqref{eq-87}, we can find the length $\overline{AB}$, i.e. the required position of the centre of gravity measured from the left end of the bar
\begin{equation}%
\overline{AB} = \frac{aP_{2} + 2aP_{3} + 3aP_{4} + 4aP_{5} + 5aP_{6}}{P_{1}+P_{2}+P_{3}+P_{4}+P_{5}+P_{6}}
\label{eq-88}
\end{equation}
\stda Yes, I must admit that your method is much simpler.

\tchr Also note that your method of solving the problem is very sensitive to the number of loads on the bar (the addition of each load makes the solution more and more tedious). My solution, on the contrary, does not become more complicated when loads are added. With each new load, only
one term is added to the numerator and one to the denominator in equation \eqref{eq-88}.

\stdb Can we find the location of the centre of gravity of the bar if only the moment condition is used?

\tchr Yes, we can. This is done by writing the condition of the equilibrium of force moments with respect to two different points. Let us do precisely that. We will consider the condition for the force moments with respect to points $A$ and $C$ (see \figr{fig-65}~\drkgry{(b)}). For point $A$ the moment condition is expressed by equation (\ref{eq-87}); for point $C$, the equation will be
\begin{equation}%
N (5a-\overline{AB}) = aP_{5}+ 2aP_{4} + 3aP_{3} + 4aP_{2} + 5aP_{1}
\label{eq-89}
\end{equation}
Dividing equation (\ref{eq-87}) by (\ref{eq-89}) we obtain 
\begin{equation*}%
\frac{\overline{AB}}{5a - \overline{AB}} = \frac{aP_{2} + 2aP_{3} + 3aP_{4} + 4aP_{5} + 5aP_{6}
}{aP_{5}+ 2aP_{4} + 3aP_{3} + 4aP_{2} + 5aP_{1}}
\end{equation*}
From which
%\begin{equation}%
\begin{align*}
&  \overline{AB} \left( aP_{5}+ 2aP_{4} + 3aP_{3} + 4aP_{2} + 5aP_{1} + \right. \\
&\qquad  \left. aP_{2} + 2aP_{3} + 3aP_{4} + 4aP_{5} + 5aP_{6} \right) \\
& = 5a \left( aP_{2} + 2aP_{3} + 3aP_{4} + 4aP_{5} + 5aP_{6} \right)
\end{align*}
%\end{equation*}
or
\begin{equation*}%
\begin{split}
& \overline{AB} \times 5a \left(P_{1}+P_{2}+P_{3}+P_{4}+P_{5}+P_{6} \right)\\
& = 5a \left( aP_{2} + 2aP_{3} + 3aP_{4} + 4aP_{5} + 5aP_{6} \right)
\end{split}
\end{equation*}
Thus we obtain the same result as in equation \eqref{eq-88}.

\end{dialogue}

\section*{Problems}
\label{prob-15}
\addcontentsline{toc}{section}{Problems}


\begin{enumerate}[resume=problems]
\item Locate the centre of gravity of a circular disk having two circular holes as shown in \figr{fig-66}. The radii of the holes are equal to one half and one fourth of the radius of the disk.
\begin{figure}[!htb]%[-3cm]	%
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-15/fig-066a.pdf}
\caption{Problem is to find the centre of gravity of the given body.}
\label{fig-66}
\end{figure}


\end{enumerate}


\cleardoublepage

\thispagestyle{empty}
\vspace*{-1.5cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.75\textwidth]{figs/sec/tarasov-qasp-sec-08.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{Archimedes' principle does not usually draw special attention. This is a common mistake of students preparing for physics exams. Highly interesting questions and problems can be devised on the basis of this principle. We shall discuss the problem of the applicability of Archimedes' principle to bodies in a state of weightlessness.
}}}
\end{fullwidth}

\cleardoublepage
