% !TEX root = tarasov-qasp-2.tex

\chapter{How Do You Deal With Motion In A Circle?}
\label{ch-08}

\begin{dialogue}

\tchr I have found from experience that questions and problems concerning motion of a body in a circle turn out to be extremely difficult for many examinees. Their answers to such questions contain a great many typical errors. To demonstrate this let us invite another student
to take part in our discussion. This student doesn't know what we have discussed previously. We shall conditionally call him ``\textsc{Student B}'' (the first student will hereafter be called ``\textsc{Student A}''). 

Will \textsc{Student B} \hlred{please indicate the forces acting on a satellite, or sputnik, in orbit around the earth?} We will agree to neglect the resistance of the atmosphere and the attraction of the moon, sun and other celestial bodies.

\stdb The satellite is subject to two forces: the attraction of the earth and the centrifugal force.

\tchr I have no objections to the attraction of the earth, but \hlred{I don't understand where you got the centrifugal force from. Please explain.}

\stdb If there were no such force, the satellite could not stay in orbit.

\tchr And what would happen to it?

\stdb Why, it would fall to the earth.

\tchr (turning to \textsc{Student A}): Remember what I told you before! This is a perfect example of an attempt to prove that a certain force exists, not on the basis of the interaction of bodies, but by a backdoor manoeuvre - from the nature of the motion of bodies. As you see, the satellite must stay in orbit, so it is necessary to introduce a retaining force. Incidentally, \hlblue{if this centrifugal force really did exist, then the satellite could not remain in orbit because the forces acting on the satellite would cancel out and it would fly at uniform velocity and in a straight line.}

\stda The centrifugal force is never applied to a rotating body. It is applied to the tie (string or another bonding member). It is the centripetal force that is applied to the rotating body.

\stdb Do you mean that only the weight is applied to the satellite?

\tchr Yes, only its weight.

\stdb And, nevertheless, it doesn't fall to the earth?

\tchr The motion of a body subject to the force of gravity is called falling. Hence, the satellite is falling. However, \hlblue{its ``falling'' is in the form of motion in a circle around the earth and therefore can continue indefinitely.} We have already established that the direction of motion of a body and the forces acting on it do not necessarily coincide (see \chap{ch-04}).

\stdb In speaking of the attraction of the earth and the centrifugal force, I based my statement on the formula 
\begin{equation}%
\frac{GmM}{r^{2}} = \frac{mv^{2}}{r}
\label{eq-34}
\end{equation}
where the left-hand side is the force of attraction ($m$=mass of the satellite, $M$ =mass of the earth, $r$=radius of the orbit and $G$=gravitational constant), and the right-hand side is the centrifugal force ($v$=velocity of the satellite). Do you mean to say that this formula is incorrect?
\tchr No, the formula is quite correct. What is incorrect is your interpretation of the formula. You regard equation \eqref{eq-34} as one of equilibrium between two forces. Actually, it is an expression of Newton's second law of motion
\begin{equation}%
F = ma
\tag{34a}
\label{eq-34a}
\end{equation}
where $F=\dfrac{GmM}{r^{2}}$ and $a=\left( \dfrac{v^{2}}{r} \right)$ is the centripetal acceleration.

\stdb I agree that your interpretation enables us to get along without any centrifugal force. \hlred{But, if there is no centrifugal force, there must at least be a centripetal force.} You have not, however, mentioned such a force.

\tchr In our case, \hlblue{the centripetal force is the force of attraction between the satellite and the earth.} I want to underline the fact that \hlblue{this does not refer to two different forces.} By no means. This is one and the same force.

\stdb Then why introduce the concept of a centripetal force at all?

\tchr I fully agree with you on this point. The term ``centripetal force'', in my opinion, leads to nothing but confusion. What is understood to be the centripetal force is not at all an independent force applied to a body along with other forces. It is, instead, \hlblue{the resultant of all the forces applied to a body travelling in a circle at uniform velocity.}

The quantity $mv^{2}/r$ is not a force. It represents the product of the mass $m$ of the body by the centripetal acceleration $v^{2}/r$. This acceleration is directed toward the centre and, consequently, the resultant of all forces, applied to a body travelling in a circle at uniform velocity, is directed toward the centre. Thus, \hlblue{there is a centripetal acceleration and there are forces which, added together, impart a centripetal acceleration to the body.}

\stdb I must admit that this approach to the motion of a body in a circle is to my liking. Indeed, this motion is not a static case, for which an equilibrium of forces is characteristic, but a dynamic case.

\stda If we reject the concept of a centripetal force, then we should probably drop the term ``centrifugal force'' as well, even in reference to ties.

\tchr \hlblue{The introduction of the term ``centrifugal force'' is even less justified.} The centripetal force actually exists, if only as a resultant force. \hlblue{The centrifugal force does not even exist in many cases.}

\stda I don't understand your last remark. The centrifugal force is introduced as a reaction to the centripetal force. If it does not always exist, as you say, then Newton's third law of motion is not always valid. Is that so?

\tchr \hlblue{Newton's third law is valid only for real forces determined by the interaction of bodies, and not for the resultants of these forces.} I can demonstrate this by the example of the conical pendulum that you are already familiar with (\figr{fig-33}). 

\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-08/fig-033a.pdf}
\caption{A conical pendulum. The body attached to the string performs circular motion in the horizontal plane. During this motion the solid that it traces has the shape of a cone.}
\label{fig-33}
\end{figure}

The ball is subject to two forces: the weight $P$ and the tension $T$ of the string. These forces, taken together, provide the centripetal acceleration of the ball, and their sum is called the centripetal force. Force $P$ is due to the interaction of the ball with the earth. The reaction of this force is force $P_{1}$ which is applied to the earth. Force $T$ results from interaction between the ball and the string. The reaction of this force is force $T_{1}$ which is applied to the string. 

If forces $P_{1}$ and $T_{1}$ are formally added together we obtain a force which is conventionally understood to be the centrifugal force (see the dashed line in \figr{fig-33}). But to what is this force applied? Are we justified in calling it a force when one of its components is applied to the earth and the other to an entirely different body-the string? Evidently, in the given case, the concept of a centrifugal force has no physical meaning.

\stda In what cases does the centrifugal force exist?

\tchr In the case of a satellite in orbit, for instance, when only two bodies interact the earth and the satellite. The centripetal force is the force with which the earth attracts the satellite. \hlblue{The centrifugal force is the force with which the satellite attracts the earth.}

\stdb You said that \hlblue{Newton's third law was not valid for the resultant of real forces.} I think that in this case it will be invalid also for the components of a real force. Is that true?

\tchr Yes, quite true. In this connection I shall cite an example which has nothing in common with rotary motion. A ball lies on a floor and touches a wall which makes an obtuse
angle with the floor (\figr{fig-34}). 

\begin{marginfigure}%[!ht]
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-08/fig-034a.pdf}
\caption{A ball on the floor touching a wall at an obtuse angle with the floor.}
\label{fig-34}
\end{marginfigure}

Let us resolve the weight of the ball into two components: perpendicular to the wall and parallel to the floor. We shall deal with these two components instead of the weight of the ball. If Newton's third law were applicable to separate components, we could expect a reaction of the wall counter balancing the component of the weight perpendicular to it. Then, the component of the weight parallel to the floor would remain unbalanced and the ball would have to have a horizontal acceleration. Obviously, this is physically absurd.

\stda So far you have discussed uniform motion in a circle. \hlred{How do you deal with a body moving nonuniformly in a circle?} For instance, a body slides down from the top of a vertically held hoop. While it slides along the hoop it is moving in a circle. This cannot, however, be uniform motion because the velocity of the body increases. What do you, do in such cases?

\tchr If a body moves in a circle at uniform velocity, the resultant of all forces applied to the body must be directed to the centre; it imparts centripetal acceleration to the body. In the more general case of nonuniform motion in a circle, the resultant force is not directed strictly toward the centre. In this case, it has a component along a radius toward the centre and another component tangent to the trajectory of the body (i.e. to the circle). The first component is responsible for the centripetal acceleration of the body, and the second component, for the so-called tangential acceleration, associated with the change in velocity. It should be pointed out that since the velocity of the body changes, the centripetal acceleration $v^{2}/r$ must also change.

\stda Does that mean that for each instant of time the centripetal acceleration will be determined by the formula $a = v^{2}/r$, where $v$ is the instantaneous velocity?

\tchr Exactly. While the centripetal acceleration is constant in uniform motion in a circle, it varies in the process of motion in nonuniform motion in a circle.

\stda What does one do to find out just how the velocity $v$ varies in nonuniform rotation?

\tchr Usually, the law of conservation of energy is resorted to for this purpose. Let us consider a specific example.
\hlred{Assume that a body slides without friction from the top of a vertically held hoop of radius $R$. With what force will the body press on the hoop as it passes a point located at a height $h$ \si{\centi\metre} below the top of the hoop? The initial velocity of the body at the top of the hoop equals zero.} 

First of all, it is necessary to find what forces act on the body.
\begin{figure}[!ht]%
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-08/fig-035a.pdf}
\caption{Resolution of forces acting on a body in nonuniform circular motion.}
\label{fig-35}
\end{figure}
\stda Two forces act on the body: the weight $P$ and
the bearing reaction $N$. They are shown in \figr{fig-35}.

\tchr Correct. What are you going to do next?

\stda I'm going to do as you said. I shall find the resultant of these two forces and resolve it into two components: one along the radius and the other tangent to the circle.

\tchr Quite right. Though it would evidently be simpler to start by resolving the two forces applied to the body in the two directions instead of finding the resultant, the more so because it will be necessary to resolve only one force - the weight.

\stda My resolution of the forces is shown in \figr{fig-35}.

\tchr Force $P_{2}$ is responsible for the tangential acceleration of the body, it does not interest us at present. The resultant of forces $P_{1}$ and $N$ causes the centripetal acceleration of the body, i.e.
\begin{equation}%
P_{1} - N = \frac{mv^{2}}{R}
\label{eq-35}
\end{equation}
The velocity of the body at the point we are interested in (point $A$ in \figr{fig-35}) can be found from the law of conservation of energy
\begin{equation}
Ph = \frac{mv^{2}}{2}
\label{eq-36}
\end{equation}
Combining equations \eqref{eq-35} and \eqref{eq-36} and taking into consideration that
\begin{equation*}%
P_{1}=P \cos \alpha = P \left(\frac{R-h}{R}\right),
\end{equation*}
 we obtain
\begin{equation*}
\frac{P}{R} \left( R-h \right) - N = \left(\frac{2Ph}{R}\right),
\end{equation*}
The sought-for force with which the body presses on the hoop is equal, according to Newton's third law, to the bearing reaction
\begin{equation}%
N=P \left( \frac{R-3h}{R} \right)
\label{eq-37}
\end{equation}
\stdb You assume that at point $A$ the body is still on the surface of the hoop. But it may fly off the hoop before it gets to point $A$.

\tchr We can find the point at which the body leaves the surface of the hoop. This point corresponds to the extreme case when the force with which the body presses against the
hoop is reduced to zero. Consequently, in equation \eqref{eq-37}, we assume $N=0$ and solve for $h$, i.e, the vertical distance from the top of the hoop to the point at which the body flies off. Thus
\begin{equation}%
h_{0} = \frac{R}{3}
\label{eq-38}
\end{equation}
If in the problem as stated the value of $h$ complies with the condition $h < h_{0}$ then the result of equation \eqref{eq-37} is correct; if, instead, $h\geqslant h_{0}$, then $N=0$.

\stda As far as I can make out, two physical laws, equations \eqref{eq-35} and \eqref{eq-36}, were used to solve this problem.

\tchr Very good that you point this out. Quite true, two laws are involved in the solution of this problem: Newton's second law of motion [see equation \eqref{eq-35}] and the law of conservation of energy  [see equation \eqref{eq-36}]. Unfortunately, examinees do not always clearly understand just which physical laws they employ in solving some problem or other. This, I think, is an essential point.

Take, for instance, the following example. 
\hlred{An initial velocity $v_{0}$ is imparted to a body so that it can travel from point $A$ to point $C$. Two alternative paths leading from $A$ to $C$ are offered \drkgry{(\figr{fig-36}~(a),~(b))}. In both cases the body must reach the same height $H$, but in different ways. Find the minimum initial velocity $v_{0}$ for each case. Friction can be neglected.}

\stdb I think that the minimum initial velocity should be the same in both cases, because there is no friction and the same height is to be reached. This velocity can be calculated from the law of conservation of energy
\begin{equation*}%
mgH = \frac{mv_{0}^{2}}{2} \, \, \text{from which} \, \, v_{0} = \sqrt{2gH}
\end{equation*}
\begin{marginfigure}
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-08/fig-036a.pdf}
\caption{A body travels from point $A$ to point $C$ via two different paths. The problem is to find the minimum initial velocity in the two cases.}
\label{fig-36}
\end{marginfigure}

\tchr Your answer is wrong. You overlooked the fact that in the first case, the body passes the upper point of its trajectory when it is in a state of rotational motion. This means that at the top point $B$ (\figr{fig-36}~\drkgry{(a)}) it will have a velocity $v_{1}$ determined from a dynamics equation similar to equation \eqref{eq-35}. Since the problem involves the finding of a minimum, we should consider the extreme case when the pressure of the body on its support at point $B$ is reduced to zero. Then only the weight will be acting on the body and imparting to it the centripetal acceleration. Thus
\begin{equation}%
mg=\frac{mv_{1}^{2}}{R} = \frac{2mv_{1}^{2}}{H}
\label{eq-39}
\end{equation}
Adding to the dynamics equation \eqref{eq-39} the energy equation
\begin{equation}%
\frac{mv_{0}^{2}}{2} = \frac{mv_{1}^{2}}{2} + mgH
\label{eq-40}
\end{equation}
we find that the minimum initial velocity equals. $\sqrt{5gH/2}$. In the second case, the body may pass the top point at a velocity infinitely close to zero and so we can limit ourselves to the energy equation. Then your answer is correct.

\stdb Now I understand. If in the first case the body had no velocity at point $B$, it would simply fall off its track.

\tchr If in the first case the body had the initial velocity $v_{0}=\sqrt{2gH}$ as you suggested, it would never reach point $B$ but would fall away from the track somewhat earlier. I propose that you \hlred{find the height $h$ of the point at which the body would fall away from the track if its initial velocity was $v_{0}=\sqrt{2gH}$.}

\stda Please let me try to do this problem.

\tchr Certainly.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-08/fig-037a.pdf}
\caption{A body moving on a circular track. The problem is to find the point at which it would fall away from the track if the initial velocity is $v_{0}=\sqrt{2gH}$.}
\label{fig-37}
\end{figure}
\stda At the point the body drops off the track the bearing reaction is evidently equal to zero. Therefore, only the weight acts on the body at this point. We can resolve the weight into two components, one along the radius ($mg \cos \alpha$) and the other perpendicular to the radius ($mg \sin \alpha$) as shown in \figr{fig-37} (point $A$ is the point at which the body falls away from the track). The component along the radius imparts a centripetal acceleration to the body, determined by the equation
\begin{equation}%
mg \cos \alpha = \frac{mv_{2}^{2}}{2}
\label{eq-41}
\end{equation}
where $v_{2}$ is the velocity of the body at point $A$. To find it we can make use of the energy equation
\begin{equation}%
\frac{mv_{2}^{2}}{2} + mgh = \frac{mv_{0}^{2}}{2} 
\label{eq-42}
\end{equation}
Combining the dynamics \eqref{eq-41} and energy \eqref{eq-42} equations, taking into consideration that $\cos \alpha = (h-R)/R$ we obtain
\begin{equation*}%
mg(h-R) = mv_{0}^{2} - 2 mgh
\end{equation*}
from which
\begin{equation}%
h = \frac{2v_{0}^{2}+gH}{6g} 
\label{eq-43}
\end{equation}
After substituting $v_{0}^{2}=2gH$ the final result is
\begin{equation*}%
h = \frac{5}{6} H
\end{equation*}

\tchr Entirely correct. Note that you can use equation \eqref{eq-43} to find the initial velocity $v_{0}$ for the body to loop the loop. For this we take $h=H$ in equation \eqref{eq-43}. Then
\begin{equation*}%
H = \frac{2v_{0}^{2}+gH}{6g} 
\end{equation*}
From this we directly obtain the result previously determined
\begin{equation*}%
v_{0} = \sqrt{\frac{5gH}{2}}
\end{equation*}

\stda Condition (\ref{eq-43}) was obtained for a body falling off its track. How can it be used for the case in which the body loops the loop without falling away?

\tchr Falling away at the very top point of the loop actually means that the body does not fall away but passes this point, continuing its motion in a circle.

\stdb One could say that the body falls away as if only for a single instant.
\begin{marginfigure}%[-6cm]
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-08/fig-038a.pdf}
\caption{A body lies on a rotating inclined plane. Problem is to find the maximum value of static friction coefficient at which body remains on the plane without sliding off.}
\label{fig-38}
\end{marginfigure}

\tchr Quite true. In conclusion I propose the following problem \drkgry{(\figr{fig-38}~(a))}.
 \hlred{A body lies at the bottom of an inclined plane with an angle of inclination $\alpha$. This plane rotates at uniform angular velocity $\omega$ about a vertical axis. The distance from the body to the axis of rotation of the plane equals $R$. Find the minimum coefficient $k_{0}$ (I remind you that this coefficient characterizes the maximum possible value of the force of static friction) at which the body remains on the rotating inclined plane without sliding off.}
 
Let us begin as always with the question: what forces are applied to the body?

\stda Three forces are applied to the body: the weight $P$, bearing reaction $N$, and the force of friction $F_{fr}$.

\tchr Quite correct. It's a good thing that you didn't add the centripetal force. Now what are you going to do next?

\stda Next, I shall resolve the forces in the directions along the plane and perpendicular to it as shown in \figr{fig-38}~\drkgry{(b)}.
% TODO 38(b) and (c) can be separated and presented as margin figures here 

\tchr I'll take the liberty of interrupting you at this point. I don't like the way you have resolved the forces. Tell me, in what direction is the body accelerated?

\stda The acceleration is directed horizontally. It is centripetal acceleration.

\tchr Good. That is why you should resolve the forces horizontally (i.e. along the acceleration) and vertically (i.e. perpendicular to the acceleration). Remember what we discussed in \chap{ch-06}.

\stda Now I understand. The resolution of the forces in the horizontal and vertical directions is shown in \figr{fig-38}~{(c)}. The vertical components of the forces counterbalance one another, and the horizontal components impart acceleration to the body. Thus
\begin{equation*}%
\left.
\begin{aligned}
N \cos \alpha + F_{fr} \sin \alpha = P \\
F_{fr} \cos \alpha -N \sin \alpha = \frac{mv^{2}}{R}
\end{aligned}
\right\}
\end{equation*}

Taking into consideration that 
\begin{equation*}%
F_{fr}=k_{0}N, \, \, \left( \frac{v^{2}}{R} \right)=\omega^{2}R \, \, \text{and} \, \, m=\left(\frac{P}{g} \right),
\end{equation*}
we can rewrite these equations in the form
\begin{equation*}%
\left.
\begin{aligned}
N (\cos \alpha + k_{0} \sin \alpha)=P \\
N(k_{0} \cos \alpha -\sin \alpha)=P \omega^{2} Rg
\end{aligned}
\right\}
\end{equation*}
\stdb You have only two equations and three unknowns: $k_{0}$, $P$ and $N$.
\\
\tchr That is no obstacle. We don't have to find all three
unknowns, only the coefficient $k_{0}$. The unknowns $P$ and $N$ can be easily eliminated by dividing the first equation by the second.

\stda After dividing we obtain
\begin{equation*}%
\frac{\cos \alpha+ k_{0} \sin \alpha}{k_{0} \cos \alpha - \sin \alpha} = \frac{g}{\omega^{2}R}
\end{equation*}
which we solve for the required coefficient
\begin{equation}%
k_{0} =\left( \frac{\omega^{2}R \cos \alpha+g \sin \alpha}{g \cos \alpha- \omega^{2} R \sin \alpha} \right)
\label{eq-44}
\end{equation}
\tchr It is evident from equation \eqref{eq-44} that the condition 
\begin{equation*}%
\left( g \cos \alpha- \omega^{2}R \sin \alpha \right) >0
\end{equation*}
should hold true. This condition can also be written in the form
\begin{equation}%
\tan \alpha < \frac{g}{\omega^{2}R}
\label{eq-45}
\end{equation}
If condition \eqref{eq-45} is not complied with, no friction force is capable of retaining the body on the rotating inclined plane.
\end{dialogue}

\section*{Problems}
\label{prob-08}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume*=problems]
\item What is the ratio of the forces with which an army tank bears down
on the middle of a convex and of a concave bridge? The radius of curvature
of the bridge is \SI{40}{\metre} in both cases and the speed of the tank is \SI[per-mode=symbol]{45}{\kilo\metre\per\hour}.
\item A body slides without friction from the height $H=\SI{60}{\centi\metre}$ and then loops the
loop of radius $R=\SI{20}{\centi\metre}$ (\figr{fig-39}). Find the ratio of the forces with which the body bears against the track at points $A$, $B$ and $C$.
\begin{figure}[!ht]%[-6cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-08/fig-039a.pdf}
\caption{A body slides from a given height and then loops on a circular track. Problem is to find ratio of forces which the body bears against the track at points $A$, $B$ and $C$. See Problem 16.}
\label{fig-39}
\end{figure}
\item A body can rotate in a vertical plane at the end of a string of length $R$. What horizontal velocity should be imparted to the body in the top position so that the tension of the string in the bottom position is ten times as great as the weight of the body?
\item Calculate the density of the substance of a spherical planet if a satellite rotates about it with a period $T$ in a circular orbit at a distance from the surface of the planet equal to one half of its radius
$R$. The gravitational constant is denoted by $G$.
\item A body of mass $m$ can slide without friction along a trough bent in the form of a circular arc of radius $R$. At what height $h$ will the body be at rest if the trough rotates at a uniform angular velocity $\omega$ (\figr{fig-40}) about a vertical axis? What force $F$ does the body exert on the trough?
\begin{figure}[!ht]%[-6cm]
\centering
\includegraphics[width=0.35\textwidth]{figs/ch-08/fig-040a.pdf}
\caption{A body slides without friction on a uniformly rotating circular trough. Problem is to find the force the body exerts on the trough and the height at which the body will be at rest. See Problem 19.}
\label{fig-40}
\end{figure}

\item A hoop of radius R is fixed vertically on the floor. A body slides without friction from the top of the hoop (\figr{fig-41}). At what distance $l$ from the point where the hoop is fixed will the body fall?
\begin{figure}[!ht]%[1cm]
\centering
\includegraphics[width=0.35\textwidth]{figs/ch-08/fig-041a.pdf}
\caption{A body slides without friction form top of the hoop. Problem is to find distance $l$ from the point where the hoop is fixes and the body will fall. See Problem 20.}
\label{fig-41}
\end{figure}
\end{enumerate}
%\end{dialogue}
%\cleardoublepage
%
%\thispagestyle{empty}
%\vspace*{-2cm}
%\begin{figure*}[!ht]
%\centering
%\includegraphics[width=0.7\textwidth]{figs/ch-08/figs/sec/sec-c.pdf}
%\end{figure*}
%%\begin{centering}
%\begin{fullwidth}
%%%\paragraph{}
%{\textsf{\Large \hlred{Motion in a circle is the simplest form of curvilinear motion. The more important it is to comprehend the specific features of such motion. You can see that the whole universe is made up of curvilinear motion. Let us consider uniform and non-uniform motion of a material point in a circle, and the motion of orbiting satellites. This will lead us to a discussion of the physical causes of the weightlessness of bodies.
%}}}
%\end{fullwidth}
