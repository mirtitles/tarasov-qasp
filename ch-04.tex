% !TEX root = tarasov-qasp-2.tex


\chapter[How Well Do You Know Newton's Laws Of Motion?]{How Well Do You Know \\Newton's Laws Of Motion?}
\label{ch-04}

\begin{dialogue}
\tchr \hlred{Please state Newton's first law of motion.}

\std A body remains at rest or in a state of uniform motion in a straight line until the action of other bodies compels it to change that state. 

\tchr \hlred{Is this law valid in all frames of reference?}

\std I don't understand your question. 

\tchr If you say that a body is at rest, you mean that it is stationary with respect to some other body which, in the given case, serves as the reference system, or frame of reference. It
is quite pointless to speak of a body being in a state of rest or definite motion without indicating the frame of reference. The nature of the motion of a body depends upon the choice of the frame of reference. For instance, a body lying on the floor of a travelling railway car is at rest with respect to a frame of reference attached to the car, but is moving with respect to a frame of reference attached to the track. Now we can return to my question. Is Newton's first law valid for all frames of reference?

\std Well, it probably is.

\tchr I see that this question has taken you unawares. Experiments show that Newton's first law is not valid for all reference systems. Consider the example with the body lying
on the floor of the railway car. We shall neglect the friction between the body and the floor. First we shall deal with the position of the body with respect to a frame of reference attached to the car. We can observe the following: the body rests on the floor and, all of a sudden, it begins to slide along the floor even though no action of any kind is evident. Here we have an obvious violation of Newton's first law of motion. The conventional explanation of this effect is that the car, which had been travelling in a straight line and at uniform velocity, begins to slow down, because the train is braked, and the body, due to the absence of friction, continues to maintain its state of uniform straight-line motion with respect to the railway tracks. From this we can conclude that Newton's law holds true in a frame of reference attached to the railway tracks, but not in one attached to a car being slowed down.

\hlblue{Frames of reference for which Newton's first law is valid are said to be inertial; those in which it is not valid are non-inertial.} For most of the phenomena we deal with we can assume that any frame of reference is inertial if it is attached to the earth's surface, or to any other bodies which are at rest with respect to the earth's surface or travel in a straight line at uniform velocity. Non-inertial frames of reference are systems travelling with acceleration (or deceleration), for instance rotating systems, accelerating or decelerating lifts, etc. Note that not only Newton's first law of motion is invalid for non-inertial reference systems, but his second law as well (since the first law is a particular case of the second law).

\std But if Newton's laws cannot be employed for frames of reference travelling with acceleration, then how can we deal with mechanics in such frames?

\tchr Newton's laws of motion can nevertheless be used for non-inertial frames of reference. To do this, however, it will be necessary to apply, purely formally, an additional
force to the body. This force, the so called inertial force, equals the product of the mass of the body by the acceleration of the reference system, and its direction is opposite to the acceleration of the body. \hlblue{I should emphasize that no such force actually exists but, if it is formally introduced, then Newton's laws of motion will hold true in a non-inertial frame of reference.}

I want to advise you, however, to employ only inertial frames of reference in solving problems. Then, all the forces that you have to deal with will be really existing forces.

\std But if we limit ourselves to inertial frames of reference, then we cannot analyse, for instance, a problem about a body lying on a rotating disk.

\tchr Why can't we? The choice of the frame of reference is up to you. If in such a problem you use a reference system attached to the disk (i.e. a non-inertial system), the body is considered to be at rest. But if your reference system is attached to the earth (i.e. an inertial reference system), then the body is dealt with as one travelling in a circle.

I would advise you to choose an inertial frame of reference. And now \hlred{please state Newton's second law of motion.}

\std This law can be written as $F=ma$, where $F$ is the
force acting on the body, $m$ is its mass and $a$ - acceleration.

\tchr Your laconic answer is very typical. I should make three critical remarks on your statement; two are not very important and one is essential. In the first place, \hlblue{it is not the force that results from the acceleration, but, on the contrary, the acceleration is the result of the applied force.} It is therefore more logical to write the equation of the law as
\begin{equation}%
a=\frac{BF}{m}
\label{eq-10}
\end{equation}
where $B$ is the proportionality factor depending upon the choice of units of measurement of the quantities in equation \eqref{eq-10}. Notice that your version had no mention of the proportionality factor $B$. Secondly, a body is accelerated by all forces applied to it (though some may counterbalance one another). Therefore, \hlblue{in stating the law you should use, not the term ``force'', but the more accurate term ``resultant force''.}

My third remark is the most important. Newton's second law establishes a relationship between force and acceleration. But force and acceleration are vector quantities, characterized not only by their numerical value (magnitude) but by their direction as well. \hlblue{Your statement of the law fails to specify the directions. This is an essential shortcoming.} Your statement leaves out a vital part of Newton's second law of motion. Correctly stated it is: the acceleration of a body is directly proportional to the resultant of all forces acting on the body, inversely proportional to the mass of the body and takes place
in the direction of the resultant force. This statement can be analytically expressed by the formula
\begin{equation}%
\vec{a}=\frac{B\vec{F}}{m}
\label{eq-11}
\end{equation}
(where the arrows over the letters denote vectors).

\std When in \chap{ch-02} we discussed the forces applied to a body thrown upward at an angle to the horizontal, you said you would show later that the direction of motion of a body does not necessarily coincide with the direction of the force applied to it. You referred then to Newton's second law.
\begin{marginfigure}%[!ht]%
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-04/fig-018a.pdf}
\caption{Depicting change in velocity vectorially.}
\label{fig-18}
\end{marginfigure}

\tchr Yes, I remember, and I think it would be quite appropriate to return to this question. Let us recall what acceleration is. As we know, acceleration is characterized by the change in velocity in unit time. Illustrated in \figr{fig-18} are the velocity vectors $\vec{v_{1}}$ and $\vec{v_{2}}$ of a body for two nearby instants of time $t$ and $t+\Delta t$. The change in velocity during the time $\Delta t$ is the vector $\Delta \vec{v} =\vec{v_{2}} - \vec{v_{1}}$. By definition, the acceleration is
\begin{equation}%
\vec{a}(t) \cong \lim_{\Delta t \rightarrow 0}\frac{\Delta \vec{v}}{\Delta t}
\label{eq-12}
\end{equation}
or, more rigorously,
\begin{equation}%
\vec{a}(t) = \lim_{\Delta t \rightarrow 0}\frac{\Delta \vec{v}}{\Delta t}
\label{eq-13}
\end{equation}
It follows that the acceleration vector is directed along vector $\Delta v$, which represents the change in velocity during a sufficiently short interval of time. It is evident from \figr{fig-18} that the velocity vectors and the change in velocity vector can be oriented in entirely different directions. This means that, \hlblue{in the general case, the acceleration and velocity vectors are also differently oriented.} Is that clear?

\std Yes, now I understand. For example, when a body travels in a circle, the velocity of the body is directed along a tangent to the circle, but its acceleration is directed along
a radius toward the centre of rotation (I mean centripetal acceleration).

\tchr Your example is quite appropriate. Now let us return to relationship (equation \eqref{eq-11}) and make it clear that it is precisely the acceleration and not the velocity that is oriented
in the direction of the applied force, and that it is again the acceleration and not the velocity that is related to the magnitude of this force. On the other hand, \hlblue{the nature of a body's motion at any given instant is determined by the direction and magnitude of its velocity at the given instant (the velocity vector is always tangent to the path of the body).}

Since the acceleration and velocity are different vectors, the direction of the applied force and the direction of motion of the body may not coincide in the general case. Consequently, \hlblue{the nature of the motion of a body at a given instant is not uniquely determined by the forces acting on the body at the given instant.}

\std This is true for the general case. But, of course, the direction of the applied force and the velocity may coincide.

\tchr Certainly, that is possible. Lift a body and release it carefully, so that no initial velocity is imparted to it. Here the direction of motion will coincide with the direction of the force of gravity. If, however, you impart a horizontal initial velocity to the body then its direction of
motion will not coincide with the direction of the gravity force; the body will follow a parabolic path. Though in both cases the body moves due to the action of the same force - its weight - the nature of its motion differs. A physicist would say that this difference is due to the different initial conditions: at
the beginning of the motion the body had no velocity in the first case and a definite horizontal velocity in the second.
\begin{marginfigure}%
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-04/fig-019a.pdf}
\caption{Trajectories of bodies with different initial velocities.}
\label{fig-19}
\end{marginfigure}

Illustrated in \figr{fig-19} are the trajectories of bodies thrown with initial velocities of different directions, but in all cases the same force, the weight of the body, is acting on it.

\std Does that mean that the nature of the motion of a body at a given instant depends not only on the forces acting on the body at this instant, but also on the initial conditions?

\tchr Exactly. It should be emphasized that the initial conditions reflect the prehistory of the body. They are the result of forces that existed in the past. These forces no longer exist, but the result of their action is manifested. From the philosophical point of view, this demonstrates the relation of the past to the present, i.e, the principle of causality.

Note that if the formula of Newton's second law contained the velocity and not the acceleration, this relationship of the past and present would not be revealed. In this case, the velocity of a body at a given instant (i.e. the nature of its motion at a given instant) would be fully determined by the
forces acting on the body precisely at this instant; the past would have no effect whatsoever on the present.
\begin{marginfigure}%[-3cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-04/fig-020a.pdf}
\caption{A ball hanging from a string can have different resultant motions depending on the initial conditions.}
\label{fig-20}
\end{marginfigure}
I want to cite one more example illustrating the aforesaid. It is shown in \figr{fig-20}: a ball hanging on a string is subject to the action of two forces, the weight and the tension of the string. If it is deflected to one side of the equilibrium position and then released, it will begin to oscillate. If, however, a definite velocity is imparted to the ball in a direction perpendicular to the plane of deviation, the ball will begin to travel in a circle at uniform velocity. As you can see, depending upon the initial conditions, the ball either oscillates in a plane (see \figr{fig-20}~\drkgry{(a)}), or travels at uniform velocity in a circle (see \figr{fig-20}~\drkgry{(b)}). Only two forces act on it in either case: its weight and the tension of the string.

\std I haven't considered Newton's laws from this viewpoint.

\tchr No wonder then that some students, in trying to determine the forces applied to a body, base their reasoning on the nature of motion without first finding out what bodies interact with the given body. You may recall that you did the same. That is exactly why, when drawing \figr{fig-08}~\drkgry{(c)} and \figr{fig-08}~\drkgry{(d)}, it seemed to you that the sets of forces applied to the body in those cases should be different. Actually, in both cases two forces are applied to the body: its weight and the tension of the string.

\std Now I understand that \hlblue{the same set of forces can cause motions of different nature and therefore data on the nature of the motion of a body cannot serve as a starting point in determining the forces applied to the body.}

\tchr You have stated the matter very precisely. There is no need, however, to go to the extremes. Though different kinds of motion may be caused by the same set of forces (as
in \figr{fig-20}), the numerical relations between the acting forces differ for the different kinds of motion. This means that there will be a different resultant applied force for each motion. Thus, for instance, in uniform motion of a body in a circle, the resultant force should be the centripetal one; in oscillation in a plane, the resultant force should be the restoring force. From this it follows that even though data on the kind of motion of a body cannot serve as the basis for determining the applied forces, they are far from superfluous.

In this connection, let us return to the example illustrated in \figr{fig-20}. Assume that the angle $\alpha$, between the vertical and the direction of the string is known and so is the weight $P$ of the body. Find the tension $T$ in the string when 
\begin{enumerate*}[label=(\arabic*)]
\item  the oscillating body is in its extreme position, and
\item when the body is travelling uniformly in a circle.
\end{enumerate*}

In the first case, the resultant force is the restoring force and it is perpendicular to the string. Therefore, the weight $P$ of the body is resolved into two components, with one component along the
resultant force and the other perpendicular to it (i.e. directed along the string). Then the forces
perpendicular to the resultant force, i.e. those acting in the direction along the string, are equated to each other (see \figr{fig-21}~\drkgry{(a)}). Thus
\begin{equation*}%
T_{1} = P \cos \alpha
\end{equation*}
\begin{figure}%
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-04/fig-021a.pdf}
\caption{Resolving the forces on a moving pendulum.}
\label{fig-21}
\end{figure}
In the second case, the resultant force is the centripetal one and is directed horizontally. Hence, the tension $T_{2}$ of the string should be resolved into a vertical and a horizontal force, and the forces perpendicular to the resultant force, i.e, the vertical forces, should be equated to each other (\figr{fig-21}~\drkgry{(b)}). 

Then
\begin{equation*}%
T_{2} \cos \alpha = P \quad \text{or} \quad T_{2} = \frac{P}{\cos \alpha}
\end{equation*}
As you can see, a knowledge of the nature of the body's motion proved useful in finding the tension of the string.

\std If I understand all this correctly, then, knowing the interaction of bodies, you can find the forces applied to one of them; if you know these forces and the initial conditions, you can predict the nature of the motion of the body (the magnitude and direction of its velocity at any instant).
On the other hand, if you know the kind of motion of a body you can establish the relationships between the forces applied to it. Am I reasoning correctly?

\tchr Quite so. But let us continue. I want to propose a comparatively simple problem relating to Newton's second law of motion. \hlred{Two bodies, of masses $M$ and $m$, are raised to the same height above the floor and are released simultaneous. Will the two bodies reach the floor simultaneously if the resistance of the air is the same for each of them? For slmplicity we shall assume that the air resistance is constant.}

\std Since the air resistance is the same for the two bodies, it can be disregarded. Consequently, both bodies reach the floor simultaneously.


\tchr You are mistaken. You have no right to disregard the resistance of the air. Take, for example, the body of mass $M$. It is subject to two forces: the weight $Mg$ and the air resistance $F$. The resultant force is $Mg-F$. From this we find the acceleration. Thus
\begin{equation*}%
a = \dfrac{Mg -F}{M} = g - \dfrac{F}{M}
\end{equation*}
In this manner, the body of larger mass has a higher acceleration and will, consequently, reach the floor first.

Once more I want to emphasize that in calculating the acceleration of a body it is necessary to take into account I all the forces applied to it, i.e. you must find the resultant force. In this connection, the use of the term ``driving force'' is open to criticism. This term is inappropriate. In applying it to some force (or to several forces) we seem to single out the role of this force (or forces) in imparting acceleration to the body. As if the other forces concerned were less essential This is absolutely wrong. \hlblue{The motion of a body is a result of the action of all the forces applied to it without any exceptions} (of course, the initial conditions should be taken into account).

Let us now consider an example on Newton’s third law of motion. A horse starts to pull a waggon. As a result, the horse and waggon begin to travel with a certain acceleration According to Newton’s third law, whatever the force with which the horse pulls the waggon, the waggon pulls back on the horse with exactly the same force but in the opposite direction. This being so, \hlred{why do the horse and waggon travel forward with an acceleration? Please explain.}

\std I have never given this any thought but I see no contradictions. The acceleration would be difficult to ex plain if the force with which the horse acts on the waggon was counterbalanced by the force with which the waggon acts on the horse. But these forces cannot cancel each other since they are applied to different bodies: one to tho horse and the other to the waggon.

\begin{figure}%[-3cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-04/fig-022a.pdf}
\caption{Why do the horse and waggon travel forward with an acceleration?}
\label{fig-22}
\end{figure}

\tchr Your explanation is applicable to the case when the waggon is not harnessed to the horse. Then the horse pushes away from the waggon, as a result of which the waggon moves in one direction and the horse in the other. The case I proposed is entirely different. The horse is harnessed to the waggon. Thus they are linked together and travel as a single system. The forces of interaction between the horse and waggon that you mentioned are applied to different parts of the same system. In the motion of this system as a whole these forces can be regarded as mutually counterbalancing forces. Thus, you haven't yet answered my question.

\std Well, then I can't understand what the matter is. Maybe the action here is not fully counterbalanced by the reaction? After all a horse is a living organism.

\tchr Now don't let your imagination run away with you. It was sufficient for you to meet with some difficulty and you are ready to sacrifice one of the principal laws of mechanics. To answer my question, there is no need to revise Newton's third law of motion. On the contrary, let us use this law as a basis for our discussion.

According to the third law. the interaction of the horse and the waggon cannot lead to the motion of
this system as a whole (or, more precisely, it cannot impart acceleration to the system as a whole). This being so there must exist some kind of supplementary interaction. In other words at least one more body must participate in the problem in addition to the horse and waggon. This body, in the given case is the earth. As a result, we have three interactions to deal’ with instead of one, namely: 
\begin{enumerate}[label=(\arabic*),leftmargin=1cm]
\item between the horse and the waggon (we shall denote this force by $f_{0}$; 
\item between the horse and the earth (force $F$), in which the horse pushes against the ground; and \item between the waggon and the earth (force $f$) which is the friction of the waggon against the ground.
\end{enumerate}
 All bodies are shown in \figr{fig-22}: the horse, the waggon and the earth and two forces are applied to each body. These two forces are the result of the interaction of the given body with the two others. The acceleration of the horse-waggon system is caused by the resultant of all the forces applied to it. There are four such forces and their resultant is $F-f$. This is what causes the acceleration of the system. Now you see that this acceleration is not associated with the interaction between the horse and the waggon.

\std So the earth's surface turns out to be, not simply the place on which certain events occur, but an active participant of these events.

\tchr Your pictorial comment is quite true. Incidentally, if you locate the horse and waggon on an ideal icy surface, thereby excluding all horizontal interaction between this system and the earth, there will be no motion, whatsoever. It should be stressed that no internal interaction can impart acceleration to a system as a whole. This can be done only by external action (you can't lift yourself by your hair, or bootstraps either). This is an important practical inference of Newton's third law of motion.
\end{dialogue}
%\newpage
\cleardoublepage
\thispagestyle{empty}


\thispagestyle{empty}
\vspace*{-2cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figs/sec/tarasov-qasp-sec-03.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{If you know mechanics well, you can easily solve problems. The converse is just as true: if you solve problems readily, you evidently have a good knowledge of mechanics. Therefore, extend your knowledge of mechanics by solving as many problems as you can.}}}
\end{fullwidth}
%\end{centering}
\cleardoublepage

