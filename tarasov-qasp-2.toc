\contentsline {chapter}{From The Editor Of The Russian Edition}{7}{chapter*.1}%
\contentsline {chapter}{Foreword}{9}{chapter*.2}%
\contentsline {chapter}{\numberline {1}Can You Analyse Graphs Representing The Kinematics Of Straight-Line Motion?}{13}{chapter.1}%
\contentsline {chapter}{\numberline {2}Can You Show The Forces Applied To A Body?}{21}{chapter.2}%
\contentsline {chapter}{\numberline {3}Can You Determine The Friction Force?}{31}{chapter.3}%
\contentsline {chapter}{\numberline {4}How Well Do You Know Newton's Laws Of Motion?}{37}{chapter.4}%
\contentsline {chapter}{\numberline {5}How Do You Go About Solving Problems In Kinematics?}{51}{chapter.5}%
\contentsline {section}{Problems}{59}{equation.5.0.20}%
\contentsline {chapter}{\numberline {6}How Do You Go About Solving Problems In Dynamics?}{61}{chapter.6}%
\contentsline {section}{Problems}{65}{equation.6.0.23}%
\contentsline {chapter}{\numberline {7}Are Problems In Dynamics Much More Difficult To Solve If Friction Is Taken Into Account?}{67}{chapter.7}%
\contentsline {section}{Problems}{73}{equation.7.0.33}%
\contentsline {chapter}{\numberline {8}How Do You Deal With Motion In A Circle?}{77}{chapter.8}%
\contentsline {section}{Problems}{89}{equation.8.0.45}%
\contentsline {chapter}{\numberline {9}How Do You Explain The Weightlessness Of Bodies?}{91}{chapter.9}%
\contentsline {section}{Problems}{95}{equation.9.0.47}%
\contentsline {chapter}{\numberline {10}Can You Apply The Laws Of Conservation Of Energy And Linear Momentum?}{99}{chapter.10}%
\contentsline {section}{Problems}{115}{equation.10.0.64}%
\contentsline {chapter}{\numberline {11}Can You Deal With Harmonic Vibrations?}{119}{chapter.11}%
\contentsline {section}{Problems}{126}{equation.11.0.76}%
\contentsline {chapter}{\numberline {12}What Happens To A Pendulum In A State Of Weightlessness?}{127}{chapter.12}%
\contentsline {chapter}{\numberline {13}Can You Use The Force Resolution Method Efficiently?}{135}{chapter.13}%
\contentsline {section}{Problems}{138}{section*.3}%
\contentsline {chapter}{\numberline {14}What Do You Know About The Equilibrium Of Bodies?}{141}{chapter.14}%
\contentsline {chapter}{\numberline {15}How Do You Locate The Centre Of Gravity? }{149}{chapter.15}%
\contentsline {section}{Problems}{155}{equation.15.0.89}%
\contentsline {chapter}{\numberline {16}Do you know Archimedes' principle?}{159}{chapter.16}%
\contentsline {section}{Problems}{165}{equation.16.0.95}%
\contentsline {chapter}{\numberline {17}Is Archimedes' Principle Valid In A Spaceship?}{167}{chapter.17}%
\contentsline {chapter}{\numberline {18}What Do You Know About The Molecular-Kinetic Theory Of Matter?}{173}{chapter.18}%
\contentsline {chapter}{\numberline {19}How Do You Account For The Peculiarity In The Thermal Expansion Of Water?}{187}{chapter.19}%
\contentsline {chapter}{\numberline {20}How Well Do You Know The Gas Laws?}{189}{chapter.20}%
\contentsline {chapter}{\numberline {21}How Do You Go About Solving Problems On Gas Laws?}{203}{chapter.21}%
\contentsline {section}{Problems}{211}{equation.21.0.126}%
\contentsline {chapter}{\numberline {22}Let Us Discuss Field Theory}{215}{chapter.22}%
\contentsline {chapter}{\numberline {23}How Is An Electrostatic Field Described?}{221}{chapter.23}%
\contentsline {paragraph}{}{221}{paragraph*.4}%
\contentsline {chapter}{\numberline {24}How Do Lines Of Force Behave Near The Surface Of A Conductor?}{231}{chapter.24}%
\contentsline {chapter}{\numberline {25}How Do You Deal With Motion In A Uniform Electrostatic Field?}{237}{chapter.25}%
\contentsline {section}{Problems}{247}{equation.25.0.156}%
\contentsline {chapter}{\numberline {26}Can You Apply Coulomb's Law?}{249}{chapter.26}%
\contentsline {section}{Problems}{256}{equation.26.0.163}%
\contentsline {chapter}{\numberline {27}Do You Know Ohm's Law?}{259}{chapter.27}%
\contentsline {section}{Problems}{267}{equation.27.0.174}%
\contentsline {chapter}{\numberline {28}Can A Capacitor Be Connected Into A Direct-Current Circuit?}{269}{chapter.28}%
\contentsline {section}{Problems}{271}{equation.28.0.178}%
\contentsline {chapter}{\numberline {29}Can you compute the resistance of a branched portion of a circuit?}{275}{chapter.29}%
\contentsline {section}{Problems}{280}{equation.29.0.179}%
\contentsline {chapter}{\numberline {30}Why Did The Electric Bulb Burn Out?}{283}{chapter.30}%
\contentsline {section}{Problems}{288}{equation.30.0.188}%
\contentsline {chapter}{\numberline {31}Do You Know How Light Beams Are Reflected And Refracted?}{293}{chapter.31}%
\contentsline {section}{Problems}{299}{equation.31.0.194}%
\contentsline {chapter}{\numberline {32}How Do You Construct Images Formed By Mirrors And Lenses?}{301}{chapter.32}%
\contentsline {chapter}{\numberline {33}How Well Do You Solve Problems Involving Mirrors And Lenses?}{313}{chapter.33}%
\contentsline {section}{Problems}{318}{equation.33.0.204}%
\contentsline {chapter}{Answers}{321}{section*.6}%
\contentsfinish 
