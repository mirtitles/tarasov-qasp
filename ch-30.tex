% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{Why Did The Electric Bulb Burn Out?}
\label{ch-30}

\begin{dialogue}

\stda Why does an electric bulb burn out? From excessive voltage or from excessive current?

\tchr How would you answer this question?

\stda I think it is due to the high current.

\tchr I don't much like your answer. Let me note, first, that the question, as you put it, should be classified in the category of provocative or tricky questions. An electric bulb burns out as a result of the evolution of an excessively large amount of heat in unit time, i. e. from a sharp increase in the heating effect of the current. This, in turn, may be due to a change in any of various factors: the voltage applied to the bulb, the current through the bulb and the resistance of the bulb. In this connection, let us recall all the formulas you know for finding the power developed or expended when an electric current passes through a certain resistance $R$.

\stdb I know the following formulas:
\begin{align}% 
P & = (\varphi_{1} -\varphi_{2})I \label{eq-180}\\
P & =I^{2} R \label{eq-181}\\
P & = \frac{ \left( \varphi_{1} -\varphi_{2} \right)^{2}}{R} \label{eq-182}
\end{align}
where $P$ is the power developed in the resistance $R$, $(\varphi_{1} -\varphi_{2})$ is the potential difference across the resistance $R$ and $I$ is the current flowing through the resistance $R$.

\stda We usually used only formula \eqref{eq-181}, which expresses the power in terms of the square of the current and the resistance.

\tchr It is quite evident that the three formulas are equivalent since one can be transformed into the others by applying Ohm's law. It is precisely the equivalence of the formulas that indicates that in solving our problem we should not deal with the current or voltage separately. We should take all three quantities - the current, voltage and resistance - into account together. (To \smallcaps{Student~A}): By the way, why do you prefer formula \eqref{eq-181}?

\stda As a rule, the voltage supplied to a bulb is constant. Therefore, the dependence of the power on the voltage is of no particular interest. Formula \eqref{eq-181} is the most useful of the three.

\tchr You are wrong in assigning a privileged position to formula \eqref{eq-181}. Consider the following problem. \hlred{The burner of an electric table stove is made up of three sections of equal resistance. If the three sections are connected in parallel, water in a tea-kettle begins to boil in 6 minutes. When will the same mass of water in the tea-kettle begin to boil if the sections are connected as shown in \figr{fig-123}?}

\begin{figure}%[-2cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-30/fig-123a}
\caption{Time taken by the kettle to boil water in different configurations.}
\label{fig-123}
\end{figure}

\stda First of all we find \figr{fig-123} the total resistance of the burner for each kind of connection, denoting the resistance of one section by $R$. In the initial case (connection in parallel), the total resistance $R_{0}=R/3$. For cases $a$, $b$ and $c$ (\figr{fig-123} we obtain
\begin{equation}%
\left. \begin{aligned}
R_{a} & = 3R \\
R_{b} & = R+ \frac{R}{2}=\frac{3}{2} R \\
R_{c} & =\frac{2R^{2}}{3R} = \frac{2}{3} R
\label{eq-183}
\end{aligned}
\right\}
\end{equation}
Next, if we denote the voltage applied to the electric table stove by $U$, then, using Ohm's law we can find the total current flowing through the burner in each case\ldots{}.

\tchr (interrupting) You don't need to find the current. Let us denote by $t_{0}, \,t_{a},\, t_{b}\,\, \text{and} \,\, t_{c}$ the times required to heat the water in the teakettle to the boiling point in each  case. The evolved heat is equal to the power multiplied by the heating time. In each case, the same amount of heat is generated. Using formula \eqref{eq-182} to determine the power, we obtain
\begin{equation}%
\frac{U^{2}t_{0}}{R_{0}} =  \frac{U^{2}t_{a}}{R_{a}} =\frac{U^{2} t_{b}}{R_{b}} = \frac{U^{2}t_{c}}{R_{c}}
\label{eq-184}
\end{equation}
Substituting equations \eqref{eq-183} into \eqref{eq-184} and cancelling the common factors ($U^{2}$ and $I/R$), we obtain
\begin{equation*}%
3t_{0} = \frac{t_{a}}{3} = \frac{2t_{b}}{3} = \frac{3t_{c}}{2}
\end{equation*}
from which we readily find the required values: 
\begin{align*}%
t_{a} & = 9 t_{0}= \SI{54}{\minute}\\
t_{b} & =\frac{9t_{0}}{2}= \SI{27}{\minute} \\
t_{c} & =2t_{0}  = \SI{12}{\minute}
\end{align*}
Note that in the given problem it was more convenient to apply formula \eqref{eq-182} to find the power, exactly because the voltage applied to the electric table stove is a constant value. 

But consider the following question. Given: \hlred{a current source with an emf $\Ea$ and internal resistance $r$; the source is connected to a certain external resistance $R$. What is the efficiency of the source?}

\stdb The efficiency of a current source is the ratio of the useful power, i.e. the power expended on the external resistance, to the total power, i.e. to the sum of the powers expended on the internal and external resistances:
\begin{equation}%
\eta = \frac{I^{2}R}{I^{2}(R+r)} = \frac{R}{(R+r)}
\label{eq-185}
\end{equation}
\tchr Correct. \hlred{Assume that the internal resistance of the current source remains unchanged and only the external resistance varies. How will the efficiency of the current source vary in this case?}

\stdb At $R=0$ (in the case of a short circuit), $\eta=0$. At $R=r, \,\, \eta=0.5$. As $R$ increases infinitely, the efficiency approaches unity.

\tchr Absolutely correct. \hlred{And how in this case will the useful power vary (the power expended on the external resistance)?}

\stdb  Since the efficiency of the source increases with $R$, it follows that the useful power will also increase. In short, the larger the $R$, the higher the useful power will be.

\tchr You are wrong. An increase in the efficiency of the current source means that there is an increase in the ratio of the useful power to the total power of the source. The useful power may even be reduced. In fact, the useful power is
\begin{equation}%
P_{u} = \frac{\Ea^{2}}{(R+r)^{2}} \,R= \frac{\Ea^{2}}{r}  \frac{x}{(x+1)^{2}} 
\label{eq-186}
\end{equation}
where $x=R/r$. If $ x\ll 1 \,\, \text{then} \,\, P_{u}  \propto x $. If $ x\gg 1 \,\, \text{then} \,\, P_{u} \propto \frac{1}{x} $. The useful power $P_{u}$ reaches its maximum value at $x= 1$ (i.e. $R=r$), when $P_{u} = \Ea^{2}/4r$. A curve of the function $y=x/(x+ 1)^{2}$ is given in \figr{fig-124}. It illustrates the variation in the useful power with an increase in the external resistance. 
\begin{figure}%[-2cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-30/fig-124a}
\caption{Graph of the function $y=x/(x+ 1)^{2}$ which shows variation in the useful power with an increase in the external resistance.}
\label{fig-124}
\end{figure}

Consider the following problem. \hlred{Two hundred identical electric bulbs with a resistance of \SI{300}{\ohm} each are connected in parallel to a current source with an emf of \SI{100}{\volt} and internal resistance of \SI{0.5}{\ohm}. Compute the power expended on each bulb and the relative change in the power expended on each bulb if one of the two hundred bulbs burns out. Neglect the resistance of the leads \drkgry{(\figr{fig-125})}.}

\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-30/fig-125a}
\caption{Compute the power expended on each bulb and the relative change in the power expended on each bulb if one of the two hundred bulbs burns out.}
\label{fig-125}
\end{marginfigure}

\stdb The total current in the external circuit equals 
\begin{equation*}%
I_{t} = \frac{\Ea}{r + \dfrac{R}{n}} = \SI{50}{\ampere}
\end{equation*}
The current passing through each bulb is $I_{t}/n=\SI{0.25}{\ampere}$. Next we can find the power expended on each bulb: $P=I^{2}R=\SI{37.5}{\watt}$. To determine the relative change In the power per bulb if one of the two hundred burns out, I shall first find the power $P_{1}$ per bulb for $n= 199$, and then compute the ratio
\begin{equation}%
f = \frac{P_{1}- P}{P}
\label{eq-187}
\end{equation}
\tchr I do not approve of this method for finding the required ratio $f$. It should be expressed in the general form in terms of the resistances $R$ and and the number of bulbs $n$. Thus
\begin{align*}%
P & = \frac{R}{n^{2}} \frac{\Ea^{2}}{\left( r + \frac{R}{n} \right)^{2}} \\
P_{1} & = \frac{R}{(n - 1)^{2}} \frac{\Ea^{2}}{\left( r + \frac{R}{n - 1} \right)^{2}}
\end{align*}
Substituting these equations into \eqref{eq-187}, we obtain
\begin{align*}%
f & = \left( \frac{P_{1}}{P} - 1\right) = \frac{nr+R}{nr -r +R} - 1\\
& = \frac{1}{1 - \dfrac{r}{nr+R}} - 1 
\end{align*}
The fraction in the denominator of the last equation is much less than unity (because there are many bulbs in the circuit and the resistance of each one is much higher than the internal resistance of the current source). Therefore, we can apply the approximation formula \eqref{eq-173}:
\begin{equation}
f = \left( 1 - \frac{r}{nr+R} \right)^{2} - 1 \approx \frac{2r}{nr+R}
\label{eq-188}
\end{equation}
Alter substituting the numerical values into equation \eqref{eq-188} we find that $f=0.0025$.

\stdb But why do you object to computing $P_{1}$ first and then finding $f$ by substituting the numerical values into equation \eqref{eq-187}?

\tchr You see that $f=0.0025$. This means that if your (numerical) method was used to obtain this result, we would have to compute the value of $P_{1}$ with an accuracy to the fourth decimal place. You cannot even know beforehand to what accuracy you should compute $P_{1}$. If in our case you computed $P_{1}$ to an accuracy of two decimal places, you would come to the conclusion that power $P_{1}$ coincides with power $P$.

\end{dialogue}

\section*{Problems}
\label{prob-30}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume=problems]
\item In the electric circuit shown in \figr{fig-126}, $\Ea= \SI{100}{\volt},  \,\, r=\SI{36}{\ohm}$ and the efficiency of the current source equals 50\%. Compute the resistance $R$ and the useful power.
\begin{marginfigure}[-2cm]
\centering
\includegraphics[width=.8\textwidth]{figs/ch-30/fig-126a}
\caption{ Compute the resistance $R$ and the useful power..}
\label{fig-126}
\end{marginfigure}

\item A current source is connected to a resistor whose resistance is four times the internal resistance of the current source. How will the efficiency of the source change (in per cent) if an additional-resistor with a resistance twice the internal resistance is connected in parallel to the external resistance?

\item Several identical resistances $R$ are connected together in an arrangement shown in \figr{fig-127}. In one case, this arrangement is connected to the current source at points 1 and 2, and in another, at points 1 and 3. Compute the internal resistance of the current source if the ratio of the efficiencies of the source in the first and second cases equals 16/15. Find the values of these efficiencies.
\begin{marginfigure}[-1cm]
\centering
\includegraphics[width=\textwidth]{figs/ch-30/fig-127a}
\caption{ Compute the internal resistance of the current source and find the efficiencies.}
\label{fig-127}
\end{marginfigure}
\item The resistances in a burner of an electric table stove are connected together in an arrangement shown in \figr{fig-127}. This arrangement is connected to the supply mains at points 1 and 2, and, after a certain time, 500 grams of water are heated to the boiling point. How much water can be heated to the boiling point in the same time interval when the arrangement of resistances is connected to the supply mains at points 1 and 3? The initial temperature of the water is the same in both cases. Neglect all heat losses.

\item One and a half litres of water at a temperature of \SI{20}{\degreeCelsius} is heated for \SI{15}{\minute} on an electric table stove burner having two sections with the same resistance. When the sections are connected in parallel, the water begins to boil and \SI{100}{\gram} of it is converted into steam. What will happen to the water if the sections are connected in series and the water is heated for \SI{60}{\minute}? The latent heat of vaporization is \SI[per-mode=symbol]{539}{\calorie \per \gram}. How much time will be required to heat this amount of water to the boiling point if only one section is switched on?


\end{enumerate}


\cleardoublepage

\thispagestyle{empty}
\vspace*{-1.95cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.75\textwidth]{figs/sec/tarasov-qasp-sec-12.pdf}
\end{figure*}
\vspace*{-.4cm}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{The laws of geometrical optics have been known to mankind for many centuries. Nevertheless, their elegance and completeness still astonish us. Find this out for yourself by doing exercises on the construction of images  formed in various optical systems. We shall discuss the laws of reflection and refraction of light.
}}}
\end{fullwidth}



\cleardoublepage
