% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{Do you know Archimedes' principle?}
\label{ch-16}

\begin{dialogue}

\tchr Do you know Archimedes' principle?

\stda Yes, of course. The buoyant force exerted by a liquid on a body immersed in it is
exactly equal to the weight of the liquid displaced by the body.

\tchr Correct. Only it should be extended to include gases: a buoyant force is also exerted by a gas on a body ``immersed'' in it. And now can you give a theoretical proof of your statement?

\stda A proof of Archimedes' principle?

\tchr Yes.

\stda But Archimedes' principle was discovered directly as the result of experiment.

\tchr Quite true. It can, however, be derived from simple energy considerations. Imagine that you raise a body of volume $V$ and density $\rho$ to a height $H$, first in a vacuum and then in a liquid with a density $\rho_{0}$. The energy required in the first case equals $\rho g V H $. The energy required in the second case is less because the raising of a body of volume $V$ by a height $H$ is accompanied by the lowering of a volume $V$ of $\rho$ the liquid by the same height $H$. Therefore, the energy expended in the second case equals 
\begin{equation*}%
\rho g V H - \rho_{0} g V H
\end{equation*}
Regarding the subtrahend $\rho_{0} g V H$ as the work done by a certain force, we can conclude that, compared with a vacuum, in a liquid an additional force $F=\rho_{0} g V H$ acts on the body making it easier to raise. This force is called the buoyant force. Quite obviously, it is exactly equal to the weight of the liquid in the volume $V$ of the body immersed in the liquid. (Note that we neglect the energy losses associated with friction upon real displacements of the body in the liquid.)

\begin{figure}%[-3cm]	%
\centering
\begin{tikzpicture} [>=latex,scale=1]
\draw [thick](0,0) ellipse (2cm and 0.5cm);
\path [fill=white,](-2,0) rectangle (2,1);
\draw [fill=white,thick](0,1) ellipse (2cm and 0.5cm);
\draw [thick](-2,0) -- (-2,1);
\draw [thick](2,0) -- (2,1);
\draw [thick](1.9,-.1) -- (1.9,0.8);
\draw [thick](1.8,-.1) -- (1.8,0.7);
\draw (1.7,-.1) -- (1.7,0.6);
\draw [thick](-1.9,-.1) -- (-1.9,0.8);
\draw [thick](-1.8,-.1) -- (-1.8,0.7);
\draw (-1.7,-.1) -- (-1.7,0.6);
\draw [thick, dashed](-2,1) -- (-6,1);
\draw [thick, dashed](-2,0) -- (-6,0);
\draw [thick](2,0) -- (3.5,0);
\draw [thick](2,1) -- (3.5,1);
\draw [thick,latex-latex](3,1) -- (3,0);
\node [left] at (3,0.5) {$h$};
\node [left] at (0.3,1) {$S$};
\draw [thick,latex-](-3,0) -- (-3,-1);
\draw [thick,latex-](-4,0) -- (-4,-1);
\draw [thick,latex-](-5,0) -- (-5,-1);
\draw [thick,latex-](-3,1) -- (-3,2);
\draw [thick,latex-](-4,1) -- (-4,2);
\draw [thick,latex-](-5,1) -- (-5,2);
\node at (-4,2.3) {$P$};
\node at (-4,-1.3) {$P+\rho_{0}gh$};
\end{tikzpicture}
%\includegraphics[width=0.8\linewidth]{fig-066a.pdf}
\caption{Buoyant force for Archimedes' principle.}
\label{fig-67}
\end{figure}
Archimedes' principle can be deduced in a somewhat different way. Assume that the body immersed in the liquid has the form of a cylinder of height $h$ and that the area of its base is $S$ (\figr{fig-67}). Assume that the pressure on the upper base is $p$.

Then the pressure on the lower base will equal $p+\rho gh$. Thus, the difference in pressure on the upper and lower bases equals $\rho_{0}gh$. If we multiply this difference by the area $S$ of the base, we obtain the force $F=\rho ghS$ which tends to push the body upward. Since $hS=V$, the volume of the cylinder, it can readily be seen that this is the buoyant force which appears in Archimedes' principle.

\stda Yes, now I see that Archimedes' principle can be arrived at by purely logical reasoning.

\tchr Before proceeding any further, let us recall the condition for the floating of a body.

\stda I remember that condition. The weight of the body should be counterbalanced by the buoyant force acting on the body in accordance with Archimedes' principle.

\tchr Quite correct. Here is an example for you. \hlred{A piece of ice floats in a vessel with water. Will the water level change when the ice melts?}

\stda The level will remain unchanged because the weight of the ice is counterbalanced by the buoyant force and is therefore equal to the weight of the water displaced by the ice. When the ice melts it converts into water whose volume is equal to that of the water that was displaced previously.

\tchr Exactly. And now let us assume that there is, for instance, a piece of lead inside the ice. What will happen to the water level after the ice melts in this case?

\stda I'm not quite sure, but I think the water level should reduce slightly. I cannot, however, prove this.

\tchr Let us denote the volume of the piece of ice together with the lead by $V$, the volume of the piece of lead by $v$, the volume of the water displaced by the submerged part of the ice by $V_{1}$ the density of the water by $\rho_{0}$, the density of the ice by $\rho_{1}$ and the density of the lead by $\rho_{2}$. The piece of ice together with the lead has a weight equal to
\begin{equation*}%
\rho_{1} g (V-v) + \rho_{2}gv
\end{equation*}
This weight is counterbalanced by the buoyant force $\rho_{0}gV t$. Thus
\begin{equation}%
\rho_{1} g (V-v) + \rho_{2}g v = \rho_{0}g V t
\label{eq-90}
\end{equation}
After melting, the ice turns into water whose volume $V_{2}$ is found from the equation
\begin{equation*}
\rho_{1} g (V-v) = \rho_{0} gV_{2}
\end{equation*}
Substituting this equation into \eqref{eq-90} we obtain
\begin{equation*}%
\rho_{0}gV_{2} + \rho_{2}gv = \rho_{0}gV_{1}
\end{equation*}
From which we find that the volume of water obtained as a result of the melting of the ice is
\begin{equation}%
V_{2} = V_{1} -v \frac{\rho_{2}}{\rho_{1}}
\label{eq-91}
\end{equation}
Thus, before the ice melted, the volume of water displaced was $V_{1}$. Then the lead and the water from the melted ice began to occupy the volume $(V_{2}+v)$. To answer the question concerning the water level in the vessel, these volumes should be compared. From equation \eqref{eq-91} we get
\begin{equation}%
V_{2} + v= V_{1} - v \frac{\rho_{2}-\rho_{0}}{\rho_{0}}
\label{eq-92}
\end{equation}
Since $\rho_{2} > \rho_{0}$ (lead is heavier than water), it can be seen from equation \eqref{eq-92} that $(V_{2}+v)<V_{1}$. Consequently, the water level will reduce as a result of the melting of the ice. Dividing the difference in the volumes $V_{1} - (V_{2}+v)$ by the cross-sectional area $S$ of the vessel (assuming, for the sake of simplicity, that it is of cylindrical shape) we can find the height $h$ by which the level drops after the ice melts. Thus
\begin{equation}%
h= v \left( \frac{\rho_{2}-\rho_{0}}{\rho_{0}S} \right)
\label{eq-93}
\end{equation}
Do you understand the solution of this problem?

\stda Yes, I'm quite sure I do.

\tchr Then, instead of the piece of lead, let us put a piece of cork of volume $v$ and density $\rho_{3}$ inside the ice. What will happen to the water level when the ice melts?

\stda I think it will rise slightly.

\tchr Why?

\stda In the example with lead the level fell. Lead is heavier than water, and cork is lighter than water. Consequently, in the case of cork we should expect the opposite effect: the water level should rise.

\tchr You are mistaken. Your answer would be correct if the cork remained submerged after the ice melted. Since the cork is lighter than water it will surely rise to the surface and float. Therefore, the example with cork (or any other body lighter than water) requires special consideration. Using the result of equation \eqref{eq-91}, we can find the difference between the volume of the water displaced by the piece of ice together with the cork, and that of the water obtained by the melting of the ice. Thus
\begin{equation}%
V_{1} - V_{2} = v \left( \frac{\rho_{3}}{\rho_{0}} \right)
\label{eq-94}
\end{equation}
Next we apply the condition for the floating of the piece of cork:
\begin{equation}%
\rho_{3}v  = \rho_{0}v_{1}
\label{eq-95}
\end{equation}
where $v_{1}$ is the volume of the part of the cork submerged in water. Substituting this equation into \eqref{eq-94}, we obtain
\begin{equation*}%
V_{1} = V_{2} +v_{1}
\end{equation*}
Thus \hlblue{the volume of water displaced by the piece of ice is exactly equal to the sum of the volume of water obtained from the melted ice. and the volume displaced by the submerged portion of the floating piece of cork. So in this case the water level remains unchanged.}

\stda And if the piece of ice contained simply a bubble of air instead of the piece of cork?

\tchr After the ice melts, this bubble will be released. It can readily be seen that the water level in the vessel will be exactly the same as it was before the ice melted. In short, the example with the bubble of air in the ice is similar to that with the piece of cork.

\stda I see that quite interesting question and problems can be devised on the basis of Archimedes' principle.

\tchr Unfortunately, some examinees don't give enough attention to this principle when preparing for their physics examinations. Let us consider the following example. \hlred{One pan of a balance
carries a vessel with water and the other, a stand with a weight suspended from it. The pans are balanced (\figr{fig-68}~\drkgry{(a)}). Then the stand is turned so that the suspended weight is completely submerged in the water. Obviously, the state of equilibrium is disturbed since the pan with the stand becomes lighter (\figr{fig-68}~\drkgry{(b)}). What additional weight must be put on the pan with the stand to restore equilibrium?}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth,angle=-2]{figs/ch-16/fig-068a}
\caption{What additional weight must be put on the pan with the stand to restore equilibrium?}
\label{fig-68}
\end{figure}

\stda The submerged weight is subject to a buoyant force equal to the weight of the water of the volume displaced by the submerged weight (we denote this weight of water by $P$). Consequently, to restore equilibrium, a weight $P$ should be placed on the pan with the stand.

\tchr You are mistaken. You would do well to recall Newton's third law of motion. According to this law, the force with which the water in the vessel acts on the submerged weight is exactly equal to the force with which the submerged weight acts on the water in the opposite direction. Consequently, as the weight of the pan with the stand reduces, the weight of the pan with the vessel increases. Therefore, to restore equilibrium, a weight equal to $2P$ should be added to the pan with the stand.

\stda I can't quite understand your reasoning. After all, the interaction of the submerged weight and the water in no way resembles the interaction of two bodies in mechanics.

\tchr The field of application of Newton's third law is not limited to mechanics. The expression ``to every action there is an equal and opposite reaction'' refers to a great many kinds
of interaction. We can, however, apply a different line of reasoning in our case, one to which you will surely have no objections. Let us deal with the stand with the weight and the vessel with the water as part of a single system whose total weight is obviously the sum of the weight of the left pan and that of the right pan. The total weight of the system should not change due to interaction of its parts with one another. Hence, if as the result of interaction the weight of the right pan is decreased by $P$, the weight of the left pan must be increased by the same amount ($P$). Therefore, after the weight is submerged in the vessel with water, the difference between the weights of the left and right pans should be $2P$.

\end{dialogue}

\section*{Problems}
\label{prob-16}
\addcontentsline{toc}{section}{Problems}


\begin{enumerate}[resume=problems]
\item A vessel of cylindrical shape with a cross-sectional area $S$ is filled with water in which a piece of ice, containing a lead ball, floats. The volume of the ice together with the lead ball is $V$ and 1/20 of this volume is above the water level. To what mark will the water level in the vessel reduce after the ice melts? The densities of water, ice and lead are assumed to be known.


\end{enumerate}


\cleardoublepage
