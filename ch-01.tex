% !TEX root = tarasov-qasp-2.tex
\cleardoublepage
\thispagestyle{empty}
%\vspace*{2cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.73\textwidth]{figs/sec/tarasov-qasp-sec-01.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{Do not neglect kinematics! The question of how a body travels in space and time is of considerable interest, both from physical and practical points of view.}}}
\end{fullwidth}
%\end{centering}
\cleardoublepage


\chapter[Can You Analyse Graphs Representing The Kinematics Of Straight-Line Motion?]{Can You Analyse Graphs Representing \\The Kinematics Of Straight-Line Motion?}
%\label{analysing-graphs}
\label{ch-01}

%{\noindent 


\begin{dialogue}

\tchr You have seen graphs showing the dependence of the velocity and distance travelled by a body on the time of travel for straight-line, uniformly variable motion. In this connection,  I wish to put the following question. \hlred{Consider a velocity graph of the kind shown in \figr{fig-01}. On its basis, draw a graph showing the dependence of the distance travelled on time.}

\std But I have never drawn such graphs.

\tchr There should be no difficulties. However, let us reason this out together. First we will divide the whole interval of time into three periods: 1, 2 and 3 (see \figr{fig-01}). How does the body travel in period 1? What is the formula for the distance travelled in this period?

\begin{figure}[!h]%
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-01/fig-001.pdf}
\caption{On the basis of this graph can you draw a graph showing the dependence of distance travelled on time?}
\label{fig-01}
\end{figure}

\std In period 1, the body has uniformly accelerated motion with no initial velocity. The formula for the distance travelled is of the form
\begin{equation}%
s(t) = \frac{at^2}{2}
\label{eq-001}
\end{equation}
where $a$ is the acceleration of the body.

\tchr Using the velocity graph, can you find the acceleration?

\std Yes. The acceleration is the change in velocity in unit time. It equals the ratio of length $\overline{AC}$ to length $\overline{OC}$.

\tchr Good. Now consider periods 2 and 3.
\std In period 2 the body travels with uniform velocity $v$ acquired at the end of period 1. The formula for the distance travelled is $s=vt$.

\tchr Just a minute, your answer is inaccurate. You have forgotten that the uniform motion began, not at the initial instant of time, but at the instant $t_{1}$. Up to that time, the body had already travelled a distance equal to $\dfrac{at_{1}^{2}}{2}$. The
dependence of the distance travelled on the elapsed time for
period 2 is expressed by the equation
\begin{equation}%
s(t) = \frac{at_{1}^{2}}{2}+ v (t-t_{1})
\label{eq-002}
\end{equation}
With this in mind, please write the formula for the distance travelled in period 3.

\std The motion of the body in period 3 is uniformly decelerated. If I understand it correctly, the formula of the distance travelled in this period should be
$$
s(t) = \frac{at_{1}^{2}}{2}+ v (t_{2}-t_{1}) +  v(t-t_{2}) - \frac{a_{1}(t-t_{2})^2}{2}
$$
where $a_{1}$ is the acceleration in period 3. It is only one half of the acceleration a in period 1, because period 3 is twice as long as period 1.

\tchr Your equation can be simplified somewhat:
\begin{equation}%
s(t) = \frac{at_{1}^{2}}{2}+ v (t-t_{1}) - \frac{a_{1}(t-t_{2})^2}{2}
\label{eq-003}
\end{equation}
Now, it remains to summarize the results of equations \eqref{eq-001}, \eqref{eq-002} and \eqref{eq-003}.

\std I understand. The graph of the distance travelled
has the form of a parabola for period 1, a straight line for period 2 and another parabola
(but turned over, with the convexity facing upward) for period 3. Here is the graph I have drawn (\figr{fig-02}).
\begin{figure}%
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-01/fig-002.pdf}
\caption{Students' incorrect graph showing distance covered $s$ as a function of time $t$.}
\label{fig-02}
\end{figure}

\tchr There are two faults in your drawing: \hlblue{the graph of the distance travelled
should have no kinks}. It should be a smooth curve, i.e. the parabolas should be tangent to the straight line. Moreover, \hlblue{the vertex of the upper (inverted) parabola should correspond to the instant of time $t_{3}$.} Here is a correct drawing of the graph  (\figr{fig-03}).

\begin{figure}[!ht]%
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-01/fig-003a.pdf}
\caption{Corrected graph showing distance covered $s$ as a function of time $t$.}
\label{fig-03}
\end{figure}
\std Please explain it. \\
\tchr Let us consider a portion of a distance-travelled \emph{vs} time graph (\figr{fig-04}). The average velocity of the body in the interval from $t$ to $t+\Delta t$ equals 
\begin{equation*}%
\frac{s(t+\Delta t) - s(t)}{\Delta t}= \tan \alpha
\end{equation*}
where $\alpha$ is the angle between chord $AB$ and the horizontal. To determine the velocity of the body at the instant $t$ it is necessary to find the limit of such average velocities for $\Delta t \rightarrow 0$. Thus
\begin{equation}%
v(t)=\lim_{\Delta t \rightarrow 0} \frac{s(t+\Delta t)-s(t)}{\Delta t}
\label{eq-004}
\end{equation}

In the limit, the chord becomes a tangent to the distance-travelled \emph{vs} time curve, passing through point $A$ (see the dashed line in \figr{fig-04}). The tangent of the angle this line (tangent to the curve) makes with the horizontal is the value of the velocity at the instant $t$. Thus it is possible to find the velocity at any instant of time from the angle of inclination of the tangent to the distance-travelled \emph{vs} time curve at the corresponding point.
\begin{figure}[!ht]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-01/fig-004a.pdf}
\caption{Corrected graph showing distance covered $s$ as a function of time $t$.}
\label{fig-04}
\end{figure}
But let us return to your drawing (see \figr{fig-02}). It follows from your graph that at the instant of time $t_{1}$ (and at $t_{2}$) the velocity of the body has two different values. If we approach $t$, from the left, the velocity equals $\tan \alpha_{1}$, while if we approach it from the right the velocity equals $\tan \alpha_{2}$. According to your graph, the velocity of the body at the instant $t_{1}$ (and again at $t_{2}$) must have a discontinuity, which actually it has not (the velocity \emph{vs} time graph in \figr{fig-01} is continuous).

\std I understand now. \hlblue{Continuity of the velocity graph leads to smoothness of the distance-travelled \emph{vs} time graph.}

\tchr Incidentally, the vertices of the parabolas should correspond to the instants of time 0 and $t_{3}$ because \hlblue{at these instants the velocity of the body equals zero and the tangent
to the curve must be horizontal for these points}. 
\hlred{Now, using the velocity graph in \figr{fig-01}, find the distance travelled by a body by the instant $t_{2}$.}

\std First we determine the acceleration $a$ in period 1 from the velocity graph and then the velocity $v$ in period 2. Next we make use of formula \eqref{eq-002}. The distance travelled by the body during the time $t_{2}$ equals
\begin{equation*}%
s(t_{2}) = \frac{at_{1}^{2}}{2}+v(t_{2}-t)
\end{equation*}

\tchr Exactly. But there is a simpler way. The distance travelled by the body during the time $t_{2}$ is numerically equal to the area of the figure $OABD$ under the velocity \emph{vs} time graph in the interval $Ot_{2}$. Let us consider another problem to fix what we have learned.

\hlred{Assume that the distance-travelled \emph{vs} time graph has kinks. This graph is shown in \figr{fig-05}, where the curved line is a parabola with its vertex at point $A$. Draw the velocity \emph{vs} timegraph.}

\begin{figure}[!h]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-01/fig-005a.pdf}
\caption{Corrected graph with kinks showing distance travelled $s$ as a function of time $t$.}
\label{fig-05}
\end{figure}

\std Since there are kinks in the distance-travelled graph, there should be discontinuities in the velocity graph at the corresponding instants of time ($t_{1}$ and $t_{2}$). Here is my graph (\figr{fig-06}).

\tchr Good. What is the length of $\overline{BC}$?
\begin{figure}[!h]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-01/fig-006a.pdf}
\caption{Velocity \emph{vs} time graph for the function shown in \figr{fig-05}.}
\label{fig-06}
\end{figure}
\std It is equal to $\tan \alpha_{1}$ (see \figr{fig-05}). We don't, however, know the value of angle $\alpha_{1}$.
\tchr Nevertheless, we should have no difficulty in determining the length of $\overline{BC}$. Take notice that the distance travelled by the body by the time $t_{3}$ is the same as if it had travelled at uniform velocity all the time (the straight line in the interval from $t_{2}$ to $t_{3}$ in \figr{fig-05} is a continuation of the straight line in the interval from 0 to $t_{1}$). Since the distance travelled is measured by the area under the velocity graph, it follows that the area of rectangle $ADEC$ in \figr{fig-06} is equal to the area of triangle $ABC$. Consequently, $BC=2EC$, i.e. the velocity at instant $t_{2}$ when approached from the left is twice the velocity of uniform motion in the intervals from 0 to $t_{1}$ and from $t_{2}$ to $t_{3}$.

\end{dialogue}
