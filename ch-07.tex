% !TEX root = tarasov-qasp-2.tex

\chapter{Are Problems In Dynamics Much More Difficult To Solve If Friction Is Taken Into Account?}
\label{ch-07}

\begin{dialogue}
\tchr Problems may become much more difficult when the friction forces are taken into account.

\std But we have already discussed the force of friction (\chap{ch-03}). If a body is in motion, the friction force is determined from the bearing reaction ($F_{fr}=kN$); if the body is at rest, the friction force is equal to the force that tends to take it out of this state of rest. All this can readily be understood and remembered.

\tchr That is so. However, you overlook one important fact. You assume that you already know the answers to the following questions: 
\begin{enumerate*}[label=(\arabic*)]
\item Is the body moving or is it at rest? 
\item In which direction is the body moving (if at all)? 
\end{enumerate*}

If these items are known beforehand, then the problem is comparatively simple. Otherwise, it may be very complicated from the outset and may even require special investigation.

\std Yes, now I recall that we spoke of this matter in \chap{ch-02} in connection with our discussion concerning the choice of the direction of the friction force.

\tchr Now I want to discuss this question in more detail. It is my firm opinion that the difficulties involved in, solving problems which take the friction force into account are obviously underestimated both by students and by certain authors who think up problems for physics textbooks.

\hlred{Let us consider the example illustrated in \figr{fig-10}. The angle of inclination $\alpha$ of the plane, weight $P$ of the body, force $F$ and the coefficient of friction $k$ are given. For simplicity we shall assume that $k_{0}=k$ (where $k_{0}$ is the coefficient determining the maximum possible force of static friction). It is required to determine the kind of motion of the body and to find the acceleration.}

Let us assume that the body slides upward along the inclined surface. We can resolve the forces as shown in \figr{fig-27}~\drkgry{(b)} and make use of the result obtained for the acceleration in \chap{ch-06}.Thus
\begin{equation}%
a = \frac{g}{P} \left[F \cos \alpha- P \sin \alpha -(P \cos \alpha + F \sin \alpha) \, k \right]
\label{eq-24}
\end{equation}
It follows from equation \eqref{eq-24} that for the body to slide upward along the inclined plane, the following condition must be complied with:
\begin{equation*}%
F \cos \alpha - P \sin \alpha -\left( P \cos \alpha + F \sin \alpha \right) k \geqslant 0
\end{equation*}
This condition can be written in the form
\begin{equation*}%
F \geqslant P \left ( \frac{k \cos \alpha + \sin \alpha}{\cos \alpha -k \sin \alpha} \right)
\end{equation*}
or
\begin{equation}%
F \geqslant P \left ( \frac{k + \tan \alpha}{ 1 -k \tan \alpha} \right)
\label{eq-25}
\end{equation}

We shall also assume that the angle of inclination of the plane is not too large, so that $(1-k \tan \alpha) > 0$ or 
\begin{equation}%
\tan \alpha < \frac{1}{k}
\label{eq-26}
\end{equation}
We shall next assume that the body slides downward along the inclined plane. We again resolve all the forces as in \figr{fig-27}~\drkgry{(b)} but reverse the friction force. As a result we obtain the fall owing expression for the acceleration of the body 
\begin{equation}%
a= \frac{g}{P} \left[P \sin \alpha -F \cos \alpha - \left( P \cos \alpha + F \sin \alpha \right) \, k \right]
\label{eq-27}
\end{equation}
From equation \eqref{eq-27} it follows that for the body to slide downward along the inclined plane, the following condition must be met:
\begin{equation*}%
P \sin \alpha -F \cos \alpha - \left( P \cos \alpha + F \sin \alpha \right) k \geqslant 0
\end{equation*}
This condition we write in the form
\begin{equation*}%
F \leqslant P \left ( \frac{ \sin \alpha - k \cos \alpha}{\cos \alpha + k \sin \alpha} \right)
%\label{eq-25}
\end{equation*}
or
\begin{equation}%
F \leqslant P \left ( \frac{ \tan \alpha -k }{ 1 + k \tan \alpha} \right)
\label{eq-28}
\end{equation}
In this case, we shall assume that the angle of inclination of the plane is not too small, so that $(\tan \alpha- k)>0$, or
\begin{equation}%
\tan \alpha > k
\label{eq-29}
\end{equation}
Combining conditions \eqref{eq-25}, \eqref{eq-26}, \eqref{eq-28} and \eqref{eq-29}, we can come to
the following conclusions:
\begin{enumerate}[label=\textbf{(\arabic*)},leftmargin=1cm]
\item Assume that the condition
\begin{equation*}%
k < \tan \alpha < \frac{1}{k}
\end{equation*}
holds good for an inclined plane. Then:
\begin{enumerate}[label=\textbf{(\alph*)},leftmargin=1cm]
\item if $F>P \left(\dfrac{k+\tan \alpha}{ 1-k \tan \alpha}\right)$, the body slides upward with an acceleration that can be determined by equation \eqref{eq-24};
\item if $F=P \left(\dfrac{k+\tan \alpha}{ 1-k \tan \alpha}\right)$, the body slides upward at uniform velocity or is at rest;
\item if $F<P \left(\dfrac{\tan \alpha - k}{ 1+k \tan \alpha}\right)$, the body slides downward with an acceleration that can be determined by equation \eqref{eq-27};
\item if $F=P \left(\dfrac{\tan \alpha-k}{1+k \tan \alpha}\right)$, the body slides downward with uniform velocity or is at rest;
\item if $P\left( \dfrac{\tan \alpha-k}{1+k \tan \alpha}\right) < F < P \left( \dfrac{k+\tan \alpha}{1-k \tan \alpha} \right)$, the body is at rest.
\end{enumerate}
Note that upon increase in force $F$ from $P \left(\dfrac{\tan \alpha-k}{1 +k \tan \alpha}\right)$ to $P \left( \dfrac{k+\tan \alpha} {1-k \tan \alpha} \right)$, the force of static friction is gradually reduced from $k (P \cos \alpha +F \sin \alpha)$ to zero; then, after its direction is reversed, it increases to the value $k (P \cos \alpha +F \sin \alpha)$. While this goes on the body remains at rest.
\item Now assume that the inclined plane satisfies the condition
\begin{equation*}%
0 < \tan \alpha \leqslant k
\end{equation*}
then:
\begin{enumerate}[label=\textbf{(\alph*)},leftmargin=1cm]
\item if $F>P \left(\dfrac{k+\tan \alpha}{ 1-k \tan \alpha}\right)$, the body slides upward with an acceleration that can be determined by equation \eqref{eq-24};
\item if  $F=P \left(\dfrac{k+\tan \alpha}{ 1-k \tan \alpha}\right)$, the body slides upward at uniform velocity or is at rest;
\item if  $F<P \left(\dfrac{k+\tan \alpha}{ 1-k \tan \alpha}\right)$, the body is at rest; no downward motion of the body along the inclined plane is possible (even if force $F$ vanishes).
\end{enumerate}
\item Finally, let us assume that the inclined plane meets the condition 
\begin{equation*}%
\tan \alpha \geqslant \frac{1}{k}
\end{equation*}
then:
\begin{enumerate}[label=\textbf{(\alph*)},leftmargin=1cm]
\item if $F< P \left(\dfrac{\tan \alpha - k}{ 1 + k \tan \alpha}\right)$, the body slides downward with an acceleration that can be determined by equation \eqref{eq-27};
\item if $F = P \left(\dfrac{\tan \alpha - k}{ 1 + k \tan \alpha}\right)$, the body slides down ward with uniform velocity or is at rest;
\item if $F > P \left(\dfrac{\tan \alpha - k}{ 1 + k \tan \alpha}\right)$, the body is at rest; no upward motion of the body along the inclined plane is possible. On the face of it, this seems incomprehensible because force $F$ can be increased indefinitely! The inclination of the plane is so large, however, that, with an increase in force F, the pressure of the body against the plane will increase at an even faster rate.
\end{enumerate}
\end{enumerate}

\std Nothing of the kind has ever been demonstrated to us in school. 

\tchr That is exactly why I wanted to draw your attention to this matter. Of course, in your entrance examinations you will evidently have to deal with much simpler cases: there will be no friction, or there will be friction but the nature of the motion will be known beforehand (for instance, whether the body is in motion or at rest). However, even if one does not have to swim over deep spots, it is good to know where they are.

\std What will happen if we assume that $k=0$?

\tchr In the absence of friction, everything becomes much simpler at once. For any angle of inclination of the plane, the results will be: 
\begin{enumerate}[label=$\blacktriangleright$,leftmargin=1cm]
\item at $F > P \tan \alpha$, the body slides upward with the acceleration 
\begin{equation}%
a = \frac{g}{P} \left( F \cos \alpha -P \sin \alpha \right)
\label{eq-30}
\end{equation}
\item at $F = P \tan \alpha$, the body slides with uniform velocity (upward or downward) or is at rest;
\item at $F < P \tan \alpha$, the body slides downward with an acceleration
\begin{equation}%
a = \frac{g}{P} \left( P \sin \alpha - F \cos \alpha \right)
\label{eq-31}
\end{equation}
\end{enumerate}
Note that the results of equations \eqref{eq-30} and \eqref{eq-31} coincide with an accuracy to the sign. Therefore, in solving problems, \hlblue{you can safely assume any direction of motion, find $a$ and take notice of the sign of the acceleration. If $a>0$, the body travels in the direction you have assumed; if $a<0$, the body will travel in the opposite direction} (the acceleration will be equal to $|a|$).

Let us consider one more problem. \hlred{Two bodies $P_{1}$ and $P_{2}$ are connected by a string running over a pulley. Body $P_{1}$ is on an inclined plane with the angle of inclination $\alpha$ and coefficient of friction $k$; body $P_{2}$ hangs on the string \drkgry{(\figr{fig-32})}. Find the acceleration of the system.}
\begin{figure}[!ht]
\centering
\includegraphics[width=0.4\linewidth]{figs/ch-07/fig-032a.pdf}
\caption{Two bodies connected via string on an inclined plane with friction. The problem is to find the acceleration of the system.}
\label{fig-32}
\end{figure}

Assume that the system is moving from left to right. Considering the motion of the system as a whole, we can write the following equation for the acceleration:
\begin{equation}%
a = g \left( \frac{P_{2} - P_{1} \sin \alpha - P_{1} k \cos \alpha}{P_{1}+P_{2}}\right)
\label{eq-32}
\end{equation}
Assuming now that the system moves from right to left, we obtain
\begin{equation}%
a = g \left( \frac{P_{1} \sin \alpha - P_{2} - P_{1} k \cos \alpha}{P_{1}+P_{2}}\right)
\label{eq-33}
\end{equation}
We will carry out an investigation for the given $\alpha$, and $k$ values, varying the ratio $p=P_{2} /P_{1}$. From equation \eqref{eq-32} it follows that for motion from left to right, the condition 
\begin{equation*}%
p \leqslant \frac{1}{\sin \alpha + k \cos \alpha}
\end{equation*}
should be met. Equation \eqref{eq-33} implies that for motion from right to left the necessary condition is
\begin{equation*}%
p \geqslant \frac{1}{\sin \alpha - k \cos \alpha}
\end{equation*}
Here an additional condition is required: the angle of inclination should not be too small, i.e. $\tan \alpha >k$. If $\tan \alpha \leqslant k$, then the system will not move from right to left, however large the ratio $p$ may be. 

If $\tan \alpha >k$, the body is at rest provided the following inequality holds true:
\begin{equation*}%
 \frac{1}{\sin \alpha + k \cos \alpha} < p < \frac{1}{\sin \alpha - k \cos \alpha}
\end{equation*}
If, instead, $\tan \alpha \leqslant k$, then the body is at rest at
\begin{equation*}%
p > \frac{1}{\sin \alpha + k \cos \alpha}
\end{equation*}

\std And what will happen if we change the angle $\alpha$ or the coefficient $k$?

\tchr I leave investigation from this point of view to you as a home assignment (see Problems Nos. 13 and 14).

\end{dialogue}
\section*{Problems}
\label{prob-07}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume*=problems]
\item Investigate the problem illustrated in \figr{fig-32} assuming that the
angle $\alpha$ of inclination of the plane and the ratio $p=P_{2} /P_{1}$ are given, and
assigning various values to the coefficient $k$.
\item Investigate the problem illustrated in \figr{fig-32}, assuming that the
coefficient of friction $k$ and the ratio $p=P_{2} /P_{1}$ are given and assigning
various values to the angle $\alpha$ of inclination of the plane. For the sake of
simplicity, use only two values of the ratio: $p= 1$ (the bodies are of equal
weight) and $p= 1/2$ (the body on the inclined plane is twice as heavy as
the one suspended on the string).
\end{enumerate}
\cleardoublepage

\thispagestyle{empty}
\vspace*{-2cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.68\textwidth]{figs/sec/tarasov-qasp-sec-04.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{Motion in a circle is the simplest form of curvilinear motion. The more important it is to comprehend the specific features of such motion. You can see that the whole universe is made up of curvilinear motion. Let us consider uniform and non-uniform motion of a material point in a circle, and the motion of orbiting satellites. This will lead us to a discussion of the physical causes of the weightlessness of bodies.
}}}
\end{fullwidth}
