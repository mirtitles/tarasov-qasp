% !TEX root = tarasov-qasp-2.tex

\chapter{How Do You Explain The Weightlessness Of Bodies?}
\label{ch-09}

\begin{dialogue}

\tchr How do you understand the expression: \hlred{At the equator of a planet, a body weightless than at the poles?}

\stdb I understand it as follows. The attraction of a body by the earth is less at the
equator than at the poles for two reasons. In the first place, the earth is somewhat flattened
at the poles and therefore the distance from the centre of the earth is somewhat less to the poles than
to the equator. In the second place, the earth rotates about its axis as a result of which the force
of attraction at the equator is weakened due to the centrifugal effect.

\stda Please make your last remark a little clearer.

\stdb You must subtract the centrifugal force from the force of attraction.

\stda I don't agree with you. Firstly, \hlblue{the centrifugal force is not applied to a body travelling in a circle.} We already discussed that in the preceding section (\chap{ch-08}). Secondly, even if such a force existed it could not prevent the force of attraction from being exactly the same as if there was no rotation of the earth. The force of attraction equals $GmM/r^{2}$ and, as such, does not change just because other forces may act on the body.

\tchr As you can see, the question of the ``weightness of bodies'' is not as simple as it seems at first glance. That's why it is among the questions that examinees quite frequently fail to answer correctly. As a matter of fact, if we agree on the definition that the ``weight of a body'' is the force with which the body is attracted by the earth, i.e. the force $GmM/r^{2}$, then the reduction of weight at the equator should be associated only with the flattening at the poles (or bulging
at the equator).

\stdb But you cannot disregard rotation of the earth!

\tchr I fully agree with you. But first I wish to point out that usually, \hlblue{in everyday life, the ``weight of a body'' is understood to be, not the force with which it is attracted to the earth, and this is quite logical, but the force measured by a spring balance, i.e. the force with which the body bears against the earth.} In other words, the bearing reaction is measured (the force with which a body bears against a support is equal to the bearing reaction according to Newton's third
law). It follows that the expression ``a body weighs less at the equator than at the poles'' means that at the equator it bears against its support with a lesser force than at the poles.

Let us denote the force of attraction at the poles by $P_{1}$  and at the equator by $P_{2}$, the bearing reaction at the poles by $N_{1}$ and at the equator by $P_{2}$. At the poles the body is at rest, and at the equator it travels in a circle. Thus
\begin{equation*}%
\begin{split}
P_{1} - N_{1} & =0\\
P_{2} - N_{2}& =ma_{cp}
\end{split}
\end{equation*}
where $a_{cp}$ is the centripetal acceleration. We can rewrite these equations in the form
\begin{equation}%
\left.
\begin{split}
N_{1} & = P_{1} \\
N_{2} & = P_{2} - ma_{cp}
\label{eq-46}
\end{split}
\right\}
\end{equation}
Here it is clear that $N_{2}$ is less than $N_{1}$ since, firstly, $P_{2}$ is less than $P_{1}$ (from the effect of the flattening at the poles) and, secondly, we subtract from $P_{2}$ the quantity $ma_{cp}$ (the effect of rotation of the earth).

\stdb So, the expression ``a body has lost half of its weight'' does not mean that the force with which it is attracted to the earth (or any other planet) has been reduced by one half?

\tchr No, it doesn't. The force of attraction may not change at all. This expression means that \hlblue{the force with which the body bears against its support (in other words, the bearing reaction) has been reduced by one half.}

\stdb But then it follows that I can dispose of the ``weightness'' of a body quite freely. What can prevent me from digging a deep pit under the body and letting it fall into the
pit together with its support? In this case, there will be no force whatsoever bearing on the support. Does that mean that the body has completely ``lost its weight''? That it is in a state of weightlessness?

\tchr You have independently come to the correct conclusion. As a matter of fact, \hlblue{the state of weightlessness is a state of fall of a body.} In this connection I wish to make several remarks. I have come across the interpretation of weightlessness as a state in which the force of attraction of the earth is counterbalanced by some other force. In the case of a satellite, the centrifugal force was suggested as this counterbalancing force. The statement was as follows: the force with which the satellite is attracted by the earth and the centrifugal force counter balance each other and, as a result, the resultant force applied to the satellite equals zero, and this corresponds to weightlessness.


You now understand, of course, that such a treatment is incorrect if only because no centrifugal force acts on the satellite. Incidentally, \hlblue{if weightlessness is understood to be a state in which the force of attraction is counterbalanced by some other force, then it would be more logical to consider a body weightless when it simply rests on a horizontal plane.} This is precisely one of the cases where the weight is counterbalanced by the bearing reaction! Actually, no counter balancing of the force of attraction is required for weightlessness. On the contrary, \hlblue{for a body to become weightless, it is necessary to provide conditions in which no other force acts on it except attraction.} In other words, it is necessary that the bearing reaction equal zero. The motion of a body subject to the force of attraction is the falling of the body. Consequently, weightlessness is a state of falling, for example the falling of a lift in a mine shaft or the orbiting of the earth by a satellite.

\stda In the preceding section (\chap{ch-08}) you mentioned that the orbiting of a satellite about the earth is none other than its falling to the earth for an indefinitely long period of time.

\tchr That the motion of a satellite about the earth is falling can be shown very graphically in the following way. Imagine that you are at the top of a high mountain and throw a stone horizontally. We shall neglect the air resistance. The greater the initial velocity of the stone, the farther it will fall. \figr{fig-42}~\drkgry{(a)} illustrates how the trajectory of the stone gradually changes with an increase in the initial velocity. At a certain velocity $v_{1}$ the trajectory of the falling stone becomes a circle and the stone becomes a satellite of the earth. The velocity $v_{1}$ is called the circular orbital velocity. It is found from equation \eqref{eq-34}
\begin{equation}%
%\left.
%\begin{split}
v_{1} = \sqrt{\frac{GM}{r}}
\label{eq-47}
%\end{split}
%\right\}
\end{equation}
\begin{marginfigure}%[-6cm]
\centering
\includegraphics[width=1.1\linewidth]{figs/ch-09/fig-042a.pdf}
\caption{Trajectory of a body around Earth with different initial velocities.}
\label{fig-42}
\end{marginfigure}
If the radius $r$ of the satellite's orbit is taken approximately equal to the radius of the earth then $v_{1} \approx \SI[per-mode=symbol]{8}{\kilo\metre\per\second}$.



\stda What will happen if in throwing a stone from
the mountain top we continue to increase the initial velocity?

\tchr The stone will orbit the earth in a more and more
elongated ellipse (\figr{fig-42}~\drkgry{(b)}). At a certain velocity $v_{2}$ the trajectory of the stone becomes a parabola and the stone ceases to be a satellite of the earth. The velocity $v_{2}$ is called the escape velocity. According to calculations, $v_{2} \approx \SI[per-mode=symbol]{11}{\kilo\metre\per\second}$. This is about $\sqrt{2}$ times $v_{1}$.

\stda You have defined the state of weightlessness as a state of fall. However, if
the initial velocity of the stone reaches the escape velocity, the stone will leave the earth. In this case, you cannot say that it is falling to the earth. How, then, can you interpret the weightlessness of the stone?

\tchr Very simply. Weightlessness in this case is the falling of the stone to the sun.

\stda Then the weightlessness of a spaceship located somewhere in interstellar space is to be associated with the falling of the ship in the gravitational field of some celestial body?

\tchr Exactly.

\stdb Still, it seems to me that the definition of weightlessness as a state of falling requires some refinement. A parachutist also falls, but he has none of the sensations associated with weightlessness.

\tchr You are right. Weightlessness is not just any kind of falling. Weightlessness is the so-called free fall, i.e. the motion of a body subject only (!) to the force of gravity. I have already mentioned that for a body to become weightless it is necessary to create conditions under which no other force, except the force of attraction, acts on the body. In the case of the fall of a parachutist, there is an additional force, the resistance of the air.

\end{dialogue}


\section*{Problems}
\label{problems-09}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume*=problems]
\item Calculate the density of the substance of a spherical planet where the daily period of rotation equals 10 hours, if it is known that bodies are weightless at the equator of the planet.
\end{enumerate}
%

%\cleardoublepage
%\thispagestyle{empty}
%\vspace*{2cm}
%
%\begin{figure*}
%\centering
%\includegraphics[width=0.65\linewidth]{sec-d.pdf}
%%\caption{Velocity \emph{vs} time graph for the function shown in Figure \ref{fig-05}.}
%%\label{fig-06}
%\end{figure*}
%%\addtocounter{figure}{1}
%% For incrementing the figure numbers for figures without caption or numbering
%%\begin{fullwidth}
%\begin{Large}
%\end{Large}
%%\end{fullwidth}
%



%
\cleardoublepage

\thispagestyle{empty}
\vspace*{-2.2cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.68\textwidth]{figs/sec/tarasov-qasp-sec-05.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{The role of the physical laws of conservation can scarcely be overestimated. They constitute the most general rules established by mankind on the basis of the experience of many generations. Skillful application of the laws of conservation enables many problems to be solved with comparative ease. Let us consider examples concerning the laws of conservation of energy and momentum.
}}}
\end{fullwidth}
