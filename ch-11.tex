% !TEX root = tarasov-qasp-2.tex

\chapter{Can You Deal With Harmonic Vibrations?}
\label{ch-11}

\begin{dialogue}

\tchr Some examinees do not have a sufficiently clear
understanding of harmonic vibrations. First let us discuss their definition.

\stda Vibrations are said to be harmonic if they obey the sine law: the deviation $x$ of a body from its equilibrium position varies with time as follows
\begin{equation}%
x= A \sin ( \omega t + a)
\label{eq-65}
\end{equation}
where $A$ is the amplitude of vibration (maximum deviation of the body from the position of
equilibrium), $\omega$ is the circular frequency ($\omega = 2 \pi T$, where $T$ is the period of vibration), and $\alpha$ is the initial phase (it indicates the deviation of the body from the position of equilibrium at the instant of time $t=0$). The idea of harmonic vibrations is conveyed by the motion of the projection of a point which rotates at uniform angular velocity $\omega$ in a circle of radius $A$ \drkgry{(\figr{fig-47})}. 
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-11/fig-047a.pdf}
\caption{Simple harmonic motion and its relations.}
\label{fig-47}
\end{marginfigure}

\stdb I prefer another definition of harmonic vibrations. As is known, vibrations occur due to action of the restoring force, i.e. a force directed toward the position of equilibrium and increasing as the body recedes from the equilibrium position. Harmonic vibrations are those in which the restoring force $F$ is proportional to the deviation $x$ of the body from the equilibrium position. Thus
\begin{equation}%
F=kx
\label{eq-66}
\end{equation}
Such a force is said to be ``elastic''.

\tchr I am fully satisfied with both proposed definitions. In the first case, harmonic vibrations are defined on the basis of how they occur; in the second case, on the basis of their cause. In other words, the first definition uses the space-time (kinematic) description of the vibrations, and the second, the causal (dynamic) description.

\stdb But which of the two definitions is preferable? Or, maybe, they are equivalent?

\tchr No, they are not equivalent, and the first (kinematic) is preferable. It is more complete.

\stdb But whatever the nature of the restoring force, it will evidently determine the nature of the vibrations. I don't understand, then, why my definition is less complete.

\tchr This is not quite so. The nature of the restoring force does not fully determine the nature of the vibrations.

\stda Apparently, now is the time to recall that the nature of the motion of a body at a given instant is determined not only by the forces acting on the body at the given instant, but by the initial conditions as well, i.e. the position and velocity of the body at the initial instant. We discussed this in \chap{ch-04}.

\tchr Absolutely correct. With reference to the case being considered this statement means that the nature of the vibrations is determined not only by the restoring force, but by the conditions under which these vibrations started. It is evident that vibrations can be effected in various ways. For
example, a body can be deflected a certain distance from its equilibrium position and then smoothly released. It will begin to vibrate. If the beginning of vibration is taken as the zero instant, then from equation \eqref{eq-65}, we obtain $\alpha=\pi/2$, and the distance the body is deflected is the amplitude of vibration. The body can be deflected different distances from the equilibrium position, thereby setting different amplitudes of vibration.

Another method of starting vibrations is to impart a certain initial velocity (by pushing) to a body in a state of equilibrium. The body will begin to vibrate. Taking the beginning of vibration as the zero point, we obtain from equation \eqref{eq-65} that $\alpha=0$. The amplitude of these vibrations depends upon the initial velocity imparted to the body. It is evidently possible to propose innumerable other, intermediate methods of exciting vibrations. For instance, a body is deflected from its position of equilibrium and, at the same time, is pushed or plucked, etc. Each of these methods will set definite values of the amplitude $A$ and the initial phase $\alpha$ of the vibration.

\stdb Do you mean that the quantities $A$ and $\alpha$ do not depend upon the nature of the restoring force?

\tchr Exactly. You manipulate these two quantities at your own discretion when you excite vibrations by one or another method. The restoring force, i.e. coefficient $k$ in equation \eqref{eq-66}, determines only the circular frequency $\omega$ or, in other words, the period of vibration of the body. It can be said that \hlblue{the period of vibration is an intrinsic characteristic of the vibrating body, while the amplitude $A$ and the initial phase $\alpha$ depend upon the external conditions that excite the given vibration.}

Returning to the definitions of harmonic vibrations, we see that the dynamic definition contains no information on either the amplitude or initial phase. The kinematic definition, on the contrary, contains information on these quantities.

\stdb But if we have such a free hand in dealing with the amplitude, maybe it is not so important a characteristic of a vibrating body?

\tchr You are mistaken. The amplitude is a very important characteristic of a vibrating body. To prove this, let us consider an example. A ball of mass $m$ is attached to two elastic springs and accomplishes harmonic vibrations of amplitude $A$ in the horizontal direction \figr{fig-48}. The restoring force is determined by the coefficient of elasticity $k$ which characterizes the elastic properties of the springs. Find the energy of the vibrating ball.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-11/fig-048a.pdf}
\caption{Simple harmonic motion and its relations.}
\label{fig-48}
\end{figure}
%Fig. 49
\stda To find the energy of the ball, we can consider its position of extreme deflection ($x = A$). In this position, the velocity of the ball equals zero and therefore its total energy is its potential energy. The latter can be determined as the work done against the restoring force $F$ in displacing the
ball the distance $A$ from its equilibrium position. Thus
\begin{equation}%
W = FA
\label{eq-67}
\end{equation}
Next, taking into account that $F=kA$, according to equation \eqref{eq-66}, we obtain
\begin{equation*}%
W=kA^{2}
\end{equation*}
\tchr You reasoned along the proper lines, but committed an error. Equation \eqref{eq-67} is applicable only on condition that the force is constant. In the given case, force $F$ varies with the distance, as shown in \figr{fig-49}. The work done by this force over the distance $x=A$ is equal to the hatched area under the force curve. This is the area of a triangle and is equal to
$\dfrac{kA^{2}}{2}$. Thus
\begin{equation}%
W = \frac{kA^{2}}{2}
\label{eq-68}
\end{equation}
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-11/fig-049a.pdf}
\caption{Force varying with distance.}
\label{fig-49}
\end{figure}
Note that \hlblue{the total energy of a vibrating body is proportional to the square of the amplitude of vibration.} This demonstrates what an important characteristic of a vibrating body the amplitude is. If $0 < x < A$, then the total energy $W$ is the sum of two components-the kinetic and potential energies
\begin{equation}%
W = \frac{kA^{2}}{2} = \frac{m v^{2}}{2} + \frac{k x^{2}}{2}
\label{eq-69}
\end{equation}
Equation \eqref{eq-69} enables the velocity $v$ of the vibrating ball to be found at any distance $x$ from the equilibrium position. My next question is: \hlred{what is the period of vibration of the ball shown in \figr{fig-48}?}

\stdb To establish the formula for the period of vibration it will be necessary to employ differential calculus.

\tchr Strictly speaking, you are right. However, if we simultaneously use the kinematic and dynamic definitions of harmonic vibrations we can manage without differential calculus. As a matter of fact, we can' conclude from \figr{fig-47}, which is a graphical expression of the kinematic definition, that the velocity of the body at the instant it passes the equilibrium position is
\begin{equation}%
v_{1} = \omega A =  \frac{2 \pi A}{T}
\label{eq-70}
\end{equation}
Using the result of equation \eqref{eq-68}, following from the dynamic definition, we can conclude that velocity $v_{1}$ can be found from the energy relation
\begin{equation}%
\frac{m v_{1}^{2}}{2} = \frac{k A^{2}}{2}
\label{eq-71}
\end{equation}
(at the instant the ball passes the equilibrium position the entire energy of the ball is kinetic energy). Combining equations \eqref{eq-70} and \eqref{eq-71}, we obtain 
\begin{equation*}%
\frac{4 \pi^{2} A^{2} m}{T^{2}}=kA^{2}, 
\end{equation*}
from which
\begin{equation}%
T=2 \pi \sqrt{\frac{m}{k}}
\label{eq-72}
\end{equation}
As mentioned previously, the period of vibration is determined fully by the properties of the vibrating system itself, and is independent of the way the vibrations are set up.

\stda When speaking of vibrations we usually deal, not with a ball attached to springs,
but with a pendulum. Can the obtained results be generalized to include the pendulum?

\tchr For such generalization we must first find out what, in the case of the pendulum, plays the role of the coefficient of elasticity $k$. It is evident that a pendulum vibrates not due to an elastic force, but to the force of gravity. Let us consider a ball (called a bob in a pendulum) suspended on a string of length $l$. We pull the bob to one side of the equilibrium position so that the string makes an angle $\alpha$, (\figr{fig-50}) with the vertical. Two forces act on the bob: the force of gravity $mg$ and the tension $T$ of the string. Their resultant is the restoring force. As is evident from the figure, it equals $mg \sin \alpha$.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-11/fig-050a.pdf}
\caption{Analysing the motion of a pendulum.}
\label{fig-50}
\end{figure}
\stda Which of the lengths, $\overline{AB}$ or $\overline{AC}$, should be considered the deflection of the pendulum from the equilibrium position (see \figr{fig-50})? .

\tchr We are analysing the harmonic vibrations of a pendulum. For this it is necessary that the angle of maximum deviation of the string from the equilibrium position be very small
\begin{equation}%
\alpha \ll 1
\label{eq-73}
\end{equation}
(note that here angle $\alpha$ is expressed in radians; in degrees, angle $\alpha$ should, in any case, be less than \ang{10}). If condition \eqref{eq-73} is complied with, the difference between the lengths $\overline{AB}$ and $\overline{AC}$ can be neglected
\begin{equation*}%
\overline{AB} = l \sin \alpha \approx \overline{AC} = l \tan \alpha
\end{equation*}

Thus your question becomes insignificant. For definiteness, we can assume that $x = \overline{AB}=l \sin \alpha$. Then equation \eqref{eq-66} will take the following form for a pendulum
\begin{equation}%
mg \sin \alpha = k l \sin \alpha
\tag{66a}
\label{eq-66a}
\end{equation}
from which
\begin{equation}%
k= \frac{mg}{l}
\label{eq-74}
\end{equation}
Substituting this equation into equation \eqref{eq-72}, we obtain the formula for the period of harmonic vibrations of a pendulum
\begin{equation}%
T=2 \pi \sqrt{\frac{l}{g}}
\label{eq-75}
\end{equation}
We shall also take up the question of the energy of the pendulum. Its total energy is evidently equal to $mgh$, where $h$ is the height to which the bob ascends at the extreme position (see \figr{fig-50}). Thus
\begin{equation}%
W = mgh = mgl \left( 1- \cos \alpha \right) = 2mgl \sin^{2} \frac{\alpha}{2}
\label{eq-76}
\end{equation}
Relationship \eqref{eq-76} is evidently suitable for all values of angle $\alpha$. To convert this result to relationship \eqref{eq-68}, it is necessary to satisfy the condition of harmonicity of the pendulum's vibrations, i.e. inequality \eqref{eq-73}. Then, $\sin \alpha$ can be approximated by the angle $\alpha$ expressed in radians, and equation \eqref{eq-76} will change to
\begin{equation*}%
W \approx 2 m g l \left( \frac{\alpha}{2}\right)^{2} = mgl \left( \frac{\alpha^{2}}{2}\right)
\end{equation*}
Taking equation \eqref{eq-74} into consideration, we finally obtain
\begin{equation*}%
W = k \left( \frac{(l \alpha)^{2}}{2}\right) \approx k \frac{\overline{(AB)}^{2}}{2}
\end{equation*}
which is, in essence, the same as equation \eqref{eq-68}.

\stdb If I remember correctly, in previously studying the vibrations of a pendulum, there was generally no requirement about the smallness of the angle of deviation.

\tchr This requirement is unnecessary if we only deal with the energy of the bob or the tension of the string. In the given case we are actually considering, not a pendulum, but the motion of a ball in a circle in a vertical plane. However, if the problem involves formula \eqref{eq-75} for the period of vibrations, then the vibration of the pendulum must necessarily be harmonic and, consequently, the angle of deviation must be small. For instance, in Problem~No.~33, the condition of the smallness of the angle of deviation of the pendulum is immaterial, while in Problem~No.~34 it is of vital importance.
\end{dialogue}

\section*{Problems}
\label{prob-11}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume*=problems]
\item A ball accomplishes harmonic vibrations as shown in \figr{fig-48}. Find the ratio of the velocities of the ball at points whose distances from the equilibrium position equal one half and one third of the amplitude.

\item A bob suspended on a string is deflected from the equilibrium position by an angle of \ang{60} and is then released. Find the ratio of the tensions of the string for the equilibrium position and for the maximum deviation of the bob.

\item A pendulum in the form of a ball (bob) on a thin string is deflected through an angle of \ang{5}. Find the velocity of the bob at the instant it passes the equilibrium position if the circular frequency of vibration of the pendulum equals \SI{2}{\per\second}.
\end{enumerate}

%\cleardoublepage
%
%\thispagestyle{empty}
%\vspace*{-1.5cm}
%\begin{figure*}[!ht]
%\centering
%\includegraphics[width=0.75\textwidth]{figs/sec/sec-e.pdf}
%\end{figure*}
%%\begin{centering}
%\begin{fullwidth}
%%%\paragraph{}
%{\textsf{\Large \hlred{The world about us is full of vibrations and waves. Remember this when you study the branch of physical science devoted to these phenomena. Let us discuss harmonic vibrations and, as a special case, the vibrations of a mathematical pendulum. We shall analyse the behaviour of the pendulum in non-inertial frames of reference.
%}}}
%\end{fullwidth}
