% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{Can You Apply Coulomb's Law?}
\label{ch-26}

\begin{dialogue}

\tchr Let us discuss Coulomb's law in more detail, as well as problems that are associated with the application of this law. First of all, please state Coulomb's law.

\stda The force of interaction between two charges is proportional to the product of the charges and inversely proportional to the square of the distance between them.

\tchr Your statement of this law is incomplete; you have left out some points.

\stdb Perhaps I should add that the force of interaction is inversely proportional to the dielectric constant $K_{e}$ of the medium. Is that it?

\tchr It wouldn't be bad to mention it, of course. But that is not the main omission. You have forgotten again that a force is a vector quantity. Consequently, in speaking of the magnitude of a force, don't forget to mention its direction (in this connection, remember our discussion of Newton's second law in \chap{ch-04}).

\stda Now I understand. You mean we must add that the force with which the charges interact is directed along the line connecting the charges?

\tchr That is insufficient. There are two directions along a line.

\stda Then we must say that the charges repulse each other if they are of the same sign and attract each other if they are of opposite signs.

\tchr Good. Now, if you collect all these additions, you will obtain a complete statement of Coulomb's law. It would do no harm to emphasize that this law refers to interaction between point charges.
\stdb Can the equation of Coulomb's law be written so that it contains full information concerning the law? The ordinary form
\begin{equation}%
F = B \frac{q_{1}q_{2}}{K_{e}r^{2}}
\label{eq-157}
\end{equation}
yields no information on the direction of the force.

\tchr Coulomb's law can be written in this way. For this we first have to find out what force we are referring to. Assume that we mean the force with which charge $q_{1}$ acts on charge $q_{2}$ (and not the other way round). We introduce coordinate axes with the origin at charge $q_{1}$. Then we draw vector from the origin to the point where charge $q_{2}$ is located (\figr{fig-99}). This vector is called the radius vector of charge $q_{2}$. In this case, the complete formula of Coulomb's law will be
\begin{equation}%
\vec{F} = B \frac{q_{1}q_{2}}{K_{e}r^{3}} \vec{r}
\label{eq-158}
\end{equation}
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-26/fig-099a}
\caption{Direction in Coulomb's law.}
\label{fig-99}
\end{marginfigure}
where factor $B$ depends upon the selection of the system of units.
\stda But in this equation the force is inversely proportional, not to the
square, but to the cube of the distance between the charges!

\tchr Not at all. Vector $(\vec{r}/r)$ is numerically equal to unity (dimensionless unity!). It is called a unit vector. It serves only to indicate direction.

\stda Do you mean that I can just write equation \eqref{eq-158} if I am asked about Coulomb's law? Nothing else?

\tchr You will only have to explain the notation in the equation.

\stda And what if I write equation \eqref{eq-157} instead of \eqref{eq-158}?

\tchr Then you will have to indicate verbally the direction of the Coulomb force.

\stda How does equation \eqref{eq-158} show that the charges attract or repulse each other?

\tchr If the charges are of the same sign, then the product $q_{1}q_{2}$ is positive. In this case vector $\vec{F}$ is parallel to vector $\vec{r}$. Vector $\vec{F}$ is the force applied to charge $q_{2}$; charge $q_{2}$ is repulsed by charge $q_{1}$. If the charges are of opposite sign, the product $q_{1}q_{2}$ is negative and then vector $\vec{F}$ will be anti-parallel to vector $\vec{r}$ i.e. charge $q_{2}$ will be attracted by charge $q_{1}$.

\stda Please explain what we should know about factor $B$.

\tchr This factor depends upon the choice of a system of units. If you use the absolute electrostatic (cgse) system of units, then $B=1$; if you use the International System of Units (51), then $B=\dfrac{1}{4 \pi \epsilon_{0}}$ , where the constant $\epsilon_{0} = \SI[per-mode=symbol]{8.85d-12}{\coulomb \squared \per \newton \per \metre \squared}$%{\couIomb^{2} \per \newton \metre^{2}}$

Let us solve a few problems on Coulomb's law.

\begin{enumerate}[label=Problem \arabic*.,font=\bfseries,series=p3,leftmargin=1cm]
\item \hlred{Four identical charges $q$ are located at the corners of a square. What charge $Q$ of opposite sign must be placed at the centre of the square so that the whole system of charges is in equilibrium?}
\end{enumerate}
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-26/fig-100a}
\caption{What is the charge of $Q$?}
\label{fig-100}
\end{marginfigure}

\stda Of the system of five charges, four are known and one is unknown. Since the system is in equilibrium, the sum of the forces applied to each of the five charges equals zero. In other words, we must deal with the equilibrium of each of the five charges.

\tchr That will be superfluous. You can readily see that charge $Q$ is in equilibrium, regardless of its magnitude, due to its geometric position. Therefore, the condition of equilibrium with respect to this charge contributes nothing to the solution. Owing to the symmetry of the square, the remaining four charges $q$ are completely equivalent. Consequently, it is sufficient to consider the equilibrium of only one of these charges, no matter which. We can select, for example, the charge at point $A$ (\figr{fig-100}). What forces act on this charge?
\stda Force $F_{1}$ from the charge at point $B$, force $F_{2}$ from the charge at point $D$ and, finally, the force from the sought-for charge at the centre of the square.

\tchr I beg your pardon, but why-didn't you take the charge at point $C$ into account?

\stda But it is obstructed by the charge at the centre of the square.

\tchr This is a naive error. Remember: in a system of charges each charge is subject to forces exerted by all the other charges of the system without exception. Thus, you will have to add force $F_{3}$ acting on the charge at point $A$ from the charge at point $C$. The final diagram of forces is shown in \figr{fig-100}.

\stda Now, everything is clear. I choose the direction $AB$ and project all the forces applied to the charge at point $A$ on this direction. The algebraic sum of all the force projections should equal zero, i.e.
\begin{equation*}%
F_{4} = 2F_{1} \cos \ang{45} + F_{3}
\end{equation*}
Denoting the side of the square by $a$, we can rewrite this equation in the form
\begin{equation*}%
\frac{Qq}{\left( \dfrac{a^{2}}{2} \right)} = \sqrt{2} \left( \frac{q^{2}}{a^{2}} \right)+ \left( \frac{q^{2}}{2a^{2}} \right)
\end{equation*}
from which
\begin{equation}%
Q = \frac{q}{4} \left( 2\sqrt{2} + 1 \right)
\label{eq-159}
\end{equation}
\tchr Quite correct. Will the equilibrium of this system of charges be stable?
\stdb No, it won't. This is unstable equilibrium. Should anyone of the charges shift slightly, all the charges will begin moving and the system will break up.

\tchr You are right. It is quite impossible to devise a stable equilibrium configuration of stationary charges.

\begin{enumerate}[label=Problem \arabic*.,font=\bfseries,resume=p3,leftmargin=*]
\item \hlred{Two spherical bobs of the same mass and radius, having equal charges and suspended from strings of the same length attached to the same point, are submerged in a liquid dielectric of permittivity $K_{e}$, and density $\rho_{0}$. What should the density $\rho$ of the bob material be for the angle of divergence of the strings to be the same in the air and in the dielectric?}
\end{enumerate}
\begin{figure}%[-5cm]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-26/fig-101a}
\caption{What is the charge of $Q$?}
\label{fig-101}
\end{figure}

\stdb The divergence of the strings is due to Coulomb repulsion of the bobs. Let $F_{e1}$ denote Coulomb repulsion in the air and $F_{e2}$ in the liquid dielectric.

\tchr In what way do these forces differ?
\stdb Since, according to the conditions of the problem, the angle of divergence of the strings is the same in both cases, the distances between the bobs are also the same. Therefore, the difference in the forces $F_{e1}$ and $F_{e2}$ is due only to the dielectric permittivity. Thus
\begin{equation}%
F_{e1} = K_{e}F_{e2}
\label{eq-160}
\end{equation}
Let us consider the case where the bobs are in the air. From the equilibrium of the bobs we conclude that the vector sum of the forces $F_{e1}$ and the weight should be directed along the string because otherwise it cannot be counterbalanced by the reaction of the string (\figr{fig-101}~\drkgry{(a)}). It follows that
\begin{equation*}%
\frac{F_{e1}}{P} = \tan \alpha
\end{equation*}
where $\alpha$ is the angle between the string and the vertical. When the bobs are submerged in the dielectric, force $F_{e1}$ should be replaced by force $F_{e2}$, and the weight $P$ by the difference $(P-F_{b})$, where $F_{b}$ is the buoyant force. However, the ratio of these new forces should, as before, be equal to $\tan \alpha$ (\figr{fig-101}~\drkgry{(b)}). Thus
\begin{equation*}%
\frac{F_{e2}}{P - F_{b}} = \tan \alpha
\end{equation*}
Using the last two equations, we obtain
\begin{equation*}%
\frac{F_{e1}}{P} = \frac{F_{e2}}{P - F_{b}}
\end{equation*}
After substituting equation \eqref{eq-160} and taking into consideration that $P=Vg\rho$ and $F_{b}=Vg\rho_{0}$, we obtain
\begin{equation*}%
\frac{K_{e}}{\rho} = \frac{1}{\rho - \rho_{0}}
\end{equation*}
and the required density of the bob material is
\begin{equation}%
\rho = \frac{\rho_{0}\, K_{e}}{K_{e} - 1}
\label{eq-161}
\end{equation}
\tchr Your answer is correct.

\begin{enumerate}[label=Problem \arabic*.,font=\bfseries,resume=p3,leftmargin=*]
\item \hlred{Two identically charged spherical bobs of mass $m$ are suspended on strings of length $l$ each and attached to the same point. At the point of suspension there is a third ball of the same charge \drkgry{(\figr{fig-102})}. Calculate the charge $q$ of the bobs and ball if the angle between the strings in the equilibrium position is equal to $\alpha$.}
\end{enumerate}
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-26/fig-102a}
\caption{What is the charge of $Q$?}
\label{fig-102}
\end{marginfigure}

\stdb We shall consider bob $A$. Four forces (\figr{fig-102}) are applied to it. Since the bob is in equilibrium, I shall resolve these forces into components in two directions \ldots{}

\tchr (interrupting): In the given case, there is a simpler solution. The force due to the charge at the point of suspension has no influence whatsoever on the equilibrium position of the string: force $F_{e2}$ acts along the string and is counterbalanced in any position by the reaction of the string. Therefore, the given problem can be dealt with as if there were no charge at all at the point of suspension of the string. As a rule, examinees don't understand this.
\stdb Then we shall disregard force $F_{e2}$. Since the vector sum of the forces $F_{e1}$ and $P$ must be directed along the string, we obtain
\begin{equation}%
\frac{F_{e1}}{P} = \tan \frac{\alpha}{2}
\label{eq-162}
\end{equation}
\tchr Note that this result does not depend upon the presence or absence of a charge at the point of suspension.

\stdb Since
\begin{equation*}%
F_{e1} = \frac{q^{2}}{4 l^{2} \sin^{2} \left(\dfrac{\alpha}{2}\right)}
\end{equation*}
we obtain from equation \eqref{eq-162}:
\begin{equation*}%
\frac{q^{2}}{4 l^{2} mg \sin^{2} \left(\dfrac{\alpha}{2}\right)} = \tan \frac{\alpha}{2}
\end{equation*}
Solving for the required charge, we obtain
\begin{equation}%
q = 2l \sin \left(\dfrac{\alpha}{2}\right) \sqrt{mg \tan \frac{\alpha}{2}}
\label{eq-163}
\end{equation}
\tchr Your answer is correct.

\stda When will the presence of a charge at the point of suspension be of significance?

\tchr For instance, when it is required to find the tension of the string.

\end{dialogue}

\section*{Problems}
\label{prob-26}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume=problems]
\item Identical charges are $+q$ located at the vertices of a regular hexagon. What charge must be placed at the centre of the hexagon to set the whole system of charges at equilibrium?
\item A spherical bob of mass $m$ and charge $q$ suspended from a string of length $l$ rotates about a
fixed charge identical to that of the bob (\figr{fig-103}). The angle between the string and the vertical is $\alpha$. Find the angular velocity of uniform rotation of the bob and the tension of the string.
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-26/fig-103a}
\caption{What is the angular velocity of uniform rotation of the bob and the tension of the string.}
\label{fig-103}
\end{marginfigure}
\item A spherical bob of mass $m$ with the charge $q$ can rotate in a vertical plane at the end of a string of length $l$. At the centre of rotation there is a second ball with a charge identical in sign and magnitude to that of the rotating bob. What minimum horizontal velocity must be imparted to the bob in its lower position to enable it to make a full revolution?
\end{enumerate}








\cleardoublepage

\thispagestyle{empty}
\vspace*{-1.95cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.75\textwidth]{figs/sec/tarasov-qasp-sec-11.pdf}
\end{figure*}
\vspace*{-.4cm}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{Electric currents have become an integral part of our everyday life, and so there is no need to point out the importance of the Ohm and the Joule-Lenz laws. But how well do you know these laws?
}}}
\end{fullwidth}

\cleardoublepage
