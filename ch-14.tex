% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{What Do You Know About The Equilibrium Of Bodies?}
\label{ch-14}

\begin{dialogue}

\tchr Two positions of equilibrium of a brick are shown in \figr{fig-59}. Both equilibrium positions are stable, but their degree of stability differs. \hlred{Which of the two positions is the more stable?}

\begin{marginfigure}
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-14/fig-059a.pdf}
\caption{Which brick is more stable?}
\label{fig-59}
\end{marginfigure}

\stda Evidently, the position of the brick in \emph{Figure \ref{fig-59}(a)}.

\tchr Why?

\stda Here the centre of gravity of the brick is nearer to the earth's surface,

\tchr This isn't all.

\stdb The area of the bearing surface is greater than in the position shown in \figr{fig-59}~\drkgry{(b)}.

\tchr And this isn't all either. To clear it up, let us consider the equilibrium of two bodies: a rectangular parallelepiped with a square base and a right circular cylinder \figr{fig-60}~\drkgry{(a)}. Assume that the parallelepiped and cylinder are of the same height $H$ and have bases of the same area $S$. In this case, the centres of gravity of the bodies are at the same height and, in addition, they have bearing surfaces of the same area. Their degrees of stability, however, are different.

The measure of the stability of a specific state of equilibrium is the energy that must be expended to permanently disturb the given state of the body.
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-14/fig-060a.pdf}
\caption{Comparing equilibrium of two bodies, which is more stable?}
\label{fig-60}
\end{figure}
\stdb What do you mean by the word ``permanently''?

\tchr It means that if the body is subsequently left to itself, it cannot return to the initial state again. This amount of energy is equal to the product of the weight of the body by the height to which the centre of gravity must be raised so that the body cannot return to its initial position. In the example with the parallelepiped and cylinder, the radius of the cylinder is $R=\sqrt{S/\pi}$ and the side of the parallelepiped's base is $a=\sqrt{S}$. To disturb the equilibrium of the cylinder, its centre of gravity must be raised through the height \figr{fig-60}~\drkgry{(b)}
\begin{equation*}%
h_{1} = \sqrt{ \left( \frac{H}{2}\right)^{2} + R^{2}} - \frac{H}{2}
\end{equation*}
To disturb the equilibrium of the parallelepiped, its centre of gravity must be raised \figr{fig-60}~\drkgry{(b)}
\begin{equation*}%
h_{2} = \sqrt{ \left( \frac{H}{2}\right)^{2} +  \left(\frac{a}{2}\right)^{2}} - \frac{H}{2}
\end{equation*}
Since
\begin{equation*}%
 \left( \frac{a/2}{R} \right)  = \frac{\sqrt{\pi S}}{2\sqrt{S}} = \frac{\sqrt{\pi}}{2} < 1
\end{equation*}
it follows that $h_{2} < h_{1}$ Thus, of the two bodies considered, the cylinder is the more stable.

Now I propose that we return to the example with the two positions of the brick.

\stda If we turn over the brick it will pass consecutively from one equilibrium position to another. The dashed line in \figr{fig-61} shows the trajectory described by its centre of gravity in this process. To change the position of a lying brick its centre of gravity should be raised through the height $h_{1}$ expending an energy equal to $mgh_{1}$ and to change its upright
position, the centre of gravity should be raised through $h_{2}$, the energy expended being $mgh_{2}$.  The greater degree of stability of the lying brick is due to the fact that 
\begin{equation}%
mgh_{1} > mgh_{2}
\label{eq-82}
\end{equation}

\begin{marginfigure}
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-14/fig-061a.pdf}
\caption{Trajectory described by the centre of gravity of a brick when turning over.}
\label{fig-61}
\end{marginfigure}
\tchr At last you've succeeded in substantiating the greater stability of the lying position of a body.

\stdb But it is evident that the heights $h_{1}$ and $h_{2}$ depend upon the height of the centre of gravity above floor level and on the area of the base. Doesn't that mean that in discussing the degree of stability of bodies it is correct to compare the heights of the centres of gravity and the areas of the bases?

\tchr Why yes, it is, but only to the extent that these quantities influence the difference between the heights $h_{1}$ and $h_{2}$. Thus, in the example with the parallelepiped and cylinder, the comparison of the heights of the centres of gravity and the areas of the bases is insufficient evidence for deciding which of the bodies is the more stable. Besides, I wish to draw your attention to the following. Up till now we have tacitly assumed that the bodies were made of the same material. In this case, the inequality \eqref{eq-82} could be satisfied by observing the geometric condition $h_{1}>h_{2}$. 

In the general case, however, bodies may be made of different materials, and the inequality  \eqref{eq-82} may be met even when $h_{1}<h_{2}$ owing to the different densities of the bodies. For example, a cork brick will be less stable in the lying position than a lead brick in the upright position. Let us now see what conditions for the equilibrium of bodies you know.

\stda The sum of all the forces applied to a body should equal zero. In addition, the weight vector of the body should fall within the limits of its base.

\tchr Good. It is better, however, to specify the conditions of equilibrium in a different form, more general and more convenient for practical application. Distinction should be made between two conditions of equilibrium:

\begin{description}[leftmargin=1cm,font=\bfseries]
\item[First condition:] The projections of all forces applied to the body onto any direction, should mutually compensate one another. In other words, the algebraic sum of the projections of all the forces onto any direction should equal zero. This condition enables as many equations to be written as there
are independent directions in the problem: one equation for a one-dimensional problem, two for a two-dimensional problem and three for the general case (mutually perpendicular directions are chosen).

\item[Second condition (moment condition):] The algebraic sum of the moments of the forces about any point should equal zero. Here, all the force moments tending to turn the body about the chosen point in one direction (say, clockwise) are taken with a plus sign and all those tending to turn the body in the opposite direction (counterclockwise) are taken with a minus sign. To specify the moment condition, do the following:
\begin{enumerate}[label=(\alph*), leftmargin=1cm]
 \item establish all forces applied to the body; 
 \item choose a point with respect to which the force moments are to be considered;
\item find the moments of all the forces with respect to the chosen point; 
 \item write the equation for the algebraic sum of the moments, equating it to zero. 
 \end{enumerate}
\end{description}
 In applying the moment condition, the following should be kept in mind: 
\begin{enumerate}[label=(\arabic*), leftmargin=1cm]
\item the above stated condition refers to the case when all the forces in the problem and their arms are in a single plane (the problem is not three-dimensional), and
\item the algebraic sum of the moments should be equated to zero with respect to any point, either within or outside the body. 
\end{enumerate}

It should be emphasized that though the values of the separate force moments do depend upon
the choice of the point - with respect to which the force moments are considered), the algebraic sum of the moments equals zero in any case. To better understand the conditions of equilibrium, we shall consider a specific problem. 

\hlred{A beam of weight $P_{1}$ is fixed at points $B$ and $C$ \drkgry{(\figr{fig-62}~(a))}. At point $D$, a load with a weight of $P_{2}$ is suspended from the beam. The distances $\overline{AB}=a,\overline{BC}=2a \,\, \text{and}\,\, \overline{CD}=a$. Find the reactions $N_{B}$ and $N_{C}$ at the two supports. Assume that the reactions of the supports are directed vertically.} 

As usual, first indicate the forces applied to the body.
 \begin{figure}[!htb]%[-3cm]
\centering
\includegraphics[width=.6\textwidth]{figs/ch-14/fig-062a.pdf}
\caption{A beam with suspended loads, the problem is to find the reactions of the supports.}
\label{fig-62}
\end{figure}
\stda The body in the given problem is the beam. Four forces are applied to it: weights $P_{1}$ and $P_{2}$ and reactions $N_{B}$ and $N_{C}$.

\tchr Indicate these forces on the drawing.

\stda But I don't know whether the reactions are directed upward or downward.

\tchr Assume that both reactions are directed upward.

\stda Well, here is my drawing \drkgry{(\figr{fig-62}~(b))}. Next I can specify the first condition of equilibrium by writing the equation
\begin{equation*}%
N_{B}+N_{C}=P_{1}+P_{2}
\end{equation*}

\tchr I have no objection to this equation as such. However, in our problem it is simpler to use the second condition of equilibrium (the moment condition), employing it first with respect to point $B$ and then to point $C$.

\stda All right, I'll do just that. As a result I can write the equations 
\begin{equation}
\left.
\begin{aligned}
 \text{with respect to point $B$: } aP_{1}-2aN_{c}+3aP_{2} &= 0  \label{eq-83}\\ 
 \text{with respect to point $C$: } 2aN_{B}-aP_{1}+aP_{2} &=0
 \end{aligned}
 \right\}
\end{equation}

\tchr Now you see: each of your equations contains only one of the unknowns. It can readily be found. 
\stda From equations \eqref{eq-83} we find
\begin{align}%
N_{B} =\frac{\left( P_{1}-P_{2} \right)}{2}
\label{eq-84}\\
N_{B} =\frac{\left( P_{1}+3P_{2} \right)}{2}
\label{eq-85}
\end{align}

\tchr Equation \eqref{eq-85} always has a positive result. This means that reaction $N_{C}$ is always directed upward (as we assumed). Equation \eqref{eq-84} gives a positive result when $P_{1}>P_{2}$, negative when $P_{1}<P_{2}$ and becomes zero when $P_{1}=P_{2}$. This means that when $P_{1}<P_{2}$, reaction $N_{B}$ is in the direction we assumed, i. e. upward (see \drkgry{(\figr{fig-62}~(b)}); that when $P_{1}<P_{2}$, reaction $N_{B}$ is downward (see  \drkgry{(\figr{fig-62}~(c)}); and at  $P_{1}=P_{2}$ there is no reaction $N_{B}$.

\end{dialogue}


\cleardoublepage
