% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{How Is An Electrostatic Field Described?}
\label{ch-23}

\begin{dialogue}

\paragraph{}
\tchr Thus, we continue the discussion that we began in the preceding section by asking: \hlred{``How is an electrostatic field described?''}

\stdb An electrostatic field is described by means of a vectorial force characteristic called the intensity of the electric field. At each point in the field, the intensity $E$ has a definite direction and numerical value. If we move from one point in a field to another in such a manner that the directions of the intensity vectors are always tangent to the direction of motion, the paths obtained by such motion are called the lines of force of the field. Lines of force are very convenient for graphically representing a field.

\tchr Good. Now let us reason more concretely. The Coulomb force of interaction between two charges $q_{1}$ and $q_{2}$ spaced a distance of $r$ apart can be written in the form
\begin{equation}%
F_{e} = \frac{q_{1} q_{2}}{r^{2}} 
\label{eq-127}
\end{equation}
This can be rewritten as
\begin{equation}%
E(r) =  \dfrac{q_{1}}{r^{2}}
\label{eq-128}
\end{equation}
\begin{equation}%
F_{e} = E(r) q_{2}
\label{eq-129}
\end{equation}
Equation \eqref{eq-128} signifies that charge $q_{1}$ sets up a field around itself, whose intensity at a distance of $r$ from the charge equals $\dfrac{q_{1}}{r^{2}}$. Equation \eqref{eq-129} signifies that this field acts on charge $q_{2}$ located at a distance of $r$ from charge $q_{1}$ with a force $E(r) q_{2}$. Equation \eqref{eq-127} could be written thus because a ``go-between'' - the quantity $E$, the characteristic of the field - was introduced. Try to determine the range of application of equations \eqref{eq-127}, \eqref{eq-128} and \eqref{eq-129}.

\stdb Equation \eqref{eq-127} is applicable for two point charges. That means that the range of application of equations \eqref{eq-128} and \eqref{eq-129} is the same. They were obtained from equation \eqref{eq-127}.

\tchr That is correct only with respect to equations \eqref{eq-127} and \eqref{eq-128}. Equation \eqref{eq-129} has a much wider range of application. No matter what sets up the field $E$ (a point charge, a set of point charges or of charged bodies of arbitrary shape), in all cases the force exerted by this field on charge $q_{0}$ is equal to the product of this charge by field intensity at the point where charge $q_{0}$ is located. The more general version of equation \eqref{eq-129} has the following vectorial form
\begin{equation}%
\vec{F}_{e} = \vec{E}(r) \, q_{0}
\label{eq-130}
\end{equation}
where the arrows, as usual, serve to denote the vectors. It is evident from equation \eqref{eq-130} that the direction of the force acting on charge $q_{0}$ at the given point of the field coincides with the direction of the field intensity at this point if charge $q_{0}$ is positive. If charge $q_{0}$ is negative, the direction of the force is opposite to the intensity. 

Here we can sense the independence of the concept of the field. Different charged bodies set up different electrostatic fields, but each of these fields acts on a charge situated in it according to the same law \eqref{eq-130}. To find the force acting on a charge, you must first calculate the intensity of the field at the point where the charge is located. Therefore, it is important to be able to find the intensity of the field set up by a system of charges. Assume that there are two charges, $q_{1}$ and $q_{2}$. The magnitude and direction of the intensity of the field set up by each of these charges can readily be found for any point in space that may interest us. Assume that at a certain point, specified by the vector $\vec{r}$ these intensities are described by the vectors $\vec{E}_{1}(\vec{r})$ and $\vec{E}_{2}(\vec{r})$. To find the resultant intensity at point $\vec{r}$, you must add vectorially the intensities due to the separate charges
\begin{equation}%
\vec{E}(r) = \vec{E}_{1}(r) + \vec{E}_{2}(r)
\label{eq-131}
\end{equation}
I repeat that the intensities must be added vectorially. (To \smallcaps{Student A}) Do you understand?

\stda Yes, I know that intensities are added vectorially.

\tchr Good. Then we can check how well you can use this knowledge in practice. \hlred{Please draw the lines of force of the field of two equal and opposite charges ($+q_{1}$ and  $-q_{2}$) assuming that one of the charges (for instance, $+q_{1}$) is several times greater than the other.}

\stda I'm afraid I can't. We never discussed such fields before.

\tchr What kind of fields did you study?

\stda I know what the picture of the lines of force looks like for a field set up by two point charges of equal magnitude. I have drawn such a picture in \figr{fig-86}.
\begin{figure}%[-5cm]%
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-23/fig-086a}
\caption{Lines of force looks like for a field set up by two point charges of equal magnitude.}
\label{fig-86}
\end{figure}
\tchr Your drawing is somewhat inaccurate though qualitatively it does represent the force lines of a field set up by two charges of the same magnitude but of opposite sign. Can't you vizualise how this picture will change as one of the charges increases?

\stda We never did anything like that.

\tchr In that case, let us use the rule for the vectorial addition of intensities. We shall begin with the familiar case when the charges are equal \drkgry{(\figr{eq-87}~(a))}. We select three points $A$, $B$ and $C$ and construct a pair of intensity vectors for each point: $\vec{E}_{1}$ and $\vec{E}_{2}$ ($\vec{E}_{1}$ for the field of charge $+q_{1}$ and $\vec{E}_{2}$ for the field of charge $-q_{2}$). Then we add the vectors $\vec{E}_{1}$ and $\vec{E}_{2}$ for each of these points to obtain the resultant vectors $\vec{E}_{A}$, $\vec{E}_{B}$and $\vec{E}_{C}$ . These vectors must be tangent to the lines of force of the field at the corresponding points. These three vectors indicate the behaviour of the lines of force which are shown in \figr{eq-88}~\drkgry{(a)}. Compare this drawing with \figr{eq-86} proposed by you. Note your inaccuracies in the behaviour of the lines of force to the left of charge $-q$ and to the right of charge $+q$. Assume now that charge $+q_{1}$ is doubled in magnitude, and charge $-q_{2}$ is halved \drkgry{(\figr{eq-87}~(b))}. We select, as before, three points $A$, $B$ and $C$. First we construct the intensity vectors for these points and then find their resultants: $\vec{E}_{A}$, $\vec{E}_{B}$ and $\vec{E}_{C}$. The picture of the lines of force corresponding to these vectors is shown in \figr{eq-88}~\drkgry{(b)}.
\begin{marginfigure}[-7.5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-23/fig-088a}
\caption{Lines of force looks like for a field set up by two point charges of equal magnitude.}
\label{fig-88}
\end{marginfigure}
\begin{figure*}%[-5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-23/fig-087a}
\caption{Lines of force looks like for a field set up by two point charges of equal magnitude.}
\label{fig-87}
\end{figure*}
Finally, we assume that $q_{1}$ is doubled again and that $q_{2}$ is halved again \drkgry{(\figr{eq-87}~(c))}. Next we construct the resultant vectors $\vec{E}_{A}$, $\vec{E}_{B}$ and $\vec{E}_{C}$ for points $A$, $B$ and $C$. The corresponding picture of the lines of force is shown in \figr{eq-88}~\drkgry{(c)}. 

As you see, the influence of charge $+q_{1}$ becomes greater with an increase in its relative magnitude; the field of charge $+q_{1}$ begins to repress the field of charge $-q_{2}$.

\stda Now I understand how to construct a picture of the lines of force for a field set up by a system of several charges.

\tchr Let us continue our discussion of an electrostatic field. This field has one important property which puts it in the same class with gravitational fields, namely: the work done by the forces of the field along any closed path equals zero. In other words, if the charge travelling in the field returns to its initial point of departure, the work done by the forces of the field during this motion is equal to zero. Over certain portions of the path this work will be positive and over others negative, but the sum of the work done will equal zero. Interesting consequences follow from this property of an electrostatic field. Can you name them?

\stdb No, I can't think of any.

\tchr I'll help you. You probably have noted that the lines of force of an electrostatic field are never closed on themselves. They begin and end in charges (beginning in positive charges and terminating in negative ones) or they end at infinity (or arrive from infinity). Can you associate this circumstance with the above-mentioned property of the electrostatic field?
\\
 Now I seem to understand. If a line of force in an electrostatic field was closed on itself, then by following it we could return to the initial point. As a charge moves along a line of force, the sign of the work done by the field evidently does not change and, consequently, cannot be equal to zero. On the other hand, the work done along any closed path must a be equal to zero. Hence, lines of force of an electrostatic field cannot be closed on themselves.

\tchr Quite correct. There is one more consequence following from the above-mentioned property of the electrostatic field: the work done in moving a charge from one point of the field to another does not depend upon the path followed.

We can move a charge from point $a$ to point $b$, for instance, along different paths, 1 and 2 (\figr{fig-89}). Let us denote by $A_{1}$ the amount of work done by the forces of the field to move the charge along path 1 and that along path 2 by $A_{2}$ Let us accomplish a complete circuit: from point a to point b along path 1 and from point b back to point a along path 2. During the re- turn along path 2, the work done will be $-A_{2}$. The total work done in a complete circuit is, $A_{1}+ (-A_{2})=A_{1}-A_{2}$. Since the work done along any path closed on itself equals zero, then $A_{1}=A_{2}$. The fact that the work done in moving a charge is independent of the chosen path but depends only on the initial and final points, enables this value to be used as a characteristic of the field (since it depends only upon the chosen points of the field!). Thus another characteristic of an electrostatic field, its potential, is introduced. In contrast to the intensity, this is a scalar quantity since it is expressed in terms of the work done.
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-23/fig-089a}
\caption{Work done in an electrostatic field.}
\label{fig-89}
\end{marginfigure}
\stdb We were told in secondary school that the concept of the potential of a field has no physical meaning. Only the difference in the potentials of any two points of the field has a physical meaning.

\tchr You are quite right. Strictly speaking, the preceding discussion enables us to introduce precisely the difference in the potentials; the potential difference between the two points $a$ and $b$ of the field (denoted by $\left( \varphi_{a}-\varphi_{b} \right)$) is defined as the ratio of the work done by the forces of the field in moving charge $q_{0}$ from point $a$ to point $b$, to charge $q_{0}$, i.e.
\begin{equation}%
\varphi_{a}-\varphi_{b}  = \frac{A_{a \to b}}{q_{0}}
\label{eq-132}
\end{equation}

However, if we assume that the field is absent at infinity (i.e. $\varphi_{\infty}=0$) equation \eqref{eq-132} takes the form , then
\begin{equation}%
\varphi_{a} = \frac{A_{a \to \infty}}{q_{0}}
\label{eq-133}
\end{equation}
In this manner, the potential of the field at the given point can be determined in terms of the work done by the forces of the field in moving a positive unit charge from the given point to infinity. If the work is regarded as being done not by the field, but against the forces of the field, then the potential at a given point is the work that must be done in moving a positive unit charge from infinity to the given point. Naturally, this definition rules out experimental measurement of the potential at the given point of the field, because we cannot recede to infinity in experiment. Precisely for this reason it is said that the difference of the potentials of two points in the field has a physical meaning, while the potential itself at some point has not. We can say that the potential at a given point is determined with an accuracy to an arbitrary constant. The value of the potential at infinity is commonly taken as this constant. The potential is measured from this value. It is assumed, for convenience, that the potential at infinity equals zero.

Within the scope of these assumptions, the potential of a field, set up by a point charge $q_{1}$ measured at a point a distance $r$, from the charge, equals
\begin{equation}%
\varphi\,(r) = \frac{q_{1}}{r}
\label{eq-134}
\end{equation}
You should have no difficulty in determining the potential of a field, set up by several point charges, at some point $\vec{r}$.

\stdb We shall denote the value of the potential at point $\vec{r}$ due to each of the charges separately as $\varphi_{1}(\vec{r}), \,\,\varphi_{2}(\vec{r})$, etc. The total potential $\varphi (\vec{r})$ is equal, evidently, to the algebraic sum of the potentials from the separate charges. Thus
\begin{equation}
\varphi (\vec{r}) = \varphi_{1}(\vec{r}) + \varphi_{2}(\vec{r}) + \ldots
\label{eq-135}
\end{equation}
In this summation, the potential from a positive charge is taken with a plus sign and that from a negative charge with a minus sign.

\tchr Quite correct. Now let us consider the concept of equipotential surfaces. The locus of the points in a field having the same potential is called an equipotential surface (or surface of constant potential). One line of force and one equipotential surface pass through each point in a field. How are they oriented with respect to each other?

\stdb I know that at each point the line of force and the equipotential surface are mutually perpendicular.

\tchr Can you prove that?

\stdb No, I probably can't.
\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=.9\textwidth]{figs/ch-23/fig-090a}
\caption{Proving that lines of force and equipotential surface are mutually perpendicular.}
\label{fig-90}
\end{marginfigure}
\tchr This proof is not difficult. Assume that the line of force $aa_{1}$, and the equipotential surface $S$ (\figr{fig-90}) pass through a certain point $a$. The field intensity at point $a$ is described by vector $\vec{E}_{a}$. Next we shall move charge $q_{0}$ from point a to a certain point $b$ which lies on the equipotential surface $S$ at a short distance $\Delta l$ from point $a$. The work done in this movement is expressed by the equation
\begin{equation}%
A = F_{e}\Delta l \cos \alpha = E_{a}\, q_{0}\, \Delta l \cos \alpha
\label{eq-136}
\end{equation}
where $\alpha$ is the angle between vector $\vec{E}_{a}$ and the direction of the movement. This same amount of work can be expressed as the difference in the field potentials at points $a$ and $b$. Thus we can write another relationship:
\begin{equation}%
A=q_{0} \left( \varphi_{a}-\varphi{b} \right)
\label{eq-137}
\end{equation}
Since both points $a$ and $b$ lie on the same equipotential surface, then it follows that $\varphi_{a}=\varphi_{b}$ This means that according to equation \eqref{eq-137}, the work $A$ should be equal to zero. Substituting this result into equation (\ref{eq-136}), we obtain
\begin{equation}%
E_{a}\, q_{0} \Delta l \cos \alpha=0
\label{eq-138}
\end{equation}
Of all the factors in the left-hand side of equation \eqref{eq-138}, only $\cos \alpha$ can be equal to zero. Thus, we conclude that $\alpha= \ang{90}$. It is clear to you, I think, that this result is obtained for various directions of movement $ab$, provided these movements are within the limits of the equipotential surface $S$. The curvature of the surface does not impair our argument because the movement $\Delta l$ is very small. Along with the use of lines of force, cross-sections of equipotential surfaces are employed to depict an electrostatic field graphically. Taking advantage of the fact that these lines and surfaces are mutually perpendicular, a family of cross-sections of equipotential surfaces can be drawn from a known family of lines of force, or vice versa. 

(To \smallcaps{Student A}) Will you try to draw the cross-sections of equipotential surfaces for the case shown in (\figr{fig-88}\drkgry{(a)})? To avoid confusing them with the lines of force, draw the crosssections of the surfaces with dashed lines.

\stda I shall draw the dashed lines so that they always intersect the lines of force at right angles. Here is my drawing (\figr{fig-91}).

\begin{figure}%[-5cm]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-23/fig-091a}
\caption{Crosssections of equipotential surfaces and lines of force.}
\label{fig-91}
\end{figure}
\tchr Your drawing is correct.

\end{dialogue}




\cleardoublepage

%\thispagestyle{empty}
%\vspace*{-1.95cm}
%\begin{figure*}[!ht]
%\centering
%\includegraphics[width=0.7\textwidth]{figs/sec/sec-i.pdf}
%\end{figure*}
%\vspace*{-.4cm}
%%\begin{centering}
%\begin{fullwidth}
%%%\paragraph{}
%{\textsf{\Large \hlred{What is a field? How is a field described? How does motion take place in a field? These fundamental problems of physics can be most conveniently considered using an electrostatic field as an example. We shall discuss the motion of charged bodies in a uniform electrostatic field. A number of problems illustrating Coulomb's law will be solved.
%}}}
%\end{fullwidth}
%
%\cleardoublepage
