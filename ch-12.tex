% !TEX root = tarasov-qasp-2.tex

\chapter{What Happens To A Pendulum In A State Of Weightlessness?}
\label{ch-12}

\begin{dialogue}
\tchr Suppose we drive a nail in the wall of a lift and suspend a bob on a string of length
$l$ tied to the nail. Then we set the bob into motion so that it accomplishes harmonic vibrations. Assume that the lift ascends with an acceleration of $a$. What is the period of vibration of the pendulum?

\stda When we go up in a lift travelling with acceleration, we feel a certain increase in weight. Evidently, the pendulum should ``feel'' the same increase. I think that its period of vibration can
be found by the formula 
\begin{equation}%
T = 2 \pi \sqrt{\frac{l}{g+a}}
\label{eq-77}
\end{equation}
I cannot, however, substantiate this formula rigorously enough.

\tchr Your formula is correct. But to substantiate it we will have to adopt a point of view that is new to us. So far we have dealt with bodies located in inertial frames of reference only, avoiding non-inertial frames. Moreover, I even warned you against employing non-inertial frames of reference
(\chap{ch-04}). Be that as it may, in the present section it is more convenient to use just this frame of reference which, in the given case, is attached to the accelerating lift. 

Recall that in considering the motion of a body of mass $m$ in a non-inertial frame of reference having an acceleration $a$, we must, on purely formal grounds, apply an additional force to the body. This is called the force of inertia, equal to $ma$ and acting in the direction opposite to the acceleration. After the force of inertia is applied to the body we can forget that the frame of reference is travelling with acceleration, and treat the motion as if it were in an inertial frame. In the case of the lift, we must apply an additional force $ma$ to the bob. This force is constant in magnitude and its direction does not change and coincides with that of the force of gravity $mg$. Thus it follows that in equation \eqref{eq-75} the acceleration $g$ should be replaced by the arithmetical sum of the accelerations $(g+a)$. As a result, we obtain the formula \eqref{eq-77} proposed by you.

\stda Consequently, if the lift descends with a downward acceleration $a$, the period of vibration will be determined by the difference in the accelerations $(g-a)$, since here the force of inertia rna is opposite to the gravitational force. Is that correct?

\tchr Of course. In this case, the period of vibration of the pendulum is
\begin{equation}%
T = 2 \pi \sqrt{\frac{l}{g-a}}
\label{eq-78}
\end{equation}
This formula makes sense on condition that $a<g$. The closer the value of the acceleration $a$ is to $g$, the greater the period of vibration of the pendulum. At $a=g$, the state of weightlessness sets in. What will happen to the pendulum in this case?

\stda According to formula \eqref{eq-78}, the period becomes infinitely large. This must mean that the pendulum is stationary.

\tchr Let us clear up some details of your answer. We started out with the pendulum vibrating in the lift. All of a sudden, the lift breaks loose and begins falling freely downward (we neglect air resistance). What happens to the pendulum?

\stda As I said before, the pendulum stops.

\tchr Your answer is not quite correct. The pendulum will indeed be stationary (with respect to the lift, of course) if at the instant the lift broke loose the bob happened to be in one of its extreme positions. If at that instant the bob was not at an extreme position it will continue to rotate at the end of the string in a vertical plane at a uniform velocity equal to its velocity at the instant the accident happened.

\stda I understand now.

\tchr Then make a drawing illustrating the behaviour of a pendulum (a bob attached to a string) inside a spaceship which is in a state of weightlessness.

\stda In the spaceship, the bob at the end of the string will either be at rest (with respect to the spaceship), or will rotate in a circle whose radius is determined by the length of the string (if, of course, the walls or ceiling of the spaceship do not interfere).

\tchr Your picture is not quite complete. Assume that  we are inside a spaceship in a state of weightlessness. We take the bob and string and attach the free end of the string so that neither walls nor ceiling interfere with the motion of the bob. After this we carefully release the bob. The ball remains stationary. Here we distinguish two cases:
\begin{enumerate*}[label=(\arabic*)]
\item the string is loose, and 
\item the string is taut. 
\end{enumerate*}
\begin{marginfigure}
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-12/fig-051a.pdf}
\caption{Anaysing the motion of a pendulum in a spaceship.}
\label{fig-51}
\end{marginfigure}
Consider the first case (Position 1 in \figr{fig-51}~\drkgry{(a)}). We impart a certain velocity $v_{0}$ to the bob. As a result, the bob will travel in a straight line at uniform velocity until the string becomes taut (Position 2 in \figr{fig-51}~\drkgry{(a)}). At this instant, the reaction of the string will act on the bob in the same manner as the reaction of a wall acts on a ball bouncing off it. As a result, the direction of travel of the bob will change abruptly and it will then again travel at uniform velocity in a straight line (Position 3 in \figr{fig-51}~\drkgry{(a)}). In this peculiar form of ``reflection'' the rule of the equality of the angles of incidence and reflection should be valid. 

Now consider the second case: we first stretch the string taut and then carefully release the bob. As in the first case, the bob will remain stationary in the position it was released (Position 1 in \figr{fig-51}~\drkgry{(b)}). Then we impart a certain velocity  $v_{0}$ to the bob in a direction perpendicular to the string. As a result the bob begins to rotate in a circle at uniform velocity. The plane of rotation is determined by the string and the vector of the velocity imparted to the bob.
\begin{marginfigure}
\centering
\includegraphics[width=1.1\textwidth]{figs/ch-12/fig-052a.pdf}
\caption{Anaysing the motion of a pendulum moving with an accelerated frame at an angle to earth's gravity.}
\label{fig-52}
\end{marginfigure}
Let us consider the following problem. \hlred{A string of length $l$ with a bob at one end is attached to a truck which slides without friction down an inclined plane having an angle of inclination $\alpha$ \drkgry{(\figr{fig-52}~(a))}. We are to find the period of vibration of this pendulum located in a frame of reference which travels with a certain acceleration.} However, in contrast to the preceding problems with the lift, the acceleration of the system is at a certain angle to the acceleration of the earth's gravity. This poses an additional question: \hlred{what is the equilibrium direction of the pendulum string?}

\stda I once tried to analyse such a problem but became confused and couldn't solve it.

\tchr The period of vibration of a pendulum in this case is found by formula \eqref{eq-75} except that $g$ is to be replaced by a certain effective acceleration as in the case of the lift. This acceleration (we shall denote it by $g_{e\!f\!\!\!f}$) is equal to the vector sum of the acceleration of gravity and that of the given system. Another matter to be taken into account is that in the above mentioned sum, the acceleration vector of the truck should appear with the reversed sign, since the force of inertia is in the direction opposite to the acceleration of the system. The acceleration vectors are shown in \figr{fig-52}~\drkgry{(b)}, the acceleration of the truck being equal to $g \sin \alpha$. Next we find $g_{e\!f\!\!\!f}$
\begin{equation}%
\begin{split}
g_{e\!f\!\!\!f} & = \sqrt{g_{e\!f\!\!\!fx}^{2} + g_{e\!f\!\!\!fy}^{2}} \\
& = \sqrt{ (g \sin \alpha \cos \alpha)^{2} + (g-g sin^{2}\alpha)^{2} }\\
& = g \cos \alpha
\end{split}
\label{eq-79}
\end{equation}
from which
\begin{equation}%
T = 2 \pi \sqrt{\frac{l}{g \cos \alpha}}
\label{80}
\end{equation}

\stda How can we determine the equilibrium direction of the string?

\tchr It is the direction of the acceleration $g_{e\!f\!\!\!f}$. On the basis of equation \eqref{eq-79} it is easy to see that this direction makes an angle $\alpha$ with the vertical. In other words, in the equilibrium position, the string of a pendulum on a truck sliding down an inclined plane will be perpendicular to the plane.

\stdb Isn't it possible to obtain this last result in some other way?

\tchr We can reach the same conclusion directly by considering the equilibrium of the bob with respect to the truck. The forces applied to the bob are: its weight $mg$, the tension $T$ of the string and the force of inertia $ma$ (\figr{fig-53}). We denote the angle the string makes with the vertical by $\beta$.
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-12/fig-053a.pdf}
\caption{Anaysing the motion of a pendulum.}
\label{fig-53}
\end{marginfigure}
Next we resolve all these forces in the vertical and horizontal directions and then write the conditions of equilibrium for the force components in each of these directions. Thus
\begin{equation}%
\left.
\begin{aligned}
T \cos \beta +ma \sin \alpha & =mg \\
T \sin \beta & = ma \cos \alpha	
\end{aligned}
\right\}
\label{eq-81}
\end{equation}
Taking into consideration that $a=g \sin \alpha$, we rewrite the system of equations \eqref{eq-81} in the form
\begin{equation*}%
\left.
\begin{aligned}
T \cos \beta & = mg (1 - \sin^{2} \alpha) \\
T \sin \beta & = mg \sin \alpha \cos \alpha
\end{aligned}
\right\}
\end{equation*}
After dividing one equation by the other we obtain
\begin{equation*}%
\cot \beta = \cot \alpha
\end{equation*}
Thus, angles $\beta$ and $\alpha$ turn out to be equal. Consequently, the equilibrium direction of the pendulum string is perpendicular to the inclined plane.

\stdb I have followed your explanations very closely and come to the conclusion that I was not so wrong after all when, in answer to your question about the forces applied to a satellite, I indicated the force of gravity and the centrifugal force (see \chap{ch-08}).

Simply, my answer should be referred to the frame of reference attached to the satellite, and the centrifugal force is to be understood as being the force of inertia. In a non-inertial frame of reference attached to the satellite, we have a problem, not in dynamics, but in statics. It is a problem of the equilibrium of forces of which one is the centrifugal force of inertia.

\tchr Such an approach to the satellite problem is permissible. However, in referring to the centrifugal force in \chap{ch-08}, you did not consider it to be a force of inertia. You were
simply trying to think up something to keep the satellite from falling to the earth. Moreover, in the case you mention, there was no necessity for passing over to a frame of reference attached to the satellite: the physical essence of the problem was more clearly demonstrated without introducing a centrifugal force of inertia. My previous advice is still valid: \hlblue{if there is no special need, do not employ a non-inertial frame of reference.}

\end{dialogue}

%\section*{\textsc{\hlred{Problems}}}
%\label{problems-06}
%
%\begin{enumerate}[resume*=problems]
%\item A ball accomplishes harmonic vibrations as shown in \figr{fig-48}. Find the ratio of the velocities of the ball at points whose distances from the equilibrium position equal one half and one third of the amplitude.
%
%\item A bob suspended on a string is deflected from the equilibrium position by an angle of \ang{60} and is then released. Find the ratio of the tensions of the string for the equilibrium position and for the maximum deviation of the bob.
%
%\item A pendulum in the form of a ball (bob) on a thin string is deflected through an angle of \ang{5}. Find the velocity of the bob at the instant it passes the equilibrium position if the circular frequency of vibration of the pendulum equals \SI{2}{\per\second}.
%\end{enumerate}
%
%\cleardoublepage
%\thispagestyle{empty}
%\vspace*{2cm}
%
%\begin{figure*}
%\centering
%\includegraphics[width=0.65\linewidth]{sec-f.pdf}
%%\caption{Velocity \emph{vs} time graph for the function shown in Figure \ref{fig-05}.}
%%\label{fig-06}
%\end{figure*}
%%\addtocounter{figure}{1}
%% For incrementing the figure numbers for figures without caption or numbering
%%\begin{fullwidth}
%\begin{Large}
%
%\end{Large}
%%\end{fullwidth}

\cleardoublepage

\thispagestyle{empty}
\vspace*{-1.5cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.75\textwidth]{figs/sec/tarasov-qasp-sec-07.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{The laws of statics are laws of equilibrium. Study these laws carefully. Do not forget that they are of immense practical importance. A builder without some knowledge of the basic laws of statics is inconceivable. We shall consider examples illustrating the rules for the resolution of forces. The subsequent discussion concerns the conditions of equilibrium of bodies, which are used, in particular, for locating the centre of gravity.
}}}
\end{fullwidth}
