% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{How Do You Construct Images Formed By Mirrors And Lenses?}
\label{ch-32}

\begin{dialogue}

\tchr Quite often we find that examinees are incapable of constructing images formed by various optical systems, such as lenses and plane and spherical mirrors. Let us consider some typical examples. \hlred{Construct the image of a man formed in the plane mirror show in \figr{fig-132}.}

\begin{figure}%
\includegraphics[width=.7\textwidth]{figs/ch-32/fig-132a}
\caption{Construct the image of a man formed in the the plane mirror.}
\label{fig-132}
\end{figure}

\stda It seems to me that no image will be formed by the mirror in this case because the mirror is located too high above the man.

\tchr You are mistaken. There will be an image in the mirror. Its construction is given in \figr{fig-132}~\drkgry{(b)}. It is quite evident that to construct the image it is sufficient to prolong the line representing the surface of the mirror  and to draw an image symmetrical to the figure of man with respect to this line (surface of the mirror). 

\stda Yes, I understand, but will the man see his image.

\tchr That is another question. As a matter of fact, the man will not see his image, because the mirror is located too high above him and is inconveniently inclined. The image of the man will be visible in the given mirror only to an observer located within the angle formed by rays $AA_{1}$ and $BB_{1}$. It is appropriate to recall that the observer's eye receives a beam of diverging rays from the object being observed. The eye will see an image of the object at the point of intersection of these rays or of their extensions. (see \figr{fig-129} and \figr{fig-132}~\drkgry{(b)}).

\hlred{Consider the construction of the image formed by a system of two plane mirrors arranged perpendicular to each other \figr{fig-133}~\drkgry{(a)}.}
\begin{figure}%
\centering
\includegraphics[width=.7\linewidth,angle=-3]{figs/ch-32/fig-133a}
\caption{Construct the images in the system of two plane mirror.}
\label{fig-133}
\end{figure}
\stda We simply represent the reflection of the object in the two planes of the mirrors. Thus we obtain two images as shown in \figr{fig-133}~\drkgry{(b)}.

\tchr You have lost the third image. Note that the rays from the object that are within the right angle $AOB$ (\figr{fig-133}~\drkgry{(c)}) are reflected twice: first from one mirror and then from the other. The paths of these two rays are illustrated in \figr{fig-133}~\drkgry{(c)}. The intersection of the extensions of these rays determines the third image of the object.

Next, we shall consider a number of examples involving a converging lens. Construct the image formed by such a lens in the case illustrated in \figr{fig-134}~\drkgry{(a)}.

\stda Thats very simple. My construction is shown in \figr{fig-134}~\drkgry{(b)}.

\begin{figure*}%
\includegraphics[width=0.8\textwidth]{figs/ch-32/fig-134a}
\caption{Examples involving a converging lens.}
\label{fig-134}
\end{figure*}

\tchr Good. Now assume that one half of the lens is closed by an opaque screen as shown in \figr{fig-134}~\drkgry{(c)}. What will happen to the image?

\stda In this case, the image will disappear.

\tchr You are mistaken. You forget that the images of any point of the arrow (for example, its head) is obtained as a result of the intersection of an infinitely large number of rays (\figr{fig-134}~\drkgry{(d)}). We usually restrict ourselves to two rays because the paths of two rays are sufficient to find the position and size of the image by construction. In the given case the screen shuts off part of the rays falling on the lens. The other part of the rays, however, pass through the lens and form an image of the object (\figr{fig-134}~\drkgry{(e)}). Since fewer rays participate in forming the image, it will not be so bright as before.

\stdb From your explanation it follows that when we close part of the lens with an opaque screen, only the brightness of the image is changed and nothing else. However, anybody who has anything to do with photography knows that when you reduce the aperture opening of the camera by irising, i.e. you reduce the effective area of the lens, another effect is observed along with the reduction in the brightness of the image: the image becomes sharper, or more clear-cut. Why does this happen?

\tchr This is a very appropriate question. It enables me to emphasize the following: all our constructions are based on the assumption that we can neglect defects in the optical system (a lens in our case). True, the word ``defects'' is hardly suitable here since it does not concern any accidental shortcomings of the lens, but its basic properties. It is known that if two rays, parallel to and differently spaced from principal optical axis, pass through a lens, they will, after refraction in the lens, intersect the principal optical axis strictly speaking, at different points (\figr{fig-135}~\drkgry{(a)}). This means that the focal point of the lens (the point of intersection of all rays parallel to the principal optical axis), or its focus, will be blurred; a sharply defined image of the object cannot be formed. The greater the differences in the distances of the various rays from the principal axis,  the more blurred the image will be. When the aperture opening is reduced by irising, the lens passes a narrow bundle of rays. This improves the sharpness to some extent (\figr{fig-135}~\drkgry{(b)}).
\begin{figure}%
\centering
\includegraphics[width=\textwidth]{figs/ch-32/fig-135a}
\caption{Image construction by a lens.}
\label{fig-135}
\end{figure}

\stdb Thus, by using the diaphragm we make the image more sharply defined at the expense of brightness.

\tchr Exactly. Remember, however, that in constructing the image formed by lenses, examinees have every reason to assume that parallel rays will always intersect at a single point. This point lies on the principal optical axis if the bundle of parallel rays is directed along this axis; the point lies on the focal plane if the bundle of parallel rays is directed at some angle to the principal optical axis. It is important however, for the examinees to understand this treatment is only approximate and that a more accurate approach would require corrections for the defects of the optical systems.

\stda What is the focal plane of a lens?

\tchr It is a plane through the principal focus of the lens perpendicular to the principal optical axis. Now, what is the difference between images formed by a plane mirror and by a converging lens in the example of \figr{fig-134}?

\stda In the first case (with the mirror) the image is virtual, and in the second it is real.

\tchr Correct. Please explain the differences between the virtual and real images in more detail.

\stdb A virtual image is formed by intersection, not of the rays themselves, but of their extensions. No wonder then that a virtual image can be seen somewhere behind a wall, where the rays cannot penetrate.

\tchr Quite right. Note also that a virtual image can be observed only form definite positions. In case of a real image you can place a screen where the image is located and observe the image from any position. Consider the example illustrated in \figr{fig-136}~\drkgry{(a)}.

\hlred{Determine, by construction, the direction of ray $AA_{1}$ after it passes through a converging lens if the path of another ray ($BB_{1}B_{2}$ in \figr{fig-136}~\drkgry{(a)}) through this lens is known.}

\begin{figure}[h]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-32/fig-136a}
\caption{Construct the images in the system of two plane mirror.}
\label{fig-136}
\end{figure}

\stda But we don't know the focal length of the lens. 

\tchr Well, we do know the path of the other ray before and after the lens.

\stda We didn't study such constructions at school.

\stdb I think that we should first find the focal length of the lens. For this purpose we can draw a vertical arrow somewhere to the left of the lens so that its head touches ray $BB_{1}$. We shall denote the point of the arrowhead by the letter $C$ (\figr{fig-136}~\drkgry{(b)}). Then we pass a ray from point $C$ through the center of the lens. This ray will go through the lens without being refracted and, at a certain point E, will intersect ray $B_{1}B_{2}$. Point $E$ is evidently the image of the point of the arrowhead. It remains to draw a third ray form the arrowhead $C$ parallel to the principal optical axis of the lens. Upon being refracted, this last ray will pass through the image of the arrowhead, i.e. through point $E$. The point of intersection of this third ray with the principal axis is the required focus of the lens. This construction is given in \figr{fig-136}~\drkgry{(b)}. 

The focal length being known, we can now construct the path of ray $AA_{1}$ after it is refracted by the lens. This is done by drawing another vertical arrow with the point of its head lying on the ray $AA_{1}$ (\figr{fig-136}~\drkgry{(c)}). Making use of the determined focal length, we can construct the image of the second arrow. The required ray will pass through point $A_{1}$ and the head of the image of the arrow. This construction is shown in \figr{fig-136}~\drkgry{(c)}.

\tchr Your arguments are quite correct. They are based on finding image of a certain auxiliary object (the arrow). Note that this method is convenient when you are asked to determine the position of the image of a luminous point lying on the principal axis of the lens. In this case it is convenient to erect an arrow at the luminous point and construct the image of the arrow. It is clear that the tail of the image of the arrow is the required image of the luminous point.

This method, however, is too cumbersome for our example. I shall demonstrate a simpler construction. To find the focal length of the lens, we can draw ray $EO$ through the centre of the lens and parallel to the ray $BB_{1}$ (\figr{fig-136}~\drkgry{(d)}). Since these two rays are parallel, they intersect in the focal plane behind the lens (the cross section of the focal plane is shown in \figr{fig-136}~\drkgry{(d)} by a dashed line). Then we draw ray $CO$ through the centre of the lens and parallel to ray $AA_{1}$. Since these two parallel rays should also intersect in the focal plane after passing through the lens, we can determine the direction of the ray $AA_{1}$ through the lens, we can determine the direction of ray $AA_{1}$ after passing through the lens. As you can see, the construction is much simpler. 

\stdb Yes, your method is appreciable simpler. 

\tchr Try to apply this method to a similar problem in which a diverging lens is used instead of converging one (\figr{fig-137}(~\drkgry{a)}).
\begin{figure}%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-32/fig-137a}
\caption{Construct the images with a diverging lens.}
\label{fig-137}
\end{figure}
\stdb First I will draw a ray through the centre of lens parallel to ray $BB_{1}$. In contrast to the preceding problem, the extension of the rays and not the rays themselves, will intersect (we may note for a ray passing through the centre, the extension will coincide with the ray itself). As a result, the focal plane, containing the point of intersection, will now be to the left of the lens instead of the right (see the dashed line in \figr{fig-137}~\drkgry{(b)}).

\tchr(intervening): Note that the image is always virtual in diverging lenses.

\stdb (continuing): Next I shall pass a ray through the centre of the lens and parallel to ray $AA_{1}$. Proceeding from the condition that the extensions of these rays intersect in the focal plane, I can draw the required ray.  

\tchr Good. Now tell me, where is the image of an object a part of which is in front of the focus of a converging lens, and the other part behind the focus (the object is of finite width)?

\stdb I shall construct the images of several points of the object located at various distances from the lens. The points located beyond the focus will provide a real image (it will be to the right of the lens), while the points in front of the focus will yield a virtual image (it will be to the left of the lens). As the chosen points approach the focus, the images will move away to infinity (either to the left or to the right of the lens).

\tchr Excellent. Thus, in our case the image of the object is made up of two pieces (to the left and right of the lens). Each piece begins at a certain finite distance from the lens and extends to infinity. As you see, the question ``Can an object have a real and virtual image simultaneously?'' should be answered in affirmative. 

I see that you understand the procedure for constructing the images formed by the lens. Therefore, we can go over to a more complicated item, the construction of an image formed by the system of two lenses. Consider the following problem:

 \hlred{we have two converging lenses with a common principal optical axis and different focal lengths. Construct the image of a vertical arrow formed by such an optical system \drkgry{(\figr{fig-138}~(a))}. The focuses of one lens are shown on the diagram by $\times$'s and those of the other, by blacked in circles $\bullet$.}

\begin{figure}%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-32/fig-138a}
\caption{Construct the images formed in the optical system.}
\label{fig-138}
\end{figure}
\stdb To construct the image of the arrow formed by two lenses, we must first construct the image formed by the first lens. In doing this we can disregard the second lens. Then we treat this image as if it were an object and, disregarding the first lens, construct its image formed by the second lens.

\tchr Here you are making a very typical error. I have heard such an answer many times. It is quite wrong.

Let us consider two rays originating at the point of arrowhead, and follow out their paths through the given system of lenses (\figr{fig-138}~\drkgry{(b)}). The paths of the rays after they pass through the first lens are easily traced. To find their paths after the second lens, we shall draw auxiliary rays parallel to our rays and passing through the centre of the second lens. In this case, we make use of principle discussed in the preceding problems (parallel rays passing through a lens should intersect in the focal plane). The required image of the point of the arrow head will be at the point of intersection of the two initial rays after they leave the second lens. This construction is shown in detail in \figr{fig-138}~\drkgry{(b)}. Now, let us see the result we would have obtained if we would have accepted your proposal. The construction is carried out in \figr{fig-138}~\drkgry{(c)}. Solid lines show the construction of the image formed by the first lens; dashed lines show the subsequent construction of the image formed by the second lens. You can see that the result would have been entirely different (and quite incorrect!). 

\stdb But I am sure we once constructed an image exactly as I indicated.

\tchr You may have done so. The fact is that in certain cases your  method of construction may turn out to be valid because it leads to results which coincide with those obtained by my method. This can be demonstrated on the preceding example by moving the arrow closer to the first lens i.e. between the focus and the lens. \figr{fig-139}~\drkgry{(a)} shows the construction according to my method, and \figr{fig-139}~\drkgry{(b)}, according to yours. As you see, in the given case the results coincide.

\begin{figure}%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-32/fig-139a}
\caption{Construct the images in the system of two plane mirror.}
\label{fig-139}
\end{figure}
\stdb But how can I determine beforehand in what cases my method of constructing the image can be used?

\tchr It would not be difficult to specify the conditions for the applicability of your method for two lenses. These conditions become much more complicated for a greater number of lenses. There is no need to discuss them at all. Use my method and you won't get into any trouble. But I wish to ask one more question: can a double concave lens be a converging one?

\stdb Under ordinary conditions a double-concave lens is a diverging one. However, it will become a converging lens if it is placed in a medium with a higher index of refraction than that of the lens material. Under the same conditions, a double-convex lens will be a diverging one.


\end{dialogue}


\cleardoublepage