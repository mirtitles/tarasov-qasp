% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{Can you compute the resistance of a branched portion of a circuit?}
\label{ch-29}

\begin{dialogue}

\tchr \hlred{Compute the resistance of the portion of the circuit shown in \drkgry{\figr{fig-117}~(a)}. You can neglect the resistance of wires (leads).}

\begin{figure}%[6cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-29/fig-117a}
\caption{ At what angle to the plates will the electron fly out of the capacitor.}
\label{fig-117}
\end{figure}

\stda If the resistance of the wires can be neglected, then the leads can be completely disregarded. The required resistance equals $3R$.

\tchr You answered without thinking. To neglect the resistance of the wire and to neglect the leads are two entirely different things (though many examinees suppose them to be the same). To throw a lead out of the circuit means to replace it with an infinitely high resistance. Here on the contrary the resistance of the leads equals zero.

\stda Yes, of course, I simply didn't give it any thought. But now I shall reason in the following manner. At point $A$ the current will be divided into two currents whose directions I have shown in \figr{fig-117}~\drkgry{(b)} by arrows. Here the middle resistor can be completely disregarded and the total resistance is $R/2$.

\tchr Wrong again! I advise you to use the following rule: find the points in the circuit with the same potential and then change the diagram so that those points coincide with one another. The currents in the various branches of the circuit will remain unchanged, but the diagram maybe substantially simplified. I have already spoken about this in \chap{ch-27}. Since in the given problem the resistances of the leads equal zero, points $A$ and $A_1$ have the same potential. Similarly points $B$ and $B_1$ have the same potential. In accordance with the rule I mentioned, we shall change the diagram so that points with the same potential will finally coincide with one another. For this purpose, we shall gradually shorten the lengths of the leads. The consecutive stages of this operation are illustrated in \figr{fig-117}~\drkgry{(c)}. As a result we find that the given connection corresponds to an arrangement with three resistors connected in parallel. Hence, the total resistance of the portion is $R/3$. 

\stda Yes, indeed. It is quite evident from \figr{fig-117}~\drkgry{(c)} that the resistors are connected in parallel.

\tchr Let us consider the following example. \hlred{ We have a cube made up of leads, each having resistance $R$ \drkgry{\figr{fig-118}~(a)}. The cube is connected into a circuit as shown in the diagram. Compute the total resistance of the cube.}

We can start by applying the rule I mentioned above. Indicate the points having the same potential.

\stda I think that the three points $A, A_{1} \,\, \text{and}\,\, A_{2}$ will have the same potential (see \figr{fig-118}~\drkgry{(a)}) since the three edges of the cube ($DA, DA_{1} \,\, \text{and}\,\, DA_{2}$) are equivalent in all respects.

\begin{figure}[h]%[6cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-29/fig-118a}
\caption{ What is the total resistance of the cube?}
\label{fig-118}
\end{figure}

\tchr Yes, and so are edges $BC, BC_{1} \,\, \text{and} \,\, BC_{2}$. Therefore, points  $C, C_{1} \,\, \text{and}\,\, C_{2}$will have the same potential. Next, let us tear apart our wire cube at the indicated points and, after bending the edge wires, connect them together' again so that points with the same potential coincide with one another. What will the diagram look like now?

\stda We shall obtain the diagram shown in \figr{fig-118}~\drkgry{(b)}.

\tchr Exactly. The diagram obtained in \figr{fig-118}~\drkgry{(b)} is equivalent to the initial diagram (with the cube) but is appreciably simpler. Now you should have no difficulty in computing the required total resistance.

\stda It equals
\begin{equation*}%
\frac{1}{3}R + \frac{1}{6}R + \frac{1}{3}R = \frac{5}{6}R
\end{equation*}

\stdb \hlred{How would you find the total resistance of a wire figure in the form of a square with diagonals, connected into a circuit as shown in \drkgry{\figr{fig-119}~(a)}?}

\begin{figure}[h]%[6cm]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-29/fig-119a}
\caption{ What is the total resistance of the cube?}
\label{fig-119}
\end{figure}

\tchr Again we must search for points with the same potential. In the given case we readily see that the diagram has an axis of symmetry which I shall indicate in \figr{fig-119}~\drkgry{(a)} as a dashed line. It is clear that all points lying on the axis of symmetry should have the same potential which is equal to one half the sum of the potentials of points $A$ and $D$. Thus the potentials of points $O, O_{1} \,\, \text{and} \,\, O_{2}$ are equal to one another. According to the rule, we can make these three points coincide with one another. As a result, the combination of resistances is broken down into two identical portions connected in series. One of these is shown in \figr{fig-119}~\drkgry{(b)}. It is not difficult to compute the resistance of this portion. If each of the wires, or leads, in the square has the same resistance $R$, then the total resistance of the portion is $(4/5) R$. Thus the required total resistance of the square equals $(8/5) R$.

\stda Do you mean to say that the main rule is to find points on the diagram with the same potential and to simplify the diagram by making these points coincide?

\tchr Exactly. In conclusion, I wish to propose an example with an infinite portion. \hlred{We are given a circuit made up of an infinite number of repeated sections with the resistors $R_{1}$ and $R_{2}$ \drkgry{(\figr{fig-120}~(a))}. Find the total resistance between points $A$ and $B$.}

\begin{figure}%[6cm]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-29/fig-120a}
\caption{ What is the total resistance of the given configuration?}
\label{fig-120}
\end{figure}

\stda Maybe we should make use of the method of mathematical induction? First we will consider one section, then two sections, then three, and so on. Finally we shall try to extend the result to $n$ sections for the case when $n \to \infty$.

\tchr No, we don't need the method of mathematical induction here. We can begin with the fact that infinity will not change if we remove one element from it. We shall cut the first section away from the diagram (along $R$ the dashed line shown in \figr{fig-120}~\drkgry{(a)}). Evidently, an infinite number of sections will still remain and so the resistance between points $C$ and $D$ should be equal to the required total resistance $R$. Thus the initial diagram can be changed to the one shown in \figr{fig-120}~\drkgry{(b)}. The portion of the circuit shown in \figr{fig-120}~\drkgry{(b)} has a total resistance of 
\begin{equation*}%
R_{1}+\frac{RR_{2}}{(R+R_{2})} 
\end{equation*}
Since this portion is equivalent to the initial portion of the circuit, its resistance should equal the required resistance $R$. Thus we obtain
\begin{equation*}%
R= R_{1} + \frac{RR_{2}}{(R+R_{2})} 
\end{equation*}
i.e. a quadratic equation with respect to $R$:
\begin{equation*}%
R^{2} - RR_{1} - R_{1}R_{2} = 0
\end{equation*}
Solving this equation we obtain
\begin{equation}
R = \frac{R_{1}}{2} \left( 1 + \sqrt{1 + 4 \dfrac{R_{2}}{R_{1}}}  \right)
\label{179}
\end{equation}

\stda Well, that certainly is an interesting method of solving the problem.

\end{dialogue}

\section*{Problems}
\label{prob-29}
\addcontentsline{toc}{section}{Problems}


\begin{enumerate}[resume=problems]
\item In the electrical circuit shown in \figr{fig-121}, $\Ea=\SI{4}{\volt},\,\,  r= \SI{1}{\ohm} \,\, \text{and} \,\, R=\SI{45}{\ohm}$. Determine the readings of the voltmeter and ammeter.

\begin{figure}[h]%[-8cm]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-29/fig-121a}
\caption{ What are the readings of the voltmeter and ammeter?}
\label{fig-121}
\end{figure}

\item Find the total resistance of the square shown in \figr{fig-119}~\drkgry{(a)} assuming that it is connected into the circuit at points $A$ and $C$.
\item A regular hexagon with diagonals is made of wire. The resistance of each lead is equal to $R$. The hexagon is connected into the circuit as shown in \figr{fig-122}~\drkgry{(a)}. Find the total resistance of the hexagon.


\item Find the total resistance of the hexagon of Problem 65 assuming that it is connected into the circuit as shown in \figr{fig-122}~\drkgry{(b)}.
\item Compute the total resistance of the hexagon given in Problem 65 assuming that it is connected into the circuit as shown in \figr{fig-122}~\drkgry{(c)}.
\begin{marginfigure}%[-2cm]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-29/fig-122a}
\caption{ What is the total resistance?}
\label{fig-122}
\end{marginfigure}

\end{enumerate}






\cleardoublepage
