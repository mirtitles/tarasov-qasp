% !TEX root = tarasov-qasp-2.tex
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode


\chapter{Do You Know Ohm's Law?}
\label{ch-27}

\begin{dialogue}

\tchr Do you know Ohm's law?

\stda Yes, of course. I think everybody knows Ohm's law. This is probably the simplest question in the entire physics course.

\tchr We shall see. A Portion of electric circuit is shown in  \figr{fig-104}~\drkgry{(a)}. Here $\Ea$ is the electromotive force (\emph{emf}) and it is directed to the right; $R_{1}$ and $R_{2}$ are resistors; $r$ is the internal resistance of the seat of the electromotive force; and $\varphi_{A}$ and $\varphi_{B}$ are the potentials at the ends of the given portion of the circuit. The current flows from left to right. Find the value $I$ of this current. 
\begin{figure} %[-5cm]%
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-27/fig-104a}
\caption{Find the value of the current in the circuit.}
\label{fig-104}
\end{figure}

\stda But you have an open circuit!

\tchr I proposed that you consider a portion of some large circuit. You know nothing about the rest of the circuit. Nor do you need to, since the potentials at the end of this portion are given.

\stda Previously we only dealt with closed electric circuits. For them the Ohm's law can be written in the form:
\begin{equation}%
I = \frac{\Ea}{R + r} 
\label{eq-164}
\end{equation}
\tchr You are mistaken. You also considered elements of the circuit. According to Ohm's law, the current in an element of a circuit is equal to the ratio of the voltage and the resistance.

\stda But is this a circuit element?

\tchr Certainly. One such element is illustrated in \figr{fig-104}~\drkgry{(b)}. For this element you can write Ohm's in the form
\begin{equation}%
I = \frac{\varphi_{A} - \varphi_{B}}{R} 
\label{eq-165}
\end{equation}
Instead of potential difference $(\varphi_{A} - \varphi_{B})$ between the ends of the element, you previously employed the simpler term ``voltage'', denoting it by the letter $V$.

\stda In any case, we did not deal with an element of circuit of the form shown in \figr{fig-104}~\drkgry{(a)}.

\tchr Thus, we find that you know Ohm's law for the special cases of a closed circuit and for the simplest kind of element which includes no emf. You do not, however, know Ohm's law for the general case. Let us look into this together.

\figr{fig-105}~\drkgry{(a)} shows the change in potential along a given portion in the circuit. The current flows form left to right and therefore the potential drops from $A$ to $C$. The drop in potential across the resistor $R_{1}$ is equal to $IR_{1}$. Further, we assume that the plates of a galvanic cell are located at $C$ and $D$. At these points upwards potential jumps occur, the sum of the jumps is the emf equal to $\Ea$. Between $C$ and $D$ the potential drops across the internal resistance of the cell; the drop on potential across the resistor $R_{2}$ equals $IR_{2}$. The sum of the drops across all the resistances of the portion minus the upward potential jump is equal to $V$. It is the potential difference between the ends of the portion being considered. Thus
\begin{equation*}%
I(R_{1}+R_{2}+r) - \Ea=  \varphi_{A} - \varphi_{B} 
\end{equation*}
\begin{figure}[h]%[-5cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-27/fig-105a}
\caption{Find the value of the current in the circuit.}
\label{fig-105}
\end{figure}

From this we obtain the expression for the current, i.e. Ohm's law for the given portion of the circuit 
\begin{equation}%
I = \frac{\Ea + (\varphi_{A} - \varphi_{B})}{R_{1}+R_{2}+r} 
\label{eq-166}
\end{equation}
Note that from this last equation we can readily obtain the special cases familiar to you. For the simplest element containing no emf we substitute $\Ea=0$ and $r=0$ into equation \eqref{eq-166}). Then 
\begin{equation*}%
I = \frac{\varphi_{A} - \varphi_{B}}{R_{1}+R_{2}}
\end{equation*}
which corresponds to equation \eqref{eq-165}. To obtain a closed circuit, we must connect the ends $A$ and $B$ of our portion. This means that  $\phi_{A} = \phi_{B}$. Then 
\begin{equation*}
I = \frac{\Ea}{(R_{1}+R_{2}+r)}
\end{equation*}
This corresponds to equation \eqref{eq-164}.

\stda I see now that I really didn't know Ohm's law. 

\tchr To be more exact, you knew it for special cases only. Assume that a voltmeter is connected to the terminals of the cell in the portion of circuit shown in \figr{fig-104}~\drkgry{(a)}. Assume also that the voltmeter has sufficiently high resistance so that we can disregard the distortions due to its introduction into the circuit. What will the voltmeter indicate?

\stda I know that a voltmeter connected to the terminals of a cell should indicate the voltage drop across the external circuit. In the given case, however we know nothing about the external circuit.

\tchr A knowledge of external circuit is not necessary for our purpose. If the voltmeter is connected to points $C$ and $D$, it will indicate the difference in potential between these points. You understand this, don't you?

\stda Yes, of course.

\tchr Now look at \figr{fig-105}~\drkgry{(a)}. It is evident that the difference in potential between points $C$ and $D$ equals $(\Ea = Ir)$. Denoting the voltmeter reading by $V$, we obtain the formula 
\begin{equation}%
V = \Ea - Ir 
\label{eq-167}
\end{equation}
I would advise you to use this very formula since it requires no knowledge of any external resistances. This is specially valuable in cases when you deal with a more or less complicated circuit. Note that equation \eqref{eq-167} lies at the basis of a well known rule: If the circuit is broken and no current flows $I = 0$, then $V =\Ea$. Here the voltmeter reading coincides with the value of the emf. 

Do you understand all this?

\stda Yes, now it is clear to me?

\tchr  As a check I shall ask you a question which examinees quite frequently find it difficult to answer. \hlred{ A closed circuit consists of $n$ cells connected in series. Each element has an emf $\Ea$ and internal resistance $r$. The resistance of the connecting wires is assumed to be zero. What will be the reading in the voltmeter connected to the terminals of one of the cells. As usual it is assumed that no current passes through the voltmeter.}

\stda I shall reason as in the preceding explanation. The voltmeter reading will be $V = \Ea -Ir$. From Ohm's law for the given circuit we can find the current 
\begin{equation*}
I = \frac{n\Ea}{nr} = \frac{\Ea}{r} 
\end{equation*}
Substituting this in the first equation we obtain 
\begin{equation*}
V = \Ea - \left( \frac{\Ea}{r} \right)r = 0
\end{equation*}
Thus, in this case the voltmeter will read zero.

\tchr Absolutely correct. Only please remember that this case was idealized. On one hand, we neglected the resistance of the connecting wires, and on the other we assumed the resistance of the voltmeter to be infinitely large, so don't try to check this result by experiment.

Now let us consider a case when the current in a portion of  a circuit flows in one direction and the emf acts in the opposite direction. This is illustrated in \figr{fig-104}~\drkgry{(c)}. Draw a diagram showing the change of potential along this portion.

\stda Is it possible for the current to flow against the emf?

\tchr You forget that we have here only the portion of a circuit. The circuit may contain other emf's outside the portion being considered, under whose effect the current in this portion may flow against the given emf.

\stda I see. Since the current flows from the left to right, there is a potential drop equal to $IR_{1}$ from $A$ to $C$. Since the emf is now in the opposite direction, the potential jumps at the points $C$ and $D$ should now reduce the potential instead of increasing it. From point $C$ to point $D$ the potential should drop by the amount $Ir$, and from point $D$ to point $B$, by $IR_{2}$. As a result we obtain the diagram of \figr{fig-105}~\drkgry{(b)}.

\tchr And what form will Ohm's law take in this case?

\stda It will be of the form
\begin{equation}%
I = \frac{ (\varphi_{A} - \varphi_{B}) - \Ea}{R_{1}+R_{2}+r} 
\label{eq-168}
\end{equation}

\tchr Correct. And what will voltmeter indicate now?

\stda It can be seen from \figr{fig-105}~\drkgry{(b)} that in this case
\begin{equation}%
V = \Ea + Ir
\end{equation}

\tchr Exactly. Now consider the following problem. \hlred{In the electrical circuit illustrated in \figr{fig-106}, $r=\SI{1}{\ohm}$, $R = \SI{10}{\ohm}$ and the resistance of the voltmeter $R_{v} = \SI{200}{\ohm}$. Compute the relative error of the voltmeter reading obtained assuming that the voltmeter has infinitely high resistance and consequently causes no distortion in the circuit.}

\begin{marginfigure}%[-5cm]%
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-27/fig-106a}
\caption{Find the value of the current in the circuit.}
\label{fig-106}
\end{marginfigure}

We shall denote the reading of the real voltmeter by $V$ and that of the voltmeter with infinite resistance by $V_{\infty}$. Then the relative error would be
\begin{equation}
f  = \frac{V_{\infty} - V}{V_{\infty}} = 1 - \frac{V}{V_{\infty}} 
\label{eq-170}
\end{equation}
Further, we shall take into consideration that
\begin{equation}
V_{\infty} = \left( \frac{\Ea}{R+r} \right) \,R 
\label{eq-171}
\end{equation}
and 
\begin{equation}
V = \frac{\Ea}{r + \left( \dfrac{RR_{V}}{R + R_{V}} \right)} \left( \frac{RR_{V}}{R+R_{V}} \right)
\label{eq-172}
\end{equation}
After substituting equations \eqref{eq-171} and \ref{eq-172} into \eqref{eq-170} we obtain:
\begin{align*}
f & = 1 - \frac{R_{V} (R + r)}{(R + R_{V})r + RR_{V}} \\
& = 1 - \frac{R_{V} (R + r)}{(r+R)R_{V} + rR} \\
&= 1 - \frac{1}{1+ \left( \dfrac{rR}{(r+R)R_{V}} \right)} 
\end{align*}
Since $R_{V}\gg R$ and $R> r$, the fraction in the denominator of the last equation is much less than unity. Therefore, we can make use of an approximation formula which is always useful to bear in mind
\begin{equation}
(1+\lambda)^{\alpha} \cong 1 + \alpha \lambda 
\label{eq-173}
\end{equation}
This formula holds true at $\lambda \ll 1$ for any value of $\alpha$ (whole or fractional, positive or negative). Employing approximation formula \eqref{eq-173} with
\begin{equation*}
\alpha = -1 \,\ \text{and} \,\, \lambda = \frac{rR}{(r+R)R_{V}}
\end{equation*}
we obtain
\begin{equation}
f \cong \frac{rR}{(r+R)R_{V}}
\label{eq-174}
\end{equation}
Substituting the given numerical values into equation \eqref{eq-174}, we find that the error is $f \approx \frac{1}{220} = 0.0045$.

\stda Does this mean that higher resistance of the voltmeter in comparison with the external resistance, the lower the relative error, and that the more reason we have to neglect the distortion of the circuit when the voltmeter is connected into it?

\tchr Yes, that's so. Only keep in mind that  $R_{V} \gg R$ is a sufficient, but not necessary condition for the smallness of the error $f$. It is evident from equation \eqref{eq-174} that error $f$ is small when the condition  $R_{V}\gg r$ is complied with, i.e. the resistance of the voltmeter is much higher than the internal resistance of the current source. The external resistance in this case maybe be infinitely high. 

Try to solve the following problem:
 \hlred{In the electrical circuit shown in \figr{fig-107}~\drkgry{(a)}, $E= \SI{6}{\volt}, \, r =\SI{2/3}{\ohm}, \, R = \SI{2}{\ohm}$. Compute the voltmeter reading. } 

\begin{figure}%[-5cm]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-27/fig-107a}
\caption{Find the value of the voltmeter reading.}
\label{fig-107}
\end{figure}

\stda Can we assume that the resistance of the voltmeter is infinitely high?

\tchr Yes, and the more so because this resistance is not specified in the problem.

\stda But then, will the current flow through the resistors in the middle of the circuit? It will probably flow directly along the elements $A_{1}A_{2}$ and $B_{1}B_{2}$.

\tchr You are mistaken. Before dealing with the currents, I would advise you simplify the diagram somewhat. Since the elements $A_{1}A_{2}$ and $B_{1}B_{2}$ have no resistance, it follows that $\varphi_{A1} = \varphi_{A2}$ and $\varphi_{B1} = \varphi_{B2}$. Next, we can make use of the rule: if in a circuit any two points have the same potential, without changing the currents through the resistors. Let us apply this rule to our case by making point $A_{1}$ coincide with point $A_{2}$, point $B_{1}$ with $B_{2}$. We then obtain the diagram shown in \figr{fig-107}~\drkgry{(b)}. This one is quite easy to handle. Therefore, I will give you the final answer directly: the voltmeter reading will be \SI{4}{\volt}. I shall leave the necessary calculations to you as a home assignment.

\end{dialogue}

\section*{Problems}
\label{prob-27}
\addcontentsline{toc}{section}{Problems}

\begin{enumerate}[resume=problems]
\item An ammeter, connected into a branch of the circuit shown in \emph{Figure \ref{fig-108}}, has a reading of \SI{0.5}{\ampere}. Find the current through resistor $R_{4}$ if the resistances are: $R_{1} = \SI{2}{\ohm}, \,\,R_{2}= \SI{4}{\ohm}, \,\,R_{3}= \SI{1}{\ohm}, \,\,R_{4}= \SI{2}{\ohm}\,\, \text{and} \,\, R_{5}= \SI{1}{\ohm}$.
\begin{figure}[h]%[-2cm]%
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-27/fig-108a}
\caption{Find the value of the current through resistor $R_{4}$.}
\label{fig-108}
\end{figure}

\item In the electric circuit shown in \figr{fig-109}, $\Ea=\SI{4}{\volt}, r= \SI{1}{\ohm} \,\, \text{and} \,\, R= \SI{2}{\ohm}$. Find the reading of the ammeter.
\begin{figure}%[-5cm]%
\centering
\includegraphics[width=0.35\textwidth]{figs/ch-27/fig-109a}
\caption{Find the reading of the ammeter.}
\label{fig-109}
\end{figure}

\item The resistance of a galvanometer equals \SI{0.2}{\ohm}. Connected in parallel to the galvanometer is a shunt with a resistance of \SI{0.05}{\ohm}. What resistance should be connected in series with this combination to make the total resistance equal to that of the galvanometer?
\item A voltmeter with a resistance of \SI{100}{\ohm} is connected to the terminals of a cell with an emf of \SI{10}{\volt} and internal resistance of \SI{1}{\ohm}. Determine the reading of the voltmeter and compute the relative error of its reading assuming that its resistance is infinitely high.
\item An ammeter with a resistance of \SI{1}{\ohm} is connected into a circuit with an external resistance of \SI{49}{\ohm} and with a current source having an emf of \SI{10}{\volt} and an internal resistance of \SI{1}{\ohm}. Determine the reading of the ammeter and compute the relative error of its reading assuming that it has no resistance.
\end{enumerate}








\cleardoublepage
