% !TEX root = tarasov-qasp-2.tex

\cleardoublepage
\thispagestyle{empty}
%\vspace*{2cm}
\begin{figure*}[!ht]
\centering
\includegraphics[width=0.75\textwidth]{figs/sec/tarasov-qasp-sec-02.pdf}
\end{figure*}
%\begin{centering}
\begin{fullwidth}
%%\paragraph{}
{\textsf{\Large \hlred{The concept of a force is one of the basic physical concepts. Can you apply it with sufficient facility? Do you have a good understanding of the laws of dynamics?}}}
\end{fullwidth}
%\end{centering}
\cleardoublepage

\chapter{Can You Show The Forces Applied To A Body?}
\label{ch-02}

\begin{dialogue}

\std Problems in mechanics seem to be the most difficult of all. How do you begin to solve them?

\tchr Frequently, you can begin by considering the forces applied to a body. As an example, we can take the following cases \figr{fig-07}: 
\begin{enumerate}[label=(\alph*), leftmargin=1cm]
\item the body is thrown upward at an angle to the horizontal, 
\item the body slides down an inclined plane, 
\item the body rotates on the end of a string in a vertical plane, and
\item the body is a pendulum.
\end{enumerate}
\begin{marginfigure}
\centering
\includegraphics[width=\linewidth]{figs/ch-02/fig-007a.pdf}
\caption{A variety of different situations for bodies. For each case we have to find out the forces acting on the given body.}
\label{fig-07}
\end{marginfigure}
Draw arrows showing the forces applied to the body in each of these cases, and explain what the arrows represent.

\std Here is my drawing (\figr{fig-08}). In the first case, $P$ is the weight of the body and $F$ is the throwing force. In the second, $P$ is the weight, $F$ is the force which keeps the body sliding along the, plane and $F_{f\!\!r}$ is the friction force. In the third, $P$ is the weight, $F_{c}$, is the centripetal force and $T$ is the tension in the string. In the fourth case, $P$ is the weight, $F$ is the restoring force and $T$ is the tension in the string. 


\tchr You have made mistakes in all four cases. Here I have the correct drawing (\figr{fig-09}). One thing that you must understand clearly is that a force is the result of interaction between bodies. Therefore, to show the forces applied to a body you must first establish what bodies
interact with the given body. Thus, in the first case, only the earth interacts with the body by attracting it (\figr{fig-09}~\drkgry{(a)}). Therefore, only one force, the weight $P$, is applied to the body. If we wished to take into consideration the resistance of the air or, say, the action of the wind, we would have to introduce additional forces. \hlblue{No ``throwing force'', shown in your drawing, actually exists, since there is no interaction creating such a force.}

\begin{marginfigure}[-1cm]%
\centering
\includegraphics[width=\textwidth]{figs/ch-02/fig-008a.pdf}
\caption{Student response for forces acting on bodies in different situations given in \figr{fig-07}.}
\label{fig-08}
\end{marginfigure}

\std But to throw a body, surely some kind of force must be exerted on it.

\tchr Yes, that's true. When you throw a body you exert a certain force on it. In the case above, however, we dealt with the motion of the body after it was thrown, \ie after the force which imparted a definite initial velocity of flight to the body had ceased to act. \hlblue{It is impossible to ``accumulate'' forces; as soon as the interaction of the bodies ends, the force isn't there any more.}

\std But if only the weight is acting on the body, why doesn't it fall vertically downward instead of travelling along a curved path? 

\tchr It surprises you that in the given case the direction of motion of the body does not coincide with the direction of the force acting on it. This, however, fully agrees with Newton's second law. Your question shows that you haven't given sufficient thought to Newton's laws of dynamics. I intend to discuss this later (see  \chap{ch-04}). Now I want to continue our analysis of the four cases of motion of a body. In the second case (\figr{fig-09}~\drkgry{(b)}), a body is sliding down an inclined plane. What bodies are interacting with it?

\std Evidently, two bodies: the earth and the inclined plane. 

\tchr Exactly. This enables us to find the forces applied to the body. The earth is responsible for the weight $P$, and the inclined plane causes the force of sliding friction $F_{f\!\!r}$
and the force $N$ ordinarily called the bearing reaction\sidenote{Also known as the normal force.}. Note that you entirely omitted force $N$ in your drawing.

\std Just a moment! Then the inclined plane acts on the body with two forces and not one?
\begin{marginfigure}%
\centering
\includegraphics[width=\textwidth]{figs/ch-02/fig-009a.pdf}
\caption{For each case the correct drawing of forces acting on the bodies are made. Compare this with \figr{fig-08}.}
\label{fig-09}
\end{marginfigure}

\tchr There is, of course, only one force. It is, however, more convenient to deal with it in the form of two component forces, one directed along the inclined plane (force of sliding friction) and the other perpendicular to it (bearing reaction). The fact that these forces have a common origin, i.e, that they are components of the same force, can be seen in the existence
of a universal relation between $F_{f\!\!r}$ and $N$:
\begin{equation}%
F_{f\!\!r}=kN
\label{eq-05}
\end{equation}
where $k$ is a constant called the coefficient of sliding friction. We shall deal with this relationship in more detail later (\chap{ch-03}). 

\std In my drawing, I showed a sliding force which keeps the body sliding down the plane. Evidently, there is no such force. But I clearly remember hearing t.he term ``sliding force'' used frequently in the past. What can you say about this?
 
\tchr Yes, such a term actually exists. You must bear in mind, however, that \hlblue{the sliding force, as you call it, is simply one of the components of the body's weight, obtained when the weight is resolved into two forces, one along the plane and the other normal to it.} If, in enumerating the forces applied to the body, you have named the weight, there is no reason to add the sliding force, one of its components. In the third case (\figr{fig-09}~\drkgry{(c)}), the body rotates in a vertical plane. What bodies act on it?
 
\std Two bodies: the earth and the string. 

\tchr Good, and that is why two forces are applied to the body: the weight and the tension of the string. 
 
\std But what about the centripetal force? 

\tchr Don't be in such a hurry! So many mistakes are made in problems concerning the motion of a body in a circle that I intend to dwell at length on this further on (\chap{ch-08}).
Here I only wish to note that \hlblue{the centripetal force is not some kind of additional force applied to the body. It is the resultant force.} In our case (when the body is at the lowest point of its path), the centripetal force is the difference between the tension of the string and the weight.

\std If I understand it correctly, the restoring force in the fourth case  (\figr{fig-09}~\drkgry{(d)}) is also the resultant of the tension in the string and the weight? 

\tchr Quite true. Here, as in the third case, the string and the earth interact with the body. Therefore, two forces, the tension of the string and the weight, are applied to the body.
I wish to emphasize again that \hlblue{forces arise only as a result of interaction of bodies; they cannot originate from any ``accessory'' considerations. Find the bodies acting on the given object and you will reveal the forces applied to the object.}

\std No doubt there are more complicated cases than the ones you have illustrated in  (\figr{fig-07}). Can we consider them?

\tchr There are many examples of more complicated interaction of bodies. For instance, a certain constant horizontal force $F$ acts on a body as a result of which the body moves upward along an inclined surface. The forces applied to the body in this case are shown in  (\figr{fig-10}).


\begin{figure}
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-02/fig-010a.pdf}
\caption{A body on inclined plane and forces acting on it.}
\label{fig-10}
\end{figure}

Another example is the oscillation of an electrically charged pendulum placed inside a parallel-plate capacitor. Here we have an additional force $F_{e}$ with which the field of the capacitor acts on the charge of the pendulum (\figr{fig-11}). It is obviously impossible to mention all the conceivable cases that may come up in solving problems.

\begin{figure}[!h]%
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-02/fig-011a.pdf}
\caption{Forces acting on an electrically charged pendulum inside a parallel plate capacitor.}
\label{fig-11}
\end{figure}
\std What do you do when there are several bodies in the problem? Take, for example, the case illustrated in \figr{fig-12}.

\tchr You should clearly realize each time the motion of what bodies or combination of bodies you intend to consider. Let us take, for instance, the motion of body 1 in the example you proposed. The earth, the inclined plane and string $AB$ interact with this body.

\begin{figure}[!h]%
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-02/fig-012a.pdf}
\caption{What are the forces acting on three bodies on balanced on an incline?}
\label{fig-12}
\end{figure}

\std Doesn't body 2 interact with body 1? 

\tchr Only through string $AB$. The forces applied to body 1 are the weight $P'$,
force $F_{f\!\!r}$ of sliding friction, bearing reaction $N'$ and the tension $T'$ of string $AB$ (\figr{fig-13}~\drkgry{(a)}). 

\std But why is the friction force directed to the left in your drawing? It would seem just as reasonable to have it act in the opposite direction. 

\begin{figure}[!h]%
\centering
\includegraphics[width=0.45\textwidth]{figs/ch-02/fig-013a.pdf}
\caption{Forces acting on the three bodies balanced on an incline as shown in Figure \ref{fig-12}.}
\label{fig-13}
\end{figure}

\tchr \hlblue{To determine the direction of the friction force, it is necessary to know the direction in which the body is travelling.} If this has not been specified in the problem, we should assume either one or the other direction. In the given problem, I assume that body 1 (together with the whole
system of bodies) is travelling to the right and the pulley is rotating clockwise. Of course, I cannot know this beforehand; the direction of motion becomes definite only after the corresponding numerical values are substituted. If my assumption is wrong, I shall obtain a negative value when I calculate the acceleration. Then I will have to assume that the body moves to the left instead of to the right (with the pulley rotating counterclockwise) and to direct the force of sliding friction correspondingly. After this I can derive an equation for calculating the acceleration and again check its sign by substituting the numerical values. 

\std Why check the sign of the acceleration a second time? If it was negative when motion was assumed to be to the right, it will evidently be positive for the second assumption.

\tchr No, it can turn out to be negative in the second case as well.

\std I can't understand that. Isn't it obvious that if the body is not moving to the right it must be moving to the left?

\tchr You forget that the body can also be at rest. We shall return to this question later and analyse in detail the complications that arise when we take the friction force into
consideration (see  \chap{ch-07}).

For the present, we shall just assume that the pulley rotates clockwise and examine the motion of body 2.

\std The earth, the inclined plane, string $AB$ and string $CD$ interact with body 2. The forces applied to body 2 are shown in \figr{fig-13}~\drkgry{(b)}.

\tchr Very well. Now let us go over to body 3.

\std Body 3 interacts only with the earth and with string $CD$. \figr{fig-13}~\drkgry{(c)} shows the forces applied to body 3.

\tchr Now, after establishing the forces applied to each body, you can write the equation of motion for each one and then solve the system of equations you obtain.

\std You mentioned that it was not necessary to deal with each body separately, but that we could also consider the set of bodies as a whole.

\tchr Why yes; bodies 1, 2 and 3 can be examined, not separately as we have just done, but as a whole. Then, the tensions in the strings need not be taken into consideration since they become, in this case, internal forces, i.e. forces of interaction between separate parts of the item being considered. The system of the three bodies as a whole interacts only with the earth and the inclined plane.

\std I should like to clear up one point. When I depicted the forces in \figr{fig-13}~\drkgry{(b)} and \drkgry{(c)}, I assumed that the tension in string $CD$ is the same on both sides of the pulley. Would that be correct?

\tchr Strictly speaking, that's incorrect. If the pulley is rotating clockwise, the tension in the part of string $CD$ attached to body 3 should be greater than the tension in the part of the string attached to body 2. This difference in tension is what causes accelerated rotation of the pulley. It was assumed in the given example that the mass of the pulley can be disregarded. In other words, the pulley has no mass that is to be accelerated, it is simply regarded as a means of changing the direction of the string connecting bodies 2 and 3. Therefore, it can be assumed that the tension in string $CD$ is the same on both sides of the pulley. \hlblue{As a rule, the mass of the pulley is disregarded unless otherwise stipulated.} Have we cleared up everything?

\std I still have a question concerning the point of application of the force. In your drawings you applied all the forces to a single point of the body. Is this correct? Can you apply the force of friction, say, to the centre of gravity of the body?

\tchr It should be remembered that we are studying the kinematics and dynamics, not of extended bodies, but of material points, or particles, i.e. we regard the body to be of point mass. On the drawings, however, we show a body, and not a point, only for the sake of clarity. Therefore, \hlblue{all the forces can be shown as applied to a single point of the body.}

\std We were taught that any simplification leads to the loss of certain aspects of the problem. Exactly what do we lose when we regard the body as a material point?

\begin{figure}[!h]%
\centering
\includegraphics[width=0.4\textwidth]{figs/ch-02/fig-014a.pdf}
\caption{Forces acting on the three bodies on balanced on an incline shown in \figr{fig-12}.}
\label{fig-14}
\end{figure}
\tchr In such a simplified approach we do not take into account the rotational moments which, under real conditions, may result in rotation and overturning of the body. A material point has only a motion of translation. Let us consider an example. Assume that two forces are applied at two different points of a body: $F_{1}$ at point $A$ and $F_{2}$ at point $B$, as shown in \figr{fig-14}~\drkgry{(a)}. Now let us apply, at point $A$, force $F_{2}'$  equal and parallel to force $F_{2}$  , and also force $F_{2}''$  equal to force $F_{2}$  but acting in the opposite direction (see \figr{fig-14}~\drkgry{(b)}). Since forces $F_{2}'$ and $F_{2}''$ counterbalance each other, their addition does not alter the physical aspect of the problem in any way. However, \figr{fig-14}~\drkgry{(b)} can be interpreted as follows: forces $F_{1}$ and  $F_{2}'$ applied at point $A$ cause motion of translation of
the body also applied to the body is a force couple (forces $F_{2}$ and $F_{2}''$) causing rotation. In other words, force $F_{2}$ can be transferred to point $A$  of the body if, at the same time, the
corresponding rotational moment is added. When we regard the body as a material point, or particle, there will evidently be no rotational moment.
\std You say that a material point cannot rotate but has only motion of translation. But we have already dealt with rotational motion - motion in a circle. 
\tchr Do not confuse entirely different things. The motion of translation of a point can take place along various paths, for instance in a circle. When I ruled out the possibility of rotational motion of a point I meant rotation about itself, i.e, about any axis passing through the point.

\end{dialogue}
