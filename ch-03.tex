% !TEX root = tarasov-qasp-2.tex

\chapter{Can You Determine The Friction Force?}
\label{ch-03}

\begin{dialogue}
\tchr I should like to dwell in more detail on the calculation of the friction force in various problems. I have in mind dry sliding friction (sliding friction is said to be dry when there is no layer of any substance, such as a lubricant, between the sliding surfaces).

\std But here everything seems to be quite clear.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-03/fig-015a.pdf}
\caption{A sled being pulled with a rope.}
\label{fig-15}
\end{figure}
\tchr Nevertheless, many mistakes made in examinations are due to the inability to calculate the friction force. Consider the example illustrated in \figr{fig-15}. \hlred{A sled of weight $P$ is being pulled with a force $F$ applied to a rope which makes an angle $\alpha$ with the horizontal; the coefficient of friction is $k$. Find the force of sliding friction.} How will you go about it?

\std Why, that seems to be very simple. The friction force equals $kP$.

\tchr Entirely wrong. The force of sliding friction is equal, not to $kP$, but to $kN$, where $N$ is the bearing reaction. Remember equation \eqref{eq-05} from  \chap{ch-02}.

\std But isn't that the same thing?

\tchr In a particular case, the weight and the bearing reaction may be equal to each other, but, in general, they are entirely different forces. Consider the example I proposed. The forces applied to the body (the sled) are the weight $P$, bearing reaction $N$, force $F_{f\!\!r}$ of sliding friction and the tension $F$ of the rope (see \figr{fig-15}). We resolve force $F$ into its vertical ($F \sin \alpha$) and horizontal ($F \cos \alpha$) components. All forces acting in the vertical direction counterbalance one another. This enables us to find the bearing reaction: 
\begin{equation}%
N = P - F \sin \alpha
\label{eq-06}
\end{equation}
As you can see, this force is not equal to the weight of the sled, but is less by the amount $F \sin \alpha$. Physically, this is what should be expected, because the taut rope, being pulled at an angle upwards, seems to ``raise'' the sled somewhat. This reduces the force with which the sled bears on the surface
and thereby the bearing reaction as well. So, in this case, 
\begin{equation}%
F_{f\!\!r}=k(P - F \sin \alpha)
\label{eq-07}
\end{equation}
If the rope were horizontal ($\alpha=0$), then instead of equation (\ref{eq-06}) we would have $N=P$, from which it follows that $F_{f\!\!r}=kP$.

\std I understand now. I never thought about this before.

\tchr This is quite a common error of examinees who attempt to treat the force of sliding friction as the product of the coefficient of friction by the weight and not by the bearing reaction. Try to avoid such mistakes in the future.

\std I shall follow the rule: \hlblue{to find the friction force, first determine the bearing reaction.}

\tchr So far we have been dealing with the force of sliding friction. Now let us consider static friction. This has certain specific features to which students do not always pay
sufficient attention. Take the following example. A body is at rest on a horizontal surface and is acted on by a horizontal force F which tends to move the body. How great do you think the friction force will be in this case?

\std If the body rests on a horizontal plane and force $F$ acts horizontally, then $N=P$. Is that correct?

\tchr Quite correct. Continue.

\std It follows that the friction force equals $kP$.
\begin{figure}[!ht]%
\centering
\includegraphics[width=0.45\textwidth]{figs/ch-03/fig-016a.pdf}
\caption{Forces acting on a body at rest.}
\label{fig-16}
\end{figure}

\tchr You have made a typical mistake by confusing the forces of sliding and static friction. If the body were sliding along the plane, your answer would be correct. But here the body is at rest. Hence it is necessary that all forces applied to the body counterbalance one another. Four forces act on the body: the weight $P$, bearing reaction $N$, force $F$ and the force of static friction $F_{f\!\!r}$ (\figr{fig-16}). The vertical forces $P$ and $N$ counterbalance each other. So should the horizontal forces
$F$ and $F_{f\!\!r}$ Therefore
\begin{equation}%
F_{f\!\!r} = F
\label{eq-08}
\end{equation}
\std It follows that the force of static friction depends on the external force tending to move the body.

\tchr Yes, that is so. The force of static friction increases with the force $F$. It does not increase infinitely, however. The force of static friction reaches a maximum value:
\begin{equation}%
F_{max} = k_{0}N
\label{eq-09}
\end{equation}


Coefficient $k_{0}$ slightly exceeds coefficient $k$ which characterizes, according to equation (\ref{eq-05}), the force of sliding friction. As soon as the external force F reaches the value $k_{0}N$, the body begins to slide. At this value, coefficient $k_{0}$ becomes equal to $k$, and so the friction force is reduced somewhat. Upon further increase of force $F$, the friction force (now the force of sliding friction) ceases to increase further (until very high velocities are attained), and the body travels with gradually increasing acceleration. The inability of many examinees to determine the friction force is disclosed by the following rather simple question:
\hlred{What is the friction force when a body of weight $P$ is at rest on an inclined plane with an angle of inclination $\alpha$?} One hears a variety of incorrect answers. Some say that the friction force equals $kP$, and others that it equals $kN =kP \cos \alpha$.

\std I understand. Since the body is at rest, we have to deal with the force of static friction. It should be found from the condition of equilibrium of forces acting along the inclined plane. There are two such forces in our case: the friction force $F_{f\!\!r}$ and the sliding force $P \sin \alpha$ acting downward along the plane: Therefore, the correct answer is
\begin{equation*}%
F_{f\!\!r}=P \sin \alpha
\end{equation*}
\tchr Exactly. In conclusion, consider the problem illustrated in \emph{Figure \ref{fig-17}}. \hlred{A load of mass $m$ lies on a body of mass $M$; the maximum force of static friction between the two is characterized by the coefficient $k_{0}$ and there is no friction between the body and the earth. Find the minimum force $F$ applied to the body at which the load will begin to slide along it.}
\begin{marginfigure}
\centering
\includegraphics[width=\textwidth]{figs/ch-03/fig-017a.pdf}
\caption{A load of mass $m$ lies on a body of mass $M$. We have to find the minimum force $F$ so that load begins to slide. }
\label{fig-17}
\end{marginfigure}
\std First I shall assume that force $F$ is sufficiently small, so that the load will not slide along the body. Then the two bodies will acquire the acceleration 
\begin{equation*}%
a= \frac{F}{M+m}
\end{equation*}

\tchr Correct. What force will this acceleration impart to the load?

\std It will be subjected to the force of static friction $F_{f\!\!r}$ by the acceleration. Thus
\begin{equation*}%
F_{f\!\!r} = ma= \frac{Fm}{M+m}
\end{equation*}
It follows that with an increase in force $F$, the force of static friction $F_{f\!\!r}$ also increases. It cannot, however, increase infinitely. Its maximum value is 
\begin{equation*}%
F_{\textrm{fr \hairsp max}}= k_{0}N = k_{0}mg
\end{equation*}
Consequently, the maximum value of force $F$. at which the two bodies can still travel together as an integral unit is determined from the condition
\begin{equation*}%
 k_{0}mg = \frac{Fm}{M+m}
\end{equation*}
This, then, is the minimum force at which the load begins to slide along the body.

\tchr Your solution of the proposed problem is correct. I am completely satisfied with your reasoning.

\end{dialogue}
