% !TEX root = tarasov-qasp-2.tex
\chapter[How Do You Go About Solving Problems In Dynamics?]{How Do You Go About\\ Solving Problems In Dynamics?}
\label{ch-06}

\begin{dialogue}
\tchr In solving problems in dynamics it is especially important to be able to determine correctly the forces applied to the body (see \chap{ch-02}).

\std Before we go any further, I wish to ask one question. Assuming that I have correctly found all the forces applied to the body, what should I do next?

\tchr \hlblue{If the forces are not directed along a single straight line, they should be resolved in two mutually perpendicular directions.} The force components should be dealt with separately for each of these directions, which we shall call ``directions of resolution''. 

We can begin with some practical advice. In the first place, \hlblue{the forces should be drawn in large scale to avoid confusion in resolving them.} In trying to save space students usually represent forces in the form of almost microscopic arrows, and this does not help. You will understand what I mean if you compare your drawing (\figr{fig-08}) with mine (\figr{fig-09}). Secondly, do not hurry to resolve the forces before it can be done properly. \hlblue{First you should find all forces applied to the body, and show them in the drawing.} Only then can you begin to resolve some of them. Thirdly, you must remember that \hlblue{after you have resolved a force you should ``forget'' about its existence and use only its components. Either the force itself, or its components, no compromise.}

\std How do I choose the directions of resolution?

\tchr In making your choice you should consider the nature of the motion of the body. There are two alternatives:
\begin{enumerate*}[label=(\arabic*)]
\item the body is at rest or travels with uniform velocity in a straight line, and 
\item the body travels with acceleration and the direction of acceleration is given (at least its sign).
\end{enumerate*}

In the first case you can select the directions of resolution arbitrarily, basing (or not basing) your choice on considerations of practical convenience. Assume, for instance, that in the case illustrated in \figr{fig-10} the body slides with uniform velocity up the inclined plane. Here the directions of resolution may be (with equal advantage) either vertical and horizontal (\figr{fig-27}~\drkgry{(a)}) or along the inclined plane and perpendicular to it (\figr{fig-27}~\drkgry{(b)}).
\begin{figure}[!ht]
\centering
\includegraphics[width=\textwidth]{figs/ch-06/fig-027a.pdf}
\caption{Two ways of resolving the forces applied to a body in motion.}
\label{fig-27}
\end{figure}
After the forces have been resolved, the algebraic sums of the component forces for each direction of resolution are equated to zero (remember that we are still dealing with the motion of bodies without acceleration). For the case illustrated in \figr{fig-27}~\drkgry{(a)} we can write the system of equations
\begin{equation}% 
\left.
\begin{aligned}
N \cos \alpha -F_{fr} \sin \alpha- P & = 0 \\
F - F_{fr} \cos \alpha - N \sin \alpha & = 0
\label{eq-21}
\end{aligned}
\right\}
\end{equation}
%\begin{equation} 
%\begin{split}
%(a+b)ˆ2 & = (a+b)(a+b)
% & = aˆ2+ab+ba+bˆ2
%& = aˆ2+2ab+bˆ2 
%\end{split}
%\end{equation}
The system of equations for the case in \figr{fig-27}~\drkgry{(b)} is
\begin{equation}% 
\left.
\begin{aligned}
N  - P \cos \alpha -F \sin \alpha & = 0 \\
F_{fr} + P \sin \alpha - F \cos \alpha & = 0
\label{eq-22}
\end{aligned}
\right\}
\end{equation}
\std But these systems of equations differ from each other.

\tchr They do but, nevertheless, lead to the same results, as can readily be shown. Suppose it is required to find the force $F$ that will ensure the motion of the body at uniform
velocity up along the inclined plane. Substituting equation \eqref{eq-05} into equations \eqref{eq-21} we obtain
\begin{equation*}%
\left.
\begin{aligned}
N  \left( \cos \alpha - k \sin \alpha \right)  - P & = 0 \\
F  - N \left( k \cos \alpha + \sin \alpha \right) & = 0
%\label{eq-22}
\end{aligned}
\right\}
\end{equation*}
From the first equation of this system we get
\begin{equation*}%
N = \frac{P}{\cos \alpha -k \sin \alpha}
\end{equation*}
which is substituted into the second equation to determine the required force. Thus
\begin{equation*}%
F = P \left(\frac{k \cos \alpha + \sin \alpha }{\cos \alpha - k \sin \alpha}\right)
\end{equation*}
Exactly the same answer is obtained from equations \eqref{eq-22}. You can check this for yourself.

\std What do we do if the body travels with acceleration?

\tchr In this case the choice of the directions of resolution depends on the direction in which the body is being accelerated (direction of the resultant force). \hlblue{Forces should be resolved in a direction along the acceleration and in one perpendicular to it.} The algebraic sum of the force components in the direction perpendicular to the acceleration is equated to zero, while that of the force components in the direction along the acceleration is equal, according to Newton's second law of motion, to the product of the mass of the body by its acceleration.

Let us return to the body on the inclined plane in the last problem and assume that the body slides with a certain acceleration up the plane. According to my previous remarks, the forces should be resolved as in the case shown in \figr{fig-27}~\drkgry{(b)}. Then, in place of equations \eqref{eq-22}, we can write the following system
\begin{equation}% 
\left.
\begin{aligned}
N  - P \cos \alpha -F \sin \alpha & = 0 \\
F \cos \alpha - F_{fr} - P \sin \alpha & = ma = P \left( \frac{a}{g} \right)
\label{eq-23}
\end{aligned}
\right\}
\end{equation}
Making use of equation \eqref{eq-05}, we find the acceleration of the body
\begin{equation*}%
a = \left( \frac{g}{p} \right) \left(F \cos \alpha - \left(P \cos \alpha + F \sin \alpha \right) k - P \sin \alpha \right) 
\end{equation*}
\std In problems of this kind dealing with acceleration, can the forces be resolved in directions other than along the acceleration and perpendicular to it? As far as I understand from your explanation, this should not be done.

\tchr Your question shows that I should clear up some points. Of course, even in problems on acceleration you have a right to resolve the forces in any two mutually perpendicular directions. In this case, however, you will have to resolve not only the forces, but the acceleration vector as well. This method of solution will lead to additional difficulties. To avoid unnecessary complications, it is best to proceed exactly as is advised. This is the simplest course. The direction of the body's acceleration is always known (at least its sign), so you can proceed on the basis of this direction. \hlblue{The inability of examinees to choose the directions of force resolution rationally is one of the reasons for their helplessness in solving more or less complex problems in dynamics.}

\std We have only been speaking about resolution in two directions. In the general case, however, it would probably be more reasonable to speak of resolution in three mutually perpendicular directions. Space is actually three-dimensional.

\tchr You are absolutely right. The two directions in our discussions are explained by the fact that we are dealing with plane (two-dimensional) problems. In the general case, forces should be resolved in three directions. All the remarks made above still hold true, however. I should mention that, as a rule, two-dimensional problems are given in examinations. Though, of course, the examinee may be asked to make a not-too-complicated generalization for the three-dimensional case.

\end{dialogue}

%\newpage
\section*{Problems}
\label{prob-02}
\addcontentsline{toc}{section}{Problems}
%%\begin{fullwidth}

\begin{enumerate}[resume*=problems]
\item A body with a mass of \SI{5}{kg} is pulled along a horizontal plane by a force of \SI{3}{\newton}\sidenote[][-1cm]{Originally unit of \emph{kgf} is used, here after we will use the unit of Newtons denoted by \si{\newton}.} applied to the body at an angle of \ang{30} to the horizontal. The coefficient of sliding friction is 0.2. Find the velocity of the body \SI{10}{\second} after the pulling force begins to act, and the work done by the friction force during this time.

\item A man pulls two sleds tied together by applying a force of $F=$ \SI{12}{\newton} to the pulling rope at an angle of \ang{45} to the horizontal (\figr{fig-28}). The masses of the sleds are equal to $m_{1}=m_{2}=$ \SI{15}{\kilogram}. The coefficient of friction between the runners and the snow is 0.02. Find the acceleration of the sleds. the tension of the rope tying the sleds together, and the force with which the man should pull the rope to impart uniform velocity to the sleds.

\begin{figure}%[-4cm]
\centering
\includegraphics[width=0.3\textwidth]{figs/ch-06/fig-028a.pdf}
\caption{Sled being pulled by a rope with an angle \ang{45}. See Problem 9.}
\label{fig-28}
\end{figure}
\item Three equal weights of a mass of \SI{2}{\kilogram} each are hanging on a string passing over a fixed pulley as shown in \figr{fig-29}. Find the acceleration of the system and the tension of the string connecting weights 1 and 2.
\begin{figure}%[3cm]
\centering
\includegraphics[width=0.15\textwidth]{figs/ch-06/fig-029a.pdf}
\caption{A system of three interacting masses.  See Problem 10.}
\label{fig-29}
\end{figure}
\item Calculate the acceleration of the weights and the tension in the strings for the case illustrated in \figr{fig-30}. Given: $\alpha = \ang{30}$, $P_{1}=\SI{4}{\newton}$, $P_{2}=\SI{2}{newton}$, and $P_{3}=\SI{8}{\newton}$. Neglect the friction between the weights and the inclined plane.
\begin{figure}[!h]
\centering
\includegraphics[width=0.34\linewidth]{figs/ch-06/fig-030a.pdf}
\caption{A system of three masses on an incline. See problem 11.}
\label{fig-30}
\end{figure}
%\end{enumerate}
%\newpage
%\begin{enumerate}[resume*=problems]
% had to do some juggling with imagesize in order to fit these here, ideally the pulley one should be a margin figure, but keeping that screws up rest of the placements.

\item Consider the system of weights shown in \figr{fig-31}. Here $P_{1}=\SI{1}{\newton}$$P_{2}=\SI{2}{\newton}$, $P_{5}=\SI{8}{\newton}$ and $P_{4}=\SI{0.5}{\newton}$, and $\alpha=\ang{30}$. The coefficient of friction between the weights and the planes equals 0.2. Find the acceleration of the set of weights, the tension of the strings and the force with which
weight $P_{4}$ presses downward on weight $P_{3}$.
\begin{marginfigure}
\centering
\includegraphics[width=0.8\linewidth]{figs/ch-06/fig-031a.pdf}
\caption{A system of three masses on an incline. See problem 12}
\label{fig-31}
\end{marginfigure}
\end{enumerate}


%%\end{fullwidth}
